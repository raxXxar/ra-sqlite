package ra.x;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.DataSetObserver;

public abstract class IndexAccessor {

	public static final String DEF_FAST_ACCESS_COLUMN_NAME = "pk$join$column";
	public static final int POSITION_UNDEFINED = -1;

	@SuppressLint("DefaultLocale")
	protected IndexAccessor(Cursor cursor, String fastAccessColumnName) {
		if (null == cursor) {
			throw new IllegalArgumentException("Cursor should not be null");
		}
		if (null == fastAccessColumnName) {
			throw new IllegalArgumentException("fastAccessColumnName should not be null");
		}
		this.cursor = cursor;
		this.cursor.registerDataSetObserver(cursorDataChangeListener);
		this.fastAccessColumnName = fastAccessColumnName.toLowerCase();
		init();
	}

	protected IndexAccessor(Cursor cursor) {
		this(cursor, DEF_FAST_ACCESS_COLUMN_NAME);
	}

	public abstract int locate(long indexValue);

	protected abstract void deactivateInner();

	protected abstract boolean initInner();

	protected void init() {
		synchronized (syncObject) {
			if (cursor.getCount() > 0) {
				deactivateInner();
				setValid(initInner());
			}
		}
	}

	protected void deactivate() {
		synchronized (syncObject) {
			setValid(false);
			deactivateInner();
		}
	}

	protected void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	protected boolean isValid() {
		return isValid;
	}

	protected final DataSetObserver cursorDataChangeListener = new DataSetObserver() {

		@Override
		public void onChanged() {
			init();
			super.onChanged();
		}

		@Override
		public void onInvalidated() {
			deactivate();
			super.onInvalidated();
		}

	};

	protected final Object syncObject = new Object();
	protected final Cursor cursor;
	protected final String fastAccessColumnName;

	private boolean isValid = false;
}
