/*
 * ra_sqlite.c
 *
 *      Author: =ra=
 */

#include <jni.h>
#include <stddef.h>
#include "ra_sqlite.h"
#include "ra_pool.h"

jclass jClassSQLException;
jclass jClassNoSuchCursorException;
jclass jClassNoSuchDatabaseException;

jclass jClassString;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
	JNIEnv* jniEnv = NULL;
	if (JNI_OK != (*vm)->GetEnv(vm, (void**) &jniEnv, JNI_VERSION_1_4)) {
		return -1;
	}
	jClassString = (*jniEnv)->FindClass(jniEnv, CLASS_PATH_STRING);
	if (NULL == jClassString) {
		return -1;
	} else {
		jClassString = (*jniEnv)->NewGlobalRef(jniEnv, jClassString);
	}

	jClassSQLException = (*jniEnv)->FindClass(jniEnv, CLASS_PATH_SQL_EXCEPTION);
	if (NULL == jClassString) {
		return -1;
	} else {
		jClassSQLException = (*jniEnv)->NewGlobalRef(jniEnv,
		                                             jClassSQLException);
	}

	jClassNoSuchCursorException =
	    (*jniEnv)->FindClass(jniEnv, CLASS_PATH_NO_SUCH_CURSOR_SQL_EXCEPTION);
	if (NULL == jClassNoSuchCursorException) {
		return -1;
	} else {
		jClassNoSuchCursorException =
		    (*jniEnv)->NewGlobalRef(jniEnv, jClassNoSuchCursorException);
	}
	jClassNoSuchDatabaseException =
	    (*jniEnv)->FindClass(jniEnv, CLASS_PATH_NO_SUCH_DATABASE_EXCEPTION);
	if (NULL == jClassNoSuchDatabaseException) {
		return -1;
	} else {
		jClassNoSuchDatabaseException =
		    (*jniEnv)->NewGlobalRef(jniEnv, jClassNoSuchDatabaseException);
	}
	initPool();
	return JNI_VERSION_1_4;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved) {
	destroyPool();
	JNIEnv* jniEnv = NULL;
	if (JNI_OK != (*vm)->GetEnv(vm, (void**) &jniEnv, JNI_VERSION_1_4)) {
		return;
	}
	if (jClassNoSuchDatabaseException) {
		(*jniEnv)->DeleteGlobalRef(jniEnv, jClassNoSuchDatabaseException);
	}
	if (jClassNoSuchCursorException) {
		(*jniEnv)->DeleteGlobalRef(jniEnv, jClassNoSuchCursorException);
	}
	if (jClassSQLException) {
		(*jniEnv)->DeleteGlobalRef(jniEnv, jClassSQLException);
	}
	if (jClassString) {
		(*jniEnv)->DeleteGlobalRef(jniEnv, jClassString);
	}

}
