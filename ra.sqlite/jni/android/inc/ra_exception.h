/*
 * ra_exception.h
 *
 *       Author: =ra=
 */

#ifndef RA_EXCEPTION_H_
#define RA_EXCEPTION_H_

#include <jni.h>
#include "sqlite3.h"

#define LOG_MSG

#define MSG_LEN 1024

void throw_sqlite3_exception(JNIEnv* jniEnv, sqlite3* handle, jint errcode);
void throw_sqlite3_exception_msg(JNIEnv* jniEnv, const char* msg);

#define THROW_SQL3(jniEnv, err, dbHandle) \
        {if(SQLITE_OK != err)throw_sqlite3_exception(jniEnv, dbHandle, err);}

#define THROW_SQL3_MSG_FORMAT(jniEnv, ...) \
	{char buf[MSG_LEN]; sprintf(buf, __VA_ARGS__); \
	throw_sqlite3_exception_msg(jniEnv, buf);}

void throw_no_such_cursor_msg(JNIEnv* jniEnv, const char* msg);
#define THROW_NO_CURSOR_FORMAT(jniEnv, ...) \
	{char buf[MSG_LEN]; sprintf(buf, __VA_ARGS__); \
	throw_no_such_cursor_msg(jniEnv, buf);}

void throw_no_such_db_msg(JNIEnv* jniEnv, const char* msg);
#define THROW_NO_DB_FORMAT(jniEnv, ...) \
	{char buf[MSG_LEN]; sprintf(buf, __VA_ARGS__); \
	throw_no_such_db_msg(jniEnv, buf);}

#endif /* RA_EXCEPTION_H_ */
