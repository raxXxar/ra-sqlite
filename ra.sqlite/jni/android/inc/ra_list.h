/*
 * ra_list.h
 *
 *       Author: =ra=
 */

#ifndef RA_LIST_H_
#define RA_LIST_H_

#include "thread_lock.h"
#include "ra_sqlite.h"

typedef void* ListRecord;
typedef void (*FnListRecordDestructor)(ListRecord);

typedef struct Link Link;

struct Link {
	ListRecord record;
	Link* next;
};

typedef struct List {
	FnListRecordDestructor recordDestructor;
	Link* head;
	// thread safety
	__LMEMBER;
} List;

List* List_Create(FnListRecordDestructor listRecordDestructor);
void List_Free(List* pList);
void List_Push(List* pList, ListRecord record);
ListRecord List_Pop(List* pList);

#endif /* RA_LIST_H_ */

