/*
 * ra_recordset.h
 *
 *       Author: =ra=
 */

#ifndef RA_RECORDSET_H_
#define RA_RECORDSET_H_

#include <jni.h>
#include "ra_list.h"
#include "ra_extent.h"

struct Field {
	jint type;

	union {
		jdouble d;
		jlong l;

		struct {
			void* data;
			jint size;
		} ptr;
	} value;
}__attribute(( packed ));

typedef struct Field Field;

typedef struct RecordSet {
	Extent** rowExtents;
	jint totalRowCount;
	jint rowExtentsCapacity;
	jint rowsPerExtent;
	jint fieldCount;
	jint rowSize;
	jint blobExtentSize;
	List* blobData;
	Extent* currentBlob;
} RecordSet;

RecordSet* RecordSet_Create(jint fieldCount,
                            jint rowsPerExtent,
                            jint blobExtentSize);

void RecordSet_Free(RecordSet* pRecordSet);

void RecordSet_Reset(RecordSet* pRecordSet);

void RecordSet_SetRowsCount(RecordSet* pRecordSet, jint rowCount);

/* bounds are controlled by caller */
Field* RecordSet_GetField(RecordSet* pRecordSet, jint row, jint col);

jlong RecordSet_GetInt64(Field* pField);

jdouble RecordSet_GetDouble(Field* pField);

jstring RecordSet_GetString(Field* pField, JNIEnv* jniEnv);

jobject RecordSet_GetBlob(Field* pField, JNIEnv* jniEnv);

jint RecordSet_Field_GetType(Field* pField);

void RecordSet_PutInt64(Field* pField, jlong value);

void RecordSet_PutDouble(Field* pField, jdouble value);

void RecordSet_PutString(RecordSet* pRecordSet,
                         Field* pField,
                         const char* str,
                         jint size);

void RecordSet_Field_PutBlob(RecordSet* pRecordSet,
                             Field* pField,
                             const void* data,
                             jint size);

void RecordSet_Field_PutType(Field* pField, jint type);

void RecordSet_GetReadScope(RecordSet* pRecordSet,
                            jint position,
                            jint* start,
                            jint* stop);

jint RecordSet_IsRead(RecordSet* pRecordSet, jint position);

#endif /* RA_RECORDSET_H_ */

