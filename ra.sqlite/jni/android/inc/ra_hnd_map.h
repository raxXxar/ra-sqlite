/*
 * ra_hnd_map.h
 *
 *      Author: =ra=
 */

#ifndef RA_HND_MAP_H_
#define RA_HND_MAP_H_

#include <stdint.h>

typedef uint32_t Handle;
typedef struct HndMap HndMap;

/**
 * return pointer to created HndMap
 * return NULL if failed to create HndMap
 */
HndMap* HndMap_Create(uint16_t size, uint16_t growthSize);

/**
 * Always return NULL
 */
HndMap* HndMap_Free(HndMap* pHndMap);

/**
 * return 0 if fail to insert
 */
Handle HndMap_Insert(HndMap* pHndMap, void* ptr);

/**
 * return NULL if no ptr associated with handle,
 * return ptr if there is a ptr associated with handle
 */
void* HndMap_getPtr(HndMap* pHndMap, Handle handle);

void HndMap_removePtr(HndMap* pHndMap, Handle handle);

#endif /* RA_HND_MAP_H_ */
