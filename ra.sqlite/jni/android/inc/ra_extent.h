/*
 * ra_extent.h
 *
 *       Author: =ra=
 */

#ifndef RA_EXTENT_H_
#define RA_EXTENT_H_

#include <jni.h>
typedef struct Extent {
	jint* ptr;
	jint* freeSpacePtr;
	jint size;
} Extent;

/* returns created extent of specified (size), (size) in bytes */
Extent* Extent_Create(jint size);

/* frees extent (pExtent) */
void Extent_Free(Extent* pExtent);

/*
 * returns pointer to allocated (size) bytes from extent (pExtent),
 * null if no (size) free space in extent (pExtent)
 */
void* Extent_CallocMem(Extent* pExtent, jint size);

/* returns amount of free space in (pExtent) in bytes */
jint Extent_GetFreeSize(Extent* pExtent);

/* resets extent, make it ready for reuse */
// void Extent_Reset(Extent* pExtent);
/* returns pointer to allocated memory */
#define Extent_GetMemPtr(pExtent) ((void*)(pExtent->ptr))

#endif /* RA_EXTENT_H_ */

