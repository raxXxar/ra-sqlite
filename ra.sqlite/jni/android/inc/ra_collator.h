/*
 * ra_collator.h
 *
 *      Author: =ra=
 */

#ifndef RA_COLLATOR_H_
#define RA_COLLATOR_H_

#include "sqlite3.h"
#include "ra_sqlite_SQLiteDatabase_.h"

void setDefaultCollation(JNIEnv* jniEnv,
                         Dbh* pDbh,
                         const char* zLocale /* Locale identifier - (e.g. "uk_UA") */);

#endif /* RA_COLLATOR_H_ */
