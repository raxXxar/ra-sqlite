package ra.retainer;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.HashMap;
import ra.retainer.ClassUtils.MethodAcceptCriteria;
import android.os.Bundle;

/**
 * <p>Operates on classes with help of reflection.</p>
 *
 * This class handles invalid <code>null</code> inputs as best it can.
 * Each method documents its behaviour in more detail.
 *
 */
public abstract class BundleUtils {

	/**
	 * <p>Store instance field value into bundle with help of appropriate bundle method according to field type.</p>
	 *
	 * @param fieldOwnerObj the field owner instance
	 * @param field2Store the field which value is being stored into bundle
	 * @param bundle the bundle
	 * @return true, if successful
	 */
	public static boolean storeIntoBundle(Object fieldOwnerObj, Field field2Store, Bundle bundle) {
		boolean result;
		Method method = getMethod(analyzeClassField(field2Store));
		if (null != method) {
			boolean isFieldAccesible = field2Store.isAccessible();
			try {
				if (!isFieldAccesible)
					field2Store.setAccessible(true);
				Object value = field2Store.get(fieldOwnerObj);
				method.invoke(bundle, new Object[] { field2Store.getName(), value });
				result = true;
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e.getMessage());
			} finally {
				if (!isFieldAccesible)
					field2Store.setAccessible(false);
			}
		} else
			result = false;
		return result;
	}

	/**
	 * Restore instance field value from bundle.
	 *
	 * @param fieldOwnerObj the the field owner instance
	 * @param field2Restore the field which value is being restored from bundle
	 * @param bundle the bundle
	 * @return true, if successful
	 */
	public static boolean restoreFromBundle(Object fieldOwnerObj, Field field2Restore, Bundle bundle) {
		boolean result;
		if (null != fieldOwnerObj && null != bundle && null != field2Restore) {
			ClassUtils.changeObjectValue(fieldOwnerObj, field2Restore, bundle.get(field2Restore.getName()));
			result = true;
		} else
			result = false;
		return result;
	}

	private static final Type[] extractTypeInfo(Type type) {
		Type[] compoundTypeInfo = new Type[2];
		if (type instanceof Class<?>) {
			Class<?> typeClass = (Class<?>) type;
			compoundTypeInfo[0] = Void.TYPE;
			if (typeClass.isPrimitive())
				compoundTypeInfo[1] = type;
			else {
				compoundTypeInfo[1] = ClassUtils.wrapperToPrimitive(typeClass);
				if (null == compoundTypeInfo[1])
					compoundTypeInfo[1] = type;
			}
		} else if (type instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) type;
			compoundTypeInfo[0] = parameterizedType.getRawType();
			Type typeArgumentType = parameterizedType.getActualTypeArguments()[0];
			if (typeArgumentType instanceof WildcardType) {
				WildcardType wildcardType = (WildcardType) typeArgumentType;
				compoundTypeInfo[1] = wildcardType.getUpperBounds()[0];
			} else
				compoundTypeInfo[1] = typeArgumentType;
		}
		return compoundTypeInfo;
	}

	private static final Method getMethod(Type[] typeInfo) {
		return getMethod(typeInfo[0], typeInfo[1]);
	}

	private static final Method getMethod(Type container, Type type) {
		Method result = null;
		HashMap<Type, Method> containerMap = sTypeMethodMap.get(container);
		if (null != containerMap) {
			result = containerMap.get(type);
			if (null == result) {
				Class<?> typeClass = (Class<?>) type;
				for (Type keyType : containerMap.keySet()) {
					Class<?> clazz = (Class<?>) keyType;
					if (Serializable.class.equals(clazz))
						continue; // this makes Serializable check the last case  
					if (clazz.isAssignableFrom(typeClass)) {
						result = containerMap.get(keyType);
						break;
					}
				}
				if (Serializable.class.isAssignableFrom(typeClass)) // check Serializable
					result = containerMap.get(Serializable.class);
			}
		}
		return result;
	}

	private static Type[] analyzeClassField(Field field) {
		Type fieldType = field.getGenericType();
		return extractTypeInfo(fieldType);
	}

	private static final Type[] analyzeMethod(Method method) {
		Type[] gParametersType = method.getGenericParameterTypes();
		Type[] result;
		if (String.class.equals(gParametersType[0]))
			result = extractTypeInfo(gParametersType[1]);
		else
			result = null;
		return result;
	}

	private static final void insertIntoMap(Type container, Type type, Method method) {
		HashMap<Type, Method> containerMap = sTypeMethodMap.get(container);
		if (null == containerMap) {
			containerMap = new HashMap<Type, Method>(2);
			sTypeMethodMap.put(container, containerMap);
		}
		containerMap.put(type, method);
	}

	private static final void buildTypeMethodMap() {
		Method[] putMethods = ClassUtils.getDeclaredMethods(Bundle.class, false, new MethodAcceptCriteria() {

			private static final String PUT_METHOD_PREFIX = "put";
			private static final int PUT_METHOD_PARAMETERS_COUNT = 2;

			@Override
			public boolean match(Method method) {
				return method.getName().startsWith(PUT_METHOD_PREFIX) && method.getReturnType() == Void.TYPE
						&& PUT_METHOD_PARAMETERS_COUNT == method.getParameterTypes().length;
			}
		});
		for (Method method : putMethods) {
			Type[] callInfo = analyzeMethod(method);
			if (null != callInfo)
				insertIntoMap(callInfo[0], callInfo[1], method);
		}
	}

	private static final HashMap<Type, HashMap<Type, Method>> sTypeMethodMap;
	static {
		sTypeMethodMap = new HashMap<Type, HashMap<Type, Method>>(2);
		buildTypeMethodMap();
	}
}
