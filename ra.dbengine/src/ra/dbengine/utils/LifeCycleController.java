package ra.dbengine.utils;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 *  @author =ra=
 */
public class LifeCycleController {

	public interface DismissListener {

		void onObjectDismissed(UUID[] lostIdsList);
	}

	public LifeCycleController(DismissListener dismissListener) {
		mDismissListener = dismissListener;
	}

	public LifeCycleController() {
		this(null);
	}

	public void add(Object controller, UUID id) {
		synchronized (this) {
			invalidateUnsafe();
			addUnsafe(controller, id);
		}
	}

	public UUID removeByUUID(UUID uuid) {
		synchronized (this) {
			invalidateUnsafe();
			return removeByUUIDUnsafe(uuid);
		}
	}

	public UUID removeByObject(Object controller) {
		synchronized (this) {
			invalidateUnsafe();
			return removeByObjectUnsafe(controller);
		}
	}

	public void clear() {
		synchronized (this) {
			stopcontrollingAllUUIDs();
			invalidateUnsafe();
		}
	}

	public void setDismissListener(DismissListener dismissListener) {
		synchronized (this) {
			invalidateUnsafe();
			mDismissListener = dismissListener;
		}
	}

	public void invalidate() {
		synchronized (this) {
			invalidateUnsafe();
		}
	}

	private void addUnsafe(Object controller, UUID id) {
		WeakReference<Object> weakReferencedController = mController.addElement(0, controller);
		mHolder.put(weakReferencedController, id);
	}

	private UUID removeByObjectUnsafe(Object controller) {
		UUID correspondingUUID = null;
		WeakReference<Object> key = mController.getCorrespondingWeakReference(controller);
		if (null != key) {
			correspondingUUID = mHolder.get(key);
			mHolder.remove(key);
			mController.removeWeakReference(key);
		}
		return correspondingUUID;
	}

	private UUID removeByUUIDUnsafe(UUID uuid) {
		UUID correspondingUUID = null;
		WeakReference<Object> key = getWeakReferenceByUUID(uuid);
		if (null != key && null != key.get()) {
			correspondingUUID = uuid;
			mHolder.remove(key);
			mController.removeWeakReference(key);
		}
		return correspondingUUID;
	}

	private void stopcontrollingAllUUIDs() {
		for (Entry<WeakReference<Object>, UUID> entry : mHolder.entrySet()) {
			markUUIDBecomeLost(entry.getValue());
		}
	}

	private void markUUIDBecomeLost(UUID uuid) {
		mLostUUIDs.add(uuid);
	}

	private WeakReference<Object> getWeakReferenceByUUID(UUID uuid) {
		WeakReference<Object> resultWeakReference = null;
		for (Entry<WeakReference<Object>, UUID> entry : mHolder.entrySet()) {
			if (uuid.equals(entry.getValue())) {
				resultWeakReference = entry.getKey();
				break;
			}
		}
		return resultWeakReference;
	}

	private void invalidateUnsafe() {
		collectLostUUIDs();
		if (null != mDismissListener) {
			UUID[] dismissedUUIDs = mLostUUIDs.toArray(new UUID[0]);
			mDismissListener.onObjectDismissed(dismissedUUIDs);
		}
		mLostUUIDs.clear();
	}

	private void collectLostUUIDs() {
		for (Entry<WeakReference<Object>, UUID> entry : mHolder.entrySet()) {
			if (null == entry.getKey().get()) {
				markUUIDBecomeLost(entry.getValue());
			}
		}
	}

	private final WeakRefList<Object> mController = new WeakRefList<Object>();
	private final ConcurrentHashMap<WeakReference<Object>, UUID> mHolder = new ConcurrentHashMap<WeakReference<Object>, UUID>();
	private final List<UUID> mLostUUIDs = new LinkedList<UUID>();
	private DismissListener mDismissListener = null;
}
