package ra.db.ra;

import ra.dbengine.ISQLDatabase;
import ra.dbengine.ISQLDbProvider;

public class RaSQLiteDbProvider extends ISQLDbProvider {

	@Override
	protected ISQLDatabase open(String databasePath) {
		boolean success = false;
		ISQLDatabase dbRA = null;
		try {
			dbRA = RaSQLiteDatabase.openDatabase(databasePath);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		} finally {
			if (!success) {
				if (null != dbRA) {
					dbRA.close();
					dbRA = null;
				}
			}
		}
		return dbRA;
	}
}
