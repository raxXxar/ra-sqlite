package ra.x;

import ra.sqlite.NativeCursor;
import android.database.Cursor;

public class IndexAccessorRa extends IndexAccessor {

	IndexAccessorRa(Cursor cursor, String fastAccessColumnName) {
		super(cursor, fastAccessColumnName);
		if (!(cursor instanceof NativeCursor)) {
			throw new IllegalArgumentException("cursor should be instance of " + NativeCursor.class.getName());
		}
	}

	IndexAccessorRa(Cursor cursor) {
		this(cursor, DEF_FAST_ACCESS_COLUMN_NAME);
	}

	@Override
	public int locate(long indexValue) {
		return Locate(accessorId, indexValue);
	}

	@Override
	protected void deactivateInner() {
		accessorId = Deactivate(accessorId);
	}

	@Override
	protected boolean initInner() {
		accessorId = Init(((NativeCursor) cursor).getHandle(), fastAccessColumnName);
		return (ACCESSOR_UNDEFINED != accessorId);
	}

	private int accessorId;

	native protected int Deactivate(int accessorId);

	native protected int Init(int cursorHandle, String fastAccessColumnName);

	native protected int Locate(int accessorId, long indexValue);

	protected static final int ACCESSOR_UNDEFINED = 0;
}
