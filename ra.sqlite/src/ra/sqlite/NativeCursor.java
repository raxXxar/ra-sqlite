package ra.sqlite;

import android.database.Cursor;

public interface NativeCursor extends Cursor {

	public int getHandle();
}
