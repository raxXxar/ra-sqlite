#include <jni.h>
#include "sqlite3.h"
#include "ra_exception.h"
#include "ra_sqlite_SQLiteCursor.h"
#include "ra_sqlite_SQLiteCursor_.h"
#include "ra_sqlite_SQLiteDatabase_.h"
#include "ra_pool.h"
#include "ra_dbut.h"

#define __THROW_IF_ERROR THROW_SQL3(jEnv, err, sqlite3_db_handle(((Cursor*) cursorHandle)->pStatement))

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    moveTo
 * Signature: (II)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_moveTo(JNIEnv* jEnv,
                                                          jobject jSQLiteCursor,
                                                          jint cursorHandle,
                                                          jint position) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jint resultPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	jint err = Cursor_MoveToPosition(pCursor, position, &resultPosition);
	__THROW_IF_ERROR;
	return resultPosition;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnCount
 * Signature: (I)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnCount(JNIEnv* jEnv, jobject jSQLiteCursor, jint cursorHandle) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	return Cursor_GetColumnCount(pCursor);
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnType
 * Signature: (II)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnType(JNIEnv* jEnv,
                                                              jobject jSQLiteCursor,
                                                              jint cursorHandle,
                                                              jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	return Cursor_GetColumnType(pCursor, columnIndex);
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnIsNull
 * Signature: (II)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnIsNull(JNIEnv* jEnv,
                                                                jobject jSQLiteCursor,
                                                                jint cursorHandle,
                                                                jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	return Cursor_GetColumnIsNull(pCursor, columnIndex);
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnInt64Value
 * Signature: (II)J
 */

JNIEXPORT jlong JNICALL Java_ra_sqlite_SQLiteCursor_columnInt64Value(JNIEnv* jEnv,
                                                                     jobject jSQLiteCursor,
                                                                     jint cursorHandle,
                                                                     jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jlong resultValue = 0L;
	jint err = Cursor_GetColumnInt64(pCursor, columnIndex, &resultValue);
	switch (err) {
		case CURSOR_ERROR_CODE_UNKNOWN_TYPE:
			throw_sqlite3_exception_msg(jEnv, "Unknown type");
			break;
		case CURSOR_ERROR_CODE_BLOB_TO_LONG:
			throw_sqlite3_exception_msg(jEnv, "Unable to convert BLOB to long");
			break;
		default:
			break;
	}
	return resultValue;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnDoubleValue
 * Signature: (II)D
 */

JNIEXPORT jdouble JNICALL Java_ra_sqlite_SQLiteCursor_columnDoubleValue(JNIEnv* jEnv,
                                                                        jobject jSQLiteCursor,
                                                                        jint cursorHandle,
                                                                        jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jdouble resultValue = .0;
	jint err = Cursor_GetColumnDouble(pCursor, columnIndex, &resultValue);
	switch (err) {
		case CURSOR_ERROR_CODE_UNKNOWN_TYPE:
			throw_sqlite3_exception_msg(jEnv, "Unknown type");
			break;
		case CURSOR_ERROR_CODE_BLOB_TO_DOUBLE:
			throw_sqlite3_exception_msg(jEnv, "Unable to convert BLOB to double");
			break;
		default:
			break;
	}
	return resultValue;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnStringValue
 * Signature: (II)Ljava/lang/String;
 */

JNIEXPORT jstring JNICALL Java_ra_sqlite_SQLiteCursor_columnStringValue(JNIEnv* jEnv,
                                                                        jobject jSQLiteCursor,
                                                                        jint cursorHandle,
                                                                        jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jstring resultValue = NULL;
	jint err = Cursor_GetColumnString(pCursor, columnIndex, &resultValue, jEnv);
	switch (err) {
		case CURSOR_ERROR_CODE_UNKNOWN_TYPE:
			throw_sqlite3_exception_msg(jEnv, "Unknown type");
			break;
		case CURSOR_ERROR_CODE_BLOB_TO_STRING:
			throw_sqlite3_exception_msg(jEnv, "Unable to convert BLOB to string");
			break;
		default:
			break;
	}
	return resultValue;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnByteBufferValue
 * Signature: (II)[B
 */

JNIEXPORT jbyteArray JNICALL Java_ra_sqlite_SQLiteCursor_columnByteBufferValue(JNIEnv* jEnv,
                                                                               jobject jSQLiteCursor,
                                                                               jint cursorHandle,
                                                                               jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jbyteArray resultValue = NULL;
	jint err = Cursor_GetColumnBlob(pCursor, columnIndex, &resultValue, jEnv);
	switch (err) {
		case CURSOR_ERROR_CODE_UNKNOWN_TYPE:
			throw_sqlite3_exception_msg(jEnv, "Unknown type");
			break;
		case CURSOR_ERROR_CODE_INTEGER_TO_BLOB:
			throw_sqlite3_exception_msg(jEnv, "INTEGER data in GetBlob");
			break;
		case CURSOR_ERROR_CODE_FLOAT_TO_BLOB:
			throw_sqlite3_exception_msg(jEnv, "FLOAT data in GetBlob");
			break;
		default:
			break;
	}
	return resultValue;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnName
 * Signature: (II)Ljava/lang/String;
 */

JNIEXPORT jstring JNICALL Java_ra_sqlite_SQLiteCursor_columnName(JNIEnv* jEnv,
                                                                 jobject jSQLiteCursor,
                                                                 jint cursorHandle,
                                                                 jint columnIndex) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	return getStatementColumnName(jEnv, pCursor->pStatement, columnIndex);
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    rowsCount
 * Signature: (I)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_rowsCount(JNIEnv* jEnv, jobject jSQLiteCursor, jint cursorHandle) {
	Cursor* xx = (Cursor*) getInPool(cursorHandle);
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jint resultRowCount = ra_sqlite_SQLiteCursor_UNDEFINED;
	jint err = Cursor_GetRowCount(pCursor, &resultRowCount);
	__THROW_IF_ERROR;
	return resultRowCount;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    deactivateCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_deactivateCursor(JNIEnv* jEnv,
                                                                    jobject jSQLiteCursor,
                                                                    jint cursorHandle) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	Cursor_Deactivate(pCursor);
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    resetCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_resetCursor(JNIEnv* jEnv, jobject jSQLiteCursor, jint cursorHandle) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	jint err = Cursor_Reset(pCursor);
	__THROW_IF_ERROR;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    createCursor
 * Signature: (II)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_createCursor(JNIEnv* jEnv,
                                                                jobject jSQLiteCursor,
                                                                jint databaseHandle,
                                                                jint statementHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, databaseHandle);
	Cursor* pCursor = Cursor_Create(pDbh,
	                                (sqlite3_stmt*) statementHandle,
	                                CURSOR_ROWS_PER_EXTENT,
	                                CURSOR_BLOB_EXTENT_SIZE);
	return pCursor->g_poolId;
}

/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    destroyCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_destroyCursor(JNIEnv* jEnv, jobject jSQLiteCursor, jint cursorHandle) {
	Cursor* pCursor = getCursorPtrOrThrow(jEnv, cursorHandle);
	Array_RemoveEntry(pCursor->pDb->pCursors, pCursor, JNI_TRUE);
}
