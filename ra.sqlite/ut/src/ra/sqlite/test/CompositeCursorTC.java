package ra.sqlite.test;

import ra.sqlite.CantleCursorInfo;
import ra.sqlite.CompositeCursor;
import ra.sqlite.SQLiteDatabase;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.test.AndroidTestCase;

public class CompositeCursorTC extends AndroidTestCase {

	protected void setUp() throws Exception {
		super.setUp();
		db = SQLiteDatabase.openDatabase(DB_PATH_NAME);
		assertTrue(null != db);
		rowCount = TestHelper.setupTable(db, 100);
		Cursor master = db.rawQuery("SELECT i, t mText, f mFloat FROM test");
		assertNotNull(master);
		Cursor details = db.rawQuery("SELECT i, t dText, t dBlob , f dFloat FROM test WHERE i%2 = 1 ORDER BY i ASC");
		assertNotNull(details);
		CantleCursorInfo[] compositionInfo = new CantleCursorInfo[2];
		compositionInfo[0] = new CantleCursorInfo(master, "m");
		compositionInfo[1] = new CantleCursorInfo("i", details, "i", "d");
		composite = new CompositeCursor(compositionInfo);

		mIdx = composite.getColumnIndex("m.i");
		mText = composite.getColumnIndex("mText");
		mFloat = composite.getColumnIndex("mFloat");
		dIdx = composite.getColumnIndex("d.i");
		dText = composite.getColumnIndex("dText");
		dBlob = composite.getColumnIndex("dBlob");
		dFloat = composite.getColumnIndex("dFloat");
	}

	protected void tearDown() throws Exception {
		composite.close();
		rowCount = 0;
		composite = null;
		db.close();
		db = null;
		getContext().deleteDatabase(DB_PATH_NAME);
		super.tearDown();
	}

	public void testCompositeCursor() {
		assertNotNull(composite);
	}

	public void testGetCount() {
		assertEquals(rowCount, composite.getCount());
	}

	public void testGetPosition() {
		int position = composite.getPosition();
		assertEquals(-1, position);
		composite.moveToLast();
		position = composite.getPosition();
		assertEquals(rowCount - 1, position);
	}

	public void testMove() {
		int offset = (int) (rowCount / 2);
		composite.moveToFirst();
		int currentPosition = composite.getPosition();
		assertEquals(0, currentPosition);
		composite.move(offset);
		currentPosition = composite.getPosition();
		assertEquals(offset, currentPosition);
	}

	public void testMoveToPosition() {
		int newPosition = (int) (rowCount / 2);
		composite.moveToPosition(newPosition);
		int currentPosition = composite.getPosition();
		assertEquals(newPosition, currentPosition);
	}

	public void testMoveToFirst() {
		composite.moveToFirst();
		int currentPosition = composite.getPosition();
		assertEquals(0, currentPosition);
	}

	public void testMoveToLast() {
		composite.moveToLast();
		int currentPosition = composite.getPosition();
		assertEquals(rowCount - 1, currentPosition);
	}

	public void testMoveToNext() {
		int count = 0;
		while (composite.moveToNext()) {
			++count;
		}
		assertEquals(rowCount, count);
	}

	public void testMoveToPrevious() {
		int count = 0;
		composite.moveToLast();
		composite.moveToNext();
		assertEquals(true, composite.isAfterLast());
		while (composite.moveToPrevious()) {
			++count;
		}
		assertEquals(rowCount, count);
	}

	public void testIsFirst() {
		assertEquals(true, composite.isBeforeFirst());
		assertEquals(false, composite.isFirst());
		composite.moveToFirst();
		assertEquals(true, composite.isFirst());
		assertEquals(0, composite.getPosition());
	}

	public void testIsLast() {
		assertEquals(false, composite.isLast());
		composite.moveToLast();
		assertEquals(true, composite.isLast());
		assertEquals(rowCount - 1, composite.getPosition());
	}

	public void testIsBeforeFirst() {
		assertEquals(true, composite.isBeforeFirst());
		composite.moveToFirst();
		assertEquals(false, composite.isBeforeFirst());
		composite.moveToPrevious();
		assertEquals(true, composite.isBeforeFirst());
		assertEquals(-1, composite.getPosition());
	}

	public void testIsAfterLast() {
		assertEquals(false, composite.isAfterLast());
		composite.moveToLast();
		assertEquals(false, composite.isAfterLast());
		composite.moveToNext();
		assertEquals(true, composite.isAfterLast());
		assertEquals(rowCount, composite.getPosition());
	}

	public void testGetColumnIndex() {
		assertEquals(mIdx, composite.getColumnIndex("i"));
		assertEquals(mText, composite.getColumnIndex("mText"));
		assertEquals(dIdx, composite.getColumnIndex("d.i"));
		assertEquals(dText, composite.getColumnIndex("dText"));
		assertEquals(dBlob, composite.getColumnIndex("dBlob"));
	}

	public void testGetColumnIndexOrThrow() {
		assertEquals(mIdx, composite.getColumnIndex("i"));
		assertEquals(mText, composite.getColumnIndex("mText"));
		assertEquals(dIdx, composite.getColumnIndex("d.i"));
		assertEquals(dText, composite.getColumnIndex("dText"));
		assertEquals(dBlob, composite.getColumnIndex("dBlob"));
		boolean wasException = false;
		try {
			composite.getColumnIndexOrThrow("FakeField");
		} catch (Exception e) {
			assertEquals(true, e instanceof IllegalArgumentException);
			wasException = true;
		}
		assertEquals(true, wasException);
	}

	public void testGetColumnName() {
		assertEquals("m.i", composite.getColumnName(mIdx));
		assertEquals("m.mtext", composite.getColumnName(mText));
		assertEquals("d.i", composite.getColumnName(dIdx));
		assertEquals("d.dtext", composite.getColumnName(dText));
		assertNull(composite.getColumnName(-1));
		assertNull(composite.getColumnName(20));
	}

	public void testGetColumnNames() {
		String[] columnNames = composite.getColumnNames();
		assertNotNull(columnNames);
		assertEquals(dFloat + 1, columnNames.length);
	}

	public void testGetColumnCount() {
		int colCount = composite.getColumnCount();
		assertEquals(dFloat + 1, colCount);
	}

	public void testGetBlob() {
		assertEquals(true, composite.moveToFirst());
		byte[] buf = null;
		boolean wasException = false;
		try {
			buf = composite.getBlob(dBlob);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		buf = composite.getBlob(dBlob);
		assertNotNull(buf);
	}

	public void testGetString() {
		assertEquals(true, composite.moveToFirst());
		String string = null;
		boolean wasException = false;
		try {
			string = composite.getString(dText);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		string = composite.getString(dText);
		assertNotNull(string);
		String str = composite.getString(mText);
		assertEquals(str, string);
	}

	public void testCopyStringToBuffer() {
		assertEquals(true, composite.moveToFirst());
		android.database.CharArrayBuffer buffer = new CharArrayBuffer(0);
		composite.copyStringToBuffer(mText, buffer);
		String resStr = new String(buffer.data);
		String str = "TEXT 0";
		assertEquals(0, resStr.compareTo(str));
	}

	public void testGetShort() {
		assertEquals(true, composite.moveToFirst());
		Short shortValue = null;
		boolean wasException = false;
		try {
			shortValue = composite.getShort(dIdx);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		shortValue = composite.getShort(dIdx);
		assertNotNull(shortValue);
		Short shortVal = composite.getShort(mIdx);
		assertEquals(shortVal, shortValue);
	}

	public void testGetInt() {
		assertEquals(true, composite.moveToFirst());
		Integer intValue = null;
		boolean wasException = false;
		try {
			intValue = composite.getInt(dIdx);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		intValue = composite.getInt(dIdx);
		assertNotNull(intValue);
		Integer intVal = composite.getInt(mIdx);
		assertEquals(intVal, intValue);
	}

	public void testGetLong() {
		assertEquals(true, composite.moveToFirst());
		Long longValue = null;
		boolean wasException = false;
		try {
			longValue = composite.getLong(dIdx);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		longValue = composite.getLong(dIdx);
		assertNotNull(longValue);
		Long longVal = composite.getLong(mIdx);
		assertEquals(longVal, longValue);
	}

	public void testGetFloat() {
		assertEquals(true, composite.moveToFirst());
		Float floatValue = null;
		boolean wasException = false;
		try {
			floatValue = composite.getFloat(dFloat);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		floatValue = composite.getFloat(dFloat);
		assertNotNull(floatValue);
		Float floatVal = composite.getFloat(mFloat);
		assertEquals(floatVal, floatValue);
	}

	public void testGetDouble() {
		assertEquals(true, composite.moveToFirst());
		Double doubleValue = null;
		boolean wasException = false;
		try {
			doubleValue = composite.getDouble(dFloat);
		} catch (SQLiteException e) {
			wasException = true;
		}
		assertEquals(true, wasException);
		assertEquals(true, composite.moveToNext());
		doubleValue = composite.getDouble(dFloat);
		assertNotNull(doubleValue);
		Double doubleVal = composite.getDouble(mFloat);
		assertEquals(doubleVal, doubleValue);
	}

	public void testIsNull() {
		assertEquals(true, composite.moveToFirst());
		Boolean isNullValue = composite.isNull(dFloat);
		assertEquals(Boolean.TRUE, isNullValue);
		assertEquals(true, composite.moveToNext());
		isNullValue = composite.isNull(dFloat);
		assertEquals(Boolean.FALSE, isNullValue);
	}

	@SuppressWarnings("deprecation")
	public void testDeactivate() {
		assertEquals(false, composite.isClosed());
		boolean isOk = false;
		try {
			composite.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		composite.deactivate();
		boolean failed = false;
		try {
			composite.getCount();
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		composite.requery();
		isOk = false;
		try {
			composite.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		composite.close();
		assertEquals(true, composite.isClosed());
	}

	@SuppressWarnings("deprecation")
	public void testRequery() {
		int count1 = -1;
		int count2 = -1;
		assertEquals(false, composite.isClosed());
		boolean isOk = false;
		try {
			count1 = composite.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		composite.deactivate();
		boolean failed = false;
		try {
			composite.getCount();
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		composite.requery();
		isOk = false;
		try {
			count2 = composite.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		assertEquals(rowCount, count1);
		assertEquals(rowCount, count2);
		assertEquals(count1, count2);
	}

	public void testClose() {
		composite.close();
		assertEquals(true, composite.isClosed());
	}

	public void testIsClosed() {
		assertEquals(false, composite.isClosed());
		composite.close();
		assertEquals(true, composite.isClosed());
	}

	private Cursor composite = null;
	private long rowCount = 0;

	private int mIdx;
	private int mText;
	private int mFloat;
	private int dIdx;
	private int dText;
	private int dBlob;
	private int dFloat;

	private SQLiteDatabase db = null;
	private static final String DB_NAME = "test_fake.db";
	private static final String DB_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private static final String DB_PATH_NAME = DB_PATH + "/" + DB_NAME;
}
