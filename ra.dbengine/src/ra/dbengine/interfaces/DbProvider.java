package ra.dbengine.interfaces;

import ra.db.DbConfig;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;

/**
 *  @author =ra=
 */

public interface DbProvider<T> {

	public void setConfig(DbConfig config);

	public DbConfig getConfig();

	public T get() throws DbIncorrectVersionException, DbOpenErrorException;

	public T get(String databasePath) throws DbIncorrectVersionException, DbOpenErrorException;

	public boolean isDbVersionOk(T db, int dbExpectedSchemaVersion);
}
