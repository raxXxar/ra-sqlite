package ra.dbengine.interfaces;

/**
 *  @author =ra=
 */
public interface ChangeListener {
	void onChanged(Object inciter);
}
