package ra.dbengine.exceptions;

/**
 *  @author =ra=
 */
public class DbEngineProviderException extends Exception {

	private static final long serialVersionUID = 3953583185098766758L;

	public DbEngineProviderException() {
		super("Some database engine exception");
	}

	public DbEngineProviderException(String message) {
		super(message);
	}
}
