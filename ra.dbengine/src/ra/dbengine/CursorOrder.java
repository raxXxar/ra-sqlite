package ra.dbengine;

import java.util.UUID;
import ra.dbengine.interfaces.SqlCommandSource;

/**
 * The Class CursorOrder is used to request DbEngine to do some job - 
 * execute one of the following types of statement:
 * 		a) SQL
 * 		b) DDL
 * 		c) DML
 */
/**
*  @author =ra=
*/
public class CursorOrder {

	/**
	 * Instantiates a new cursor order.
	 *
	 * @param sqlCommandProvider the SQL command provider
	 */
	public CursorOrder(SqlCommandSource sqlCommandProvider) {
		mSqlCommandProvider = sqlCommandProvider;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public UUID getUUID() {
		if (null == mUUID) {
			mUUID = UUID.randomUUID();
		}
		return mUUID;
	}

	/**
	 * Gets the SQL command provider.
	 *
	 * @return the SQL command provider
	 */
	public SqlCommandSource getCmdProvider() {
		return mSqlCommandProvider;
	}

	public final void setUsed() {
		mUsed = true;
	}

	public final boolean getUsed() {
		return mUsed;
	}

	/**
	 * Re initialize cursor order with new uuid and SQL command provider.
	 *
	 * @param uuid the uuid
	 * @param sqlCommandProvider the SQL command provider
	 */
	protected void reInit(UUID uuid, SqlCommandSource sqlCommandProvider) {
		mUUID = uuid;
		mSqlCommandProvider = sqlCommandProvider;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	protected void setUUID(UUID uuid) {
		mUUID = uuid;
	}

	/**
	 * Sets the SQL command provider.
	 *
	 * @param sqlCommandProvider the new SQL command provider
	 */
	protected void setCmdProvider(SqlCommandSource sqlCommandProvider) {
		mSqlCommandProvider = sqlCommandProvider;
	}

	/** The UUID used to identify concrete cursor SQL command source and their listeners in cpcEngine. */
	private UUID mUUID = null;

	/** The SQL command source provider. */
	private SqlCommandSource mSqlCommandProvider;

	private boolean mUsed = false;
}
