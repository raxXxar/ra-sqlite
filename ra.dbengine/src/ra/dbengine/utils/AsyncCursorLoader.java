package ra.dbengine.utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

/**
 * The Class AsyncCursorLoader allows cursor to be loaded asynchronously.
 */
/**
 *  @author =ra=
 */
public abstract class AsyncCursorLoader extends AsyncTaskLoader<Cursor> {

	/**
	 * Instantiates a new async cursor loader.
	 *
	 * @param context the context
	 */
	public AsyncCursorLoader(Context context) {
		super(context);
	}

	/* Runs on a worker thread */
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.content.AsyncTaskLoader#loadInBackground()
	 */
	@Override
	public abstract Cursor loadInBackground();

	/* Runs on the UI thread */
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.content.Loader#deliverResult(java.lang.Object)
	 */
	@Override
	public void deliverResult(Cursor cursor) {
		if (isReset()) {
			// An async query came in while the mAsyncTaskLoader is stopped
			if (cursor != null) {
				cursor.close();
			}
			return;
		}
		Cursor oldCursor = mCursor;
		mCursor = cursor;

		if (isStarted()) {
			super.deliverResult(cursor);
		}

		if (oldCursor != null && oldCursor != cursor && !oldCursor.isClosed()) {
			oldCursor.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.content.AsyncTaskLoader#onCanceled(java.lang.Object)
	 */
	@Override
	public void onCanceled(Cursor cursor) {
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.content.Loader#onReset()
	 */
	@Override
	protected void onReset() {
		super.onReset();
		// Ensure the mAsyncTaskLoader is stopped
		onStopLoading();
		if (mCursor != null && !mCursor.isClosed()) {
			mCursor.close();
		}
		mCursor = null;
	}

	/**
	 * Starts an asynchronous load of the data. When the result is ready, the callback
	 * will be called on the UI thread. If a previous load has been completed and is still valid
	 * the result may be passed to the callback immediately.
	 * <p>
	 * Must be called from the UI thread
	 */
	@Override
	protected void onStartLoading() {
		if (mCursor != null) {
			deliverResult(mCursor);
		}
		if (takeContentChanged() || mCursor == null) {
			forceLoad();
		}
	}

	/**
	 * Must be called from the UI thread.
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/** The Cursor. */
	private Cursor mCursor;

}
