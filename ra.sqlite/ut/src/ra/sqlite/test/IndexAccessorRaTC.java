package ra.sqlite.test;

import ra.sqlite.SQLiteDatabase;
import ra.x.AccessorFactory;
import ra.x.IndexAccessor;
import android.database.Cursor;
import android.os.Environment;
import android.test.AndroidTestCase;

public class IndexAccessorRaTC extends AndroidTestCase {

	protected void setUp() throws Exception {
		super.setUp();
		db = SQLiteDatabase.openDatabase(DB_PATH_NAME);
		assertTrue(null != db);
	}

	protected void tearDown() throws Exception {
		db.close();
		db = null;
		getContext().deleteDatabase(DB_PATH_NAME);
		super.tearDown();
	}

	public void testDefaultAccessorColumnLocate() {
		TestHelper.setupTable(db, 511);
		Cursor cursor = db.rawQuery("SELECT i, i pk$join$column FROM test");
		assertNotNull(cursor);
		IndexAccessor accesor = AccessorFactory.createAccessor(cursor);
		assertNotNull(accesor);
		int position = accesor.locate(10);
		assertEquals(10, position);
		position = accesor.locate(2000);
		assertEquals(-1, position);
		position = accesor.locate(-2000);
		assertEquals(-1, position);
		cursor.close();
	}

	public void testCustomAccessorColumnLocate() {
		TestHelper.setupTable(db, 511);
		Cursor cursor = db.rawQuery("SELECT i%10 value, 1 z,  i+1 newIndex FROM test");
		assertNotNull(cursor);
		IndexAccessor accesor = AccessorFactory.createAccessor(cursor, "newIndex");
		assertNotNull(accesor);
		int position = accesor.locate(10);
		assertEquals(9, position);
		position = accesor.locate(11);
		assertEquals(10, position);
		position = accesor.locate(2000);
		assertEquals(-1, position);
		position = accesor.locate(-2000);
		assertEquals(-1, position);
		cursor.close();
	}

	public void testSwitchAccessorColumnLocate() {
		TestHelper.setupTable(db, 511);
		Cursor cursor = db.rawQuery("SELECT i%10 value, i pk$join$column, i+1 newIndex FROM test");
		assertNotNull(cursor);
		IndexAccessor accesor = AccessorFactory.createAccessor(cursor, "newIndex");
		assertNotNull(accesor);
		int position = accesor.locate(10);
		assertEquals(9, position);
		position = accesor.locate(2000);
		assertEquals(-1, position);
		position = accesor.locate(-2000);
		assertEquals(-1, position);
		cursor.close();
	}

	private SQLiteDatabase db = null;
	private static final String DB_NAME = "test_fake.db";
	private static final String DB_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private static final String DB_PATH_NAME = DB_PATH + "/" + DB_NAME;
}
