package ra.dbengine.processors;

import ra.dbengine.ISQLDatabase;
import ra.dbengine.QProcessor;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import ra.dbengine.interfaces.DbProvider;
import android.database.Cursor;
import android.database.sqlite.SQLiteTransactionListener;

public class QRaSQLiteProcessorImpl extends QProcessor {

	@Override
	public void exec(String cmd) {
		if (mIsDbOpened) {
			mDb.execSQL(cmd);
		}
	}

	@Override
	public void exec(String cmd, Object[] bindArgs) {
		if (mIsDbOpened) {
			if (null == bindArgs) {
				bindArgs = new Object[] {};
			}
			mDb.execSQL(cmd, bindArgs);
		}
	}

	@Override
	public void execBlock(String[] cmds) {
		if (mIsDbOpened) {
			for (String cmd : cmds) {
				mDb.execSQL(cmd);
			}
		}
	}

	@Override
	public boolean inTransaction() {
		return (mIsDbOpened) ? mDb.inTransaction() : false;
	}

	@Override
	public void beginTransaction() {
		if (mIsDbOpened) {
			mDb.beginTransaction();
		}
	}

	@Override
	public void endTransaction() {
		if (mIsDbOpened) {
			mDb.endTransaction();
		}
	}

	@Override
	public void setTransactionSuccessful() {
		if (mIsDbOpened) {
			mDb.setTransactionSuccessful();
		}
	}

	@Override
	public void beginTransactionWithListener(SQLiteTransactionListener listener) {
		if (mIsDbOpened) {
			mDb.beginTransactionWithListener(listener);
		}
	}

	@Override
	protected Cursor fetch(String cmd) {
		return (mIsDbOpened) ? mDb.rawQuery(cmd, null) : null;
	}

	@Override
	protected void stop() {
		if (mIsDbOpened) {
			if (mDb != null) {
				mDb.close();
				mDb = null;
			}
			mIsDbOpened = false;
		}
	}

	@Override
	protected void start() {
		mIsDbOpened = (null != mDb) && mDb.isOpen();
	}

	@Override
	protected void setDbProvider(DbProvider<?> dbProvider) throws DbIncorrectVersionException, DbOpenErrorException {
		mDb = (ISQLDatabase) dbProvider.get();
		mIsDbOpened = mIsDbOpened && (null != mDb) && (mDb.isOpen());
	}

	private boolean mIsDbOpened = false;
	private ISQLDatabase mDb;

	@Override
	@Deprecated
	public ISQLDatabase getISQLiteConcreteDb() {
		return mDb;
	}

}
