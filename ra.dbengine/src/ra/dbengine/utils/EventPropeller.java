package ra.dbengine.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import ra.dbengine.interfaces.ChangeListener;

/**
 * The class EventPropeller helps to manage notifications and content/data change events between interested objects.   
 */
/**
 *  @author =ra=
 */
public class EventPropeller {

	public EventPropeller() {
		mTagMap = new HashMap<String, List<UUID>>();
		mTagListenersMap = new HashMap<String, WeakRefList<ChangeListener>>();
		mUUIDListenersMap = new HashMap<UUID, WeakRefList<ChangeListener>>();
	}

	public void notifyChanged(Object inciter, String[] inciterTags) {
		synchronized (this) {
			notifyChangedUnsafe(inciter, inciterTags);
		}
	}

	public void notifyChanged(String[] inciterTags) {
		notifyChanged(null);
	}

	public void notifyChangedUUID(UUID uuid) {
		synchronized (this) {
			notifyChangedUUIDUnsafe(uuid);
		}
	}

	public ChangeListener[] getAffected(String[] inciterTags) {
		synchronized (this) {
			return getAffectedUnsafe(inciterTags);
		}
	}

	public void subscribeOnTag(String strTag, ChangeListener listener) {
		synchronized (this) {
			subscribeOnTagUnsafe(strTag, listener);
		}
	}

	public void unSubscribeOnTag(String strTag, ChangeListener listener) {
		synchronized (this) {
			unSubscribeOnTagUnsafe(strTag, listener);
		}
	}

	public void subscribeOnUUID(UUID uuidTag, ChangeListener listener) {
		synchronized (this) {
			subscribeOnUUIDUnsafe(uuidTag, listener);
		}
	}

	public void unSubscribeOnUUID(UUID uuidTag, ChangeListener listener) {
		synchronized (this) {
			unSubscribeOnUUIDUnsafe(uuidTag, listener);
		}
	}

	public void registerNotification(UUID uuid, String[] affectingTags) {
		synchronized (this) {
			registerNotificationUnsafe(uuid, affectingTags);
		}
	}

	public void unRegisterNotification(UUID uuid, String[] affectingTags) {
		synchronized (this) {
			unRregisterNotificationUnsafe(uuid, affectingTags);
		}
	}

	private void subscribeOnUUIDUnsafe(UUID uuidTag, ChangeListener listener) {
		WeakRefList<ChangeListener> subscription = prepareUUIDSubscription(uuidTag);
		subscription.add(listener);
	}

	private final WeakRefList<ChangeListener> prepareUUIDSubscription(UUID uuidTag) {
		WeakRefList<ChangeListener> subscription = mUUIDListenersMap.get(uuidTag);
		if (null == subscription) {
			subscription = new WeakRefList<ChangeListener>();
			mUUIDListenersMap.put(uuidTag, subscription);
		}
		return subscription;
	}

	private void subscribeOnTagUnsafe(String strTag, ChangeListener listener) {
		WeakRefList<ChangeListener> subscription = prepareTagSubscription(strTag);
		subscription.add(listener);
	}

	private final WeakRefList<ChangeListener> prepareTagSubscription(String strTag) {
		WeakRefList<ChangeListener> subscription = mTagListenersMap.get(strTag);
		if (null == subscription) {
			subscription = new WeakRefList<ChangeListener>();
			mTagListenersMap.put(strTag, subscription);
		}
		return subscription;
	}

	private final void unSubscribeOnTagUnsafe(String strTag, ChangeListener listener) {
		WeakRefList<ChangeListener> subscription = mTagListenersMap.get(strTag);
		unsubscribe(subscription, listener);
		if (null != subscription && 0 == subscription.size()) {
			mTagListenersMap.remove(strTag);
		}
	}

	private final void unSubscribeOnUUIDUnsafe(UUID uuidTag, ChangeListener listener) {
		WeakRefList<ChangeListener> subscription = mUUIDListenersMap.get(uuidTag);
		unsubscribe(subscription, listener);
		if (null != subscription && 0 == subscription.size()) {
			mUUIDListenersMap.remove(uuidTag);
		}
	}

	private final void unsubscribe(WeakRefList<ChangeListener> subscription, ChangeListener listener) {
		if (null != subscription) {
			subscription.remove(listener);
		}
	}

	private void registerNotificationUnsafe(UUID uuid, String[] affectingTags) {
		if (null != affectingTags) {
			for (String tag : affectingTags) {
				prepareTagUUIDList(tag).add(uuid);
			}
		}
	}

	private final List<UUID> prepareTagUUIDList(String tag) {
		List<UUID> list = mTagMap.get(tag);
		if (null == list) {
			list = new LinkedList<UUID>();
			mTagMap.put(tag, list);
		}
		return list;
	}

	private void unRregisterNotificationUnsafe(UUID uuid, String[] affectingTags) {
		if (null != affectingTags) {
			for (String tag : affectingTags) {
				List<UUID> list = mTagMap.get(tag);
				if (null != list) {
					list.remove(uuid);
					if (0 == list.size()) {
						mTagMap.remove(tag);
					}
				}
			}
		}
	}

	private ChangeListener[] getAffectedUnsafe(String[] inciterTags) {
		Set<ChangeListener> listenersSet = new HashSet<ChangeListener>();
		Set<UUID> uuidSet = new HashSet<UUID>();
		for (String tag : inciterTags) {
			// handle directly tagged listeners
			WeakRefList<ChangeListener> tagSubscription = mTagListenersMap.get(tag.toUpperCase());
			if (null != tagSubscription) {
				for (ChangeListener listener : (ChangeListener[]) tagSubscription.asArray(new ChangeListener[0])) {
					listenersSet.add(listener);
				}
				if (tagSubscription.size() == 0) {
					mTagListenersMap.remove(tag);
				}
			}
			// pick up tagged UUIDs
			List<UUID> uuidList = mTagMap.get(tag);
			if (null != uuidList) {
				uuidSet.addAll(uuidList);
				if (uuidList.size() == 0) {
					mTagMap.remove(tag);
				}
			}
		}
		// handle uuid "tagged" listeners
		for (UUID uuid : uuidSet) {
			WeakRefList<ChangeListener> tagSubscription = mUUIDListenersMap.get(uuid);
			if (null != tagSubscription) {
				for (ChangeListener listener : tagSubscription.asArray(new ChangeListener[0])) {
					listenersSet.add(listener);
				}
				if (tagSubscription.size() == 0) {
					mUUIDListenersMap.remove(uuid);
				}
			}
		}
		return listenersSet.toArray(new ChangeListener[0]);
	}

	private void notifyChangedUnsafe(Object inciter, String[] inciterTags) {
		for (ChangeListener item : getAffected(inciterTags)) {
			if (item != inciter) {
				item.onChanged(inciter);
			}
		}
	}

	private void notifyChangedUUIDUnsafe(UUID uuid) {
		WeakRefList<ChangeListener> listeners = mUUIDListenersMap.get(uuid);
		if (null != listeners) {
			for (ChangeListener listener : listeners.asArray(new ChangeListener[0])) {
				listener.onChanged(null);
			}
		}
	}

	private final HashMap<String, List<UUID>> mTagMap;
	private final HashMap<String, WeakRefList<ChangeListener>> mTagListenersMap;
	private final HashMap<UUID, WeakRefList<ChangeListener>> mUUIDListenersMap;
}
