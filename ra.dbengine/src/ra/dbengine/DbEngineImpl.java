package ra.dbengine;

import java.lang.ref.WeakReference;
import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import ra.db.DbConfig;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import ra.dbengine.interfaces.ChangeListener;
import ra.dbengine.interfaces.CursorTechHole;
import ra.dbengine.interfaces.DbDisconnectingListener;
import ra.dbengine.interfaces.DbEngine;
import ra.dbengine.interfaces.DbProvider;
import ra.dbengine.interfaces.QProcessorProvider;
import ra.dbengine.interfaces.SqlCommandSource;
import ra.dbengine.utils.AsyncCursorLoader;
import ra.dbengine.utils.EventPropeller;
import ra.dbengine.utils.LifeCycleController;
import ra.dbengine.utils.LifeCycleController.DismissListener;
import ra.dbengine.utils.LoaderManagerX;
import ra.dbengine.utils.NullCursor;
import ra.dbengine.utils.WeakRefList;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;

/**
 * DbEngineImpl DbEngine interface implementation.
 * 
 * <br>Please take a look to {@link DbEngine} methods description for more info. 
 */
/**
*  @author =ra=
*/

public class DbEngineImpl implements DbEngine {

	public DbEngineImpl(DbConfig dbCfg) {
		mContext = dbCfg.getContext();
		mDbProvider = dbCfg.getDBProvider(mContext);
		mQProcessorProvider = dbCfg.getQProcessorProvider();
		mLoaderManager = new LoaderManagerX();
		mIsEngineFrozen = true;
	}

	@Override
	public final Cursor createCursor(CursorOrder cursorOrder) {
		synchronized (this) {
			ensureNotUsed(cursorOrder);
			return createCursorOrder(cursorOrder);
		}
	}

	@Override
	public final Cursor openCursorA(CursorOrder cursorOrder) {
		synchronized (this) {
			ensureNotUsed(cursorOrder);
			return registerCursorOrder(cursorOrder);
		}
	}

	@Override
	public final Cursor openCursorA(CursorOrder cursorOrder, String lockKey) {
		synchronized (this) {
			ensureNotUsed(cursorOrder);
			Cursor result = unlockCursorUnsafe(cursorOrder, lockKey);
			if (null == result) {
				result = registerCursorOrder(cursorOrder);
			}
			return result;
		}
	}

	@Override
	public final Cursor openCursor(CursorOrder cursorOrder) {
		synchronized (this) {
			ensureNotUsed(cursorOrder);
			return getCursorUnsafe(cursorOrder);
		}
	}

	@Override
	public final Cursor openCursor(CursorOrder cursorOrder, String lockKey) {
		synchronized (this) {
			ensureNotUsed(cursorOrder);
			Cursor result = unlockCursorUnsafe(cursorOrder, lockKey);
			if (null == result) {
				result = getCursorUnsafe(cursorOrder);
			}
			return result;
		}
	}

	@Override
	public void lockCursor(UUID uuid, String lockKey) {
		synchronized (this) {
			lockCursorUnsafe(uuid, lockKey);
		}
	}

	@Override
	public void changeCursorA(CursorOrder cursorOrder) {
		synchronized (this) {
			mCursorLifeController.invalidate();
			UUID uuid = cursorOrder.getUUID();
			CursorTechHole techHole = mProxies.get(uuid);
			if (null != techHole) {
				SqlCommandSource oldCmdSrc = mCommandSources.remove(uuid);
				if (null != oldCmdSrc) {
					mEventPropeller.unRegisterNotification(uuid, oldCmdSrc.getNotificationTags());
				}
				SqlCommandSource newCmdprovider = cursorOrder.getCmdProvider();
				mCommandSources.put(uuid, newCmdprovider);
				mEventPropeller.registerNotification(uuid, newCmdprovider.getNotificationTags());
				fireCursorLoader(uuid);
			}
		}
	}

	@Override
	public void changeCursor(CursorOrder cursorOrder) {
		synchronized (this) {
			mCursorLifeController.invalidate();
			UUID uuid = cursorOrder.getUUID();
			CursorTechHole techHole = mProxies.get(uuid);
			if (null != techHole) {
				SqlCommandSource oldCmdSrc = mCommandSources.remove(uuid);
				if (null != oldCmdSrc) {
					mEventPropeller.unRegisterNotification(uuid, oldCmdSrc.getNotificationTags());
				}
				SqlCommandSource newCmdprovider = cursorOrder.getCmdProvider();
				mCommandSources.put(uuid, newCmdprovider);
				mEventPropeller.registerNotification(uuid, newCmdprovider.getNotificationTags());
				techHole.replaceCursorWith(getCursorUnsafe(cursorOrder));
				techHole.notifyDataChanged();
			}
		}
	}

	@Override
	public void execute(SqlCommandSource cmdSrc) {
		if (!mIsEngineFrozen) {
			mProcessor.exec(cmdSrc.getSqlCommand());
			ChangeListener[] listeners;
			synchronized (this) {
				listeners = mEventPropeller.getAffected(cmdSrc.getNotificationTags());
			}
			for (ChangeListener listener : listeners) {
				listener.onChanged(cmdSrc);
			}
		}
	}

	@Override
	public void executeBlock(SqlCommandSource cmdSrc) {
		if (!mIsEngineFrozen) {
			mProcessor.execBlock(cmdSrc.getSqlCommandBlock());
			ChangeListener[] listeners;
			synchronized (this) {
				listeners = mEventPropeller.getAffected(cmdSrc.getNotificationTags());
			}
			for (ChangeListener listener : listeners) {
				listener.onChanged(cmdSrc);
			}
		}
	}

	@Override
	public QProcessor getProcessor() {
		return mProcessor;
	}

	@Override
	public void dbDisconnect() {
		synchronized (this) {
			mIsEngineFrozen = true;
			// in-progress loaders will be finished with NullCursor 
			mLoaderManager.doStop();
			mLoaderManager.doDestroy();
			notifyDbDisconnecting();
			// close all opened cursors, replace them with NullCursor
			for (UUID uuid : mCursors.keySet()) {
				connectCursor(uuid, new NullCursor());
			}
			// close database
			mProcessor.stop();
		}
	}

	@Override
	public void dbConnect() throws DbIncorrectVersionException, DbOpenErrorException {
		synchronized (this) {
			mProcessor = mQProcessorProvider.get();
			mProcessor.setDbProvider(mDbProvider);
			mProcessor.start();
			mIsEngineFrozen = false;
			// requery all active command sources
			for (UUID uuid : mCommandSources.keySet()) {
				fireCursorLoader(uuid);
			}
		}
	}

	@Override
	public void registerCursorDataChangeListener(CursorOrder cursorOrder, ChangeListener changeListener) {
		synchronized (this) {
			if (null != cursorOrder) {
				mEventPropeller.subscribeOnUUID(cursorOrder.getUUID(), changeListener);
			}
		}
	}

	@Override
	public void unRegisterCursorDataChangeListener(CursorOrder cursorOrder, ChangeListener changeListener) {
		synchronized (this) {
			if (null != cursorOrder) {
				mEventPropeller.unSubscribeOnUUID(cursorOrder.getUUID(), changeListener);
			}
		}
	}

	@SuppressLint("DefaultLocale")
	@Override
	public void registerTagChangeListener(String[] inciterTags, ChangeListener changeListener) {
		synchronized (this) {
			if (null != inciterTags) {
				for (String strTag : inciterTags) {
					mEventPropeller.subscribeOnTag(strTag.toUpperCase(), changeListener);
				}
			}
		}
	}

	@Override
	public void unRegisterTagChangeListener(String[] inciterTags, ChangeListener changeListener) {
		synchronized (this) {
			if (null != inciterTags) {
				for (String strTag : inciterTags) {
					mEventPropeller.unSubscribeOnTag(strTag, changeListener);
				}
			}
		}
	}

	@Override
	public void registerDbDisconnectingListener(DbDisconnectingListener disconnectingListener) {
		synchronized (this) {
			if (disconnectingListener != null) {
				mDbDisconnectingListeners.add(disconnectingListener);
			}
		}
	}

	@Override
	public void unRegisterDbDisconnectingListener(DbDisconnectingListener disconnectingListener) {
		synchronized (this) {
			if (disconnectingListener != null) {
				mDbDisconnectingListeners.remove(disconnectingListener);
			}
		}
	}

	protected void releaseAllDismissed(UUID[] lostIdsList) {
		synchronized (this) {
			for (UUID uuid : lostIdsList) {
				releaseDismissed(uuid);
			}
		}
	}

	private void notifyDbDisconnecting() {
		for (DbDisconnectingListener listener : mDbDisconnectingListeners.asArray(new DbDisconnectingListener[0])) {
			listener.onDbDisconnecting();
		}
	}

	private void lockCursorUnsafe(UUID uuid, String lockKey) {
		CursorTechHole proxy = mProxies.get(uuid);
		if (null != proxy) {
			mLockedProxies.put(lockKey, proxy);
		}
	}

	private Cursor unlockCursorUnsafe(CursorOrder cursorOrder, String lockKey) {
		Cursor result = null;
		CursorTechHole proxy = mLockedProxies.get(lockKey);
		if (null != proxy) {
			result = proxy.getCursor();
			UUID uuid = (proxy.getUUID());
			cursorOrder.reInit(uuid, mCommandSources.get(uuid));
			mLockedProxies.remove(lockKey);
		}
		return result;
	}

	private Cursor createCursorOrder(CursorOrder cursorOrder) {
		UUID uuid = cursorOrder.getUUID();
		CursorTechHole proxy;
		if (null != uuid) {
			proxy = mProxies.get(uuid);
			if (null == proxy) {
				proxy = createNewProxy(cursorOrder);
			} else {
				proxy.replaceCursorWith(new NullCursor());
			}
		} else {
			uuid = UUID.randomUUID();
			cursorOrder.setUUID(uuid);
			proxy = createNewProxy(cursorOrder);
		}
		return proxy.getCursor();
	}

	private Cursor registerCursorOrder(CursorOrder cursorOrder) {
		UUID uuid = cursorOrder.getUUID();
		CursorTechHole proxy;
		if (null != uuid) {
			proxy = mProxies.get(uuid);
			if (null != proxy) {
				SqlCommandSource pCmdSrc = mCommandSources.get(uuid);
				SqlCommandSource oCmdSrc = cursorOrder.getCmdProvider();
				if (null != pCmdSrc && null != oCmdSrc) {
					String cmdSrcP = pCmdSrc.getSqlCommand();
					String cmdSrcO = oCmdSrc.getSqlCommand();
					if (!(null != cmdSrcP && cmdSrcP.equals(cmdSrcO))) {
						fireCursorLoader(uuid);
					}
				} else {
					fireCursorLoader(uuid);
				}
			} else {
				proxy = createNewProxy(cursorOrder);
				fireCursorLoader(uuid);
			}
		} else {
			uuid = UUID.randomUUID();
			cursorOrder.setUUID(uuid);
			proxy = createNewProxy(cursorOrder);
			fireCursorLoader(uuid);
		}
		return proxy.getCursor();
	}

	private Cursor getCursorUnsafe(CursorOrder cursorOrder) {
		CursorTechHole proxy = createNewProxy(cursorOrder);
		if (!mIsEngineFrozen) {
			SqlCommandSource sqlCmdSrc = cursorOrder.getCmdProvider();
			if (null != sqlCmdSrc) {
				Cursor cursor = mProcessor.fetch(sqlCmdSrc.getSqlCommand());
				if (null != cursor) {
					mCursors.put(cursorOrder.getUUID(), cursor);
					proxy.replaceCursorWith(cursor);
				}
			}
		}
		return proxy.getCursor();
	}

	private CursorTechHole createNewProxy(CursorOrder cursorOrder) {
		cursorOrder.setUsed();
		UUID uuid = cursorOrder.getUUID();
		CursorProxy proxy = new CursorProxy(uuid);
		SqlCommandSource cmdSrc = cursorOrder.getCmdProvider();
		mEventPropeller.registerNotification(uuid, cmdSrc.getNotificationTags());
		mEventPropeller.subscribeOnUUID(uuid, proxy);
		mProxies.put(uuid, proxy);
		mCommandSources.put(uuid, cmdSrc);
		mCursorLifeController.add(proxy, uuid);
		return proxy;
	}

	private void fireCursorLoader(UUID uuid) {
		if (!mIsEngineFrozen) {
			int loaderId = getNextFreeLoaderId();
			Bundle args = new Bundle();
			args.putSerializable("uuid", uuid);
			mLoaderManager.restartLoader(loaderId, args, mLoaderHelper);
		}
	}

	private void connectCursor(UUID uuid, Cursor newCursor) {
		mCursorLifeController.invalidate();
		Cursor cursor = mCursors.get(uuid);
		if (null != cursor) {
			// close real cursor
			mCursors.remove(uuid);
			if (!cursor.isClosed()) {
				cursor.close();
			}
		}
		CursorTechHole techHole = mProxies.get(uuid);
		if (null != techHole) {
			techHole.replaceCursorWith(newCursor);
			mCursors.put(uuid, newCursor);
			techHole.notifyDataChanged();
		} else {
			mCommandSources.remove(uuid);
			if (null != newCursor) {
				// close real cursor
				if (!newCursor.isClosed()) {
					newCursor.close();
				}
			}
		}
	}

	private void releaseDismissed(UUID uuid) {
		CursorTechHole techHole = mProxies.get(uuid);
		if (null != techHole) {
			// detach notifications
			mEventPropeller.unSubscribeOnUUID(uuid, techHole.getChangeListener());
			// disconnect real cursor
			techHole.replaceCursorWith(new NullCursor());
			mProxies.remove(uuid);
		}
		Cursor cursor = mCursors.get(uuid);
		if (null != cursor) {
			// close real cursor
			mCursors.remove(uuid);
			if (!cursor.isClosed()) {
				cursor.close();
			}
		}
		SqlCommandSource sqlCmdSrc = mCommandSources.get(uuid);
		if (null != sqlCmdSrc) {
			mEventPropeller.unRegisterNotification(uuid, sqlCmdSrc.getNotificationTags());
			mCommandSources.remove(uuid);
		}
	}

	private void initLoaderFromBundle(InternalCursorLoader loader, Bundle args) {
		if (null != loader && null != args) {
			UUID uuid = (UUID) args.getSerializable("uuid");
			SqlCommandSource sqlCmdSrc = mCommandSources.get(uuid);
			loader.init(mProcessor, sqlCmdSrc, uuid);
		}
	}

	private int getNextFreeLoaderId() {
		if (mNextFreeLoaderId == Integer.MAX_VALUE) {
			mNextFreeLoaderId = 1;
		} else {
			mNextFreeLoaderId++;
		}
		return mNextFreeLoaderId;
	}

	private boolean mIsEngineFrozen;
	private int mNextFreeLoaderId = 0;
	private LoaderManagerX mLoaderManager;
	private Context mContext;
	private QProcessor mProcessor;

	private final WeakRefList<DbDisconnectingListener> mDbDisconnectingListeners = new WeakRefList<DbDisconnectingListener>();
	private final LoaderManagerHelper mLoaderHelper = new LoaderManagerHelper();
	private final DbProvider<?> mDbProvider;
	private final QProcessorProvider mQProcessorProvider;
	private final EventPropeller mEventPropeller = new EventPropeller();
	private final WeakCursorProxyContainer mProxies = new WeakCursorProxyContainer();
	// TODO: додати TTL (time to live) до mLockedProxies
	private final ConcurrentHashMap<String, CursorTechHole> mLockedProxies = new ConcurrentHashMap<String, CursorTechHole>();
	private final ConcurrentHashMap<UUID, Cursor> mCursors = new ConcurrentHashMap<UUID, Cursor>();
	private final ConcurrentHashMap<UUID, SqlCommandSource> mCommandSources = new ConcurrentHashMap<UUID, SqlCommandSource>();
	private final LifeCycleController mCursorLifeController = new LifeCycleController(new DismissListener() {

		@Override
		public void onObjectDismissed(UUID[] lostIdsList) {
			DbEngineImpl.this.releaseAllDismissed(lostIdsList);
		}
	});

	private class LoaderManagerHelper implements LoaderManager.LoaderCallbacks<Cursor> {

		@Override
		public Loader<Cursor> onCreateLoader(int id, Bundle args) {
			InternalCursorLoader resultLoader = new InternalCursorLoader(mContext);
			initLoaderFromBundle(resultLoader, args);
			return resultLoader;
		}

		@Override
		public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor newCursor) {
			InternalCursorLoader loader = (InternalCursorLoader) cursorLoader;
			UUID loaderUUID = loader.getUUID();
			if (null != loaderUUID) {
				Cursor cursor;
				if (mIsEngineFrozen) {
					cursor = new NullCursor();
					if (null != newCursor && !newCursor.isClosed()) {
						newCursor.close();
					}
				} else {
					cursor = newCursor;
				}
				connectCursor(loaderUUID, cursor);
			}
			mEventPropeller.notifyChangedUUID(loaderUUID);
		}

		@Override
		public void onLoaderReset(Loader<Cursor> cursorLoader) {
			InternalCursorLoader loader = (InternalCursorLoader) cursorLoader;
			UUID loaderUUID = loader.getUUID();
			if (null != loaderUUID) {
				connectCursor(loaderUUID, new NullCursor());
				mEventPropeller.notifyChangedUUID(loaderUUID);
			}
		}
	}

	private static class InternalCursorLoader extends AsyncCursorLoader {

		public InternalCursorLoader(Context context) {
			super(context);
		}

		public void init(QProcessor processor, SqlCommandSource sqlCmdSrc, UUID uUID) {
			mProcessor = processor;
			mSqlCmdSrc = sqlCmdSrc;
			mUUID = uUID;
		}

		@Override
		public Cursor loadInBackground() {
			Cursor resultCursor = null;
			if (isReadyToBeProcessed()) {
				resultCursor = mProcessor.fetch(mSqlCmdSrc.getSqlCommand());
			}
			return resultCursor;
		}

		public UUID getUUID() {
			return mUUID;
		}

		private boolean isReadyToBeProcessed() {
			return (null != mProcessor) && (null != mSqlCmdSrc) && (null != mUUID);
		}

		private QProcessor mProcessor;
		private SqlCommandSource mSqlCmdSrc;
		private UUID mUUID;
	}

	private static class WeakCursorProxyContainer {

		// TODO: замінити повноцінною WeakRefHashMap з зачисткою дохлятини 
		protected CursorTechHole get(UUID key) {
			synchronized (this) {
				CursorTechHole result = null;
				WeakReference<CursorTechHole> holeRef = mHashMap.get(key);
				if (null != holeRef) {
					result = holeRef.get();
					if (null == result) {
						mHashMap.remove(key);
					}
				}
				return result;
			}
		}

		protected void put(UUID key, CursorTechHole obj) {
			synchronized (this) {
				if (null != obj) {
					mHashMap.put(key, new WeakReference<CursorTechHole>(obj));
				}
			}
		}

		protected void remove(UUID key) {
			synchronized (this) {
				mHashMap.remove(key);
			}
		}

		private final ConcurrentHashMap<UUID, WeakReference<CursorTechHole>> mHashMap = new ConcurrentHashMap<UUID, WeakReference<CursorTechHole>>();
	}

	/**
	 * Notify DbEngine about changes in tables made by content provider
	 */
	@Override
	public void notifyEventPropeller(String[] affectedTablesList) {
		ChangeListener[] listeners;
		synchronized (this) {
			listeners = mEventPropeller.getAffected(affectedTablesList);
		}
		for (ChangeListener listener : listeners) {
			listener.onChanged(null);
		}
	}

	@Override
	public long insert(String table, String nullColumnHack, ContentValues initialValues) {
		if (!mIsEngineFrozen) {
			List<Object> bindArgs = new LinkedList<Object>();
			StringBuilder sql = new StringBuilder(152);
			sql.append("INSERT INTO ");
			sql.append(table);
			StringBuilder values = new StringBuilder(40);
			Set<Map.Entry<String, Object>> entrySet = null;
			if (initialValues != null && initialValues.size() > 0) {
				entrySet = initialValues.valueSet();
				Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
				sql.append('(');
				boolean needSeparator = false;
				while (entriesIter.hasNext()) {
					if (needSeparator) {
						sql.append(", ");
						values.append(", ");
					}
					needSeparator = true;
					Map.Entry<String, Object> entry = entriesIter.next();
					sql.append(entry.getKey());
					values.append('?');
					bindArgs.add(entry.getValue());
				}
				sql.append(')');
			} else {
				sql.append("(" + nullColumnHack + ") ");
				values.append("NULL");
			}
			sql.append(" VALUES(");
			sql.append(values);
			sql.append(");");

			mProcessor.exec(sql.toString(), bindArgs.toArray());
			return 1;
		}
		return 0;
	}

	@Override
	public int delete(String table, String whereClause, String[] whereArgs) {
		if (!mIsEngineFrozen) {
			String sql = "DELETE FROM " + table + (!TextUtils.isEmpty(whereClause) ? " WHERE " + whereClause : "");
			mProcessor.exec(sql, whereArgs);
		}
		return 1;
	}

	@Override
	public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
		if (!mIsEngineFrozen) {
			StringBuilder sql = new StringBuilder(120);
			sql.append("UPDATE ");
			sql.append(table);
			sql.append(" SET ");
			List<Object> bindArgs = new LinkedList<Object>();
			Set<Map.Entry<String, Object>> entrySet = values.valueSet();
			Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
			while (entriesIter.hasNext()) {
				Map.Entry<String, Object> entry = entriesIter.next();
				sql.append(entry.getKey());
				sql.append("=?");
				if (entriesIter.hasNext()) {
					sql.append(", ");
				}
				bindArgs.add(entry.getValue());
			}
			if (!TextUtils.isEmpty(whereClause)) {
				sql.append(" WHERE ");
				sql.append(whereClause);
				if (whereArgs != null) {
					for (String whereArg : whereArgs) {
						bindArgs.add(whereArg);
					}
				}
			}
			mProcessor.exec(sql.toString(), bindArgs.toArray());
			return 1;
		}
		return 0;
	}

	@Override
	public void execBind(String sql, Object[] bindArgs) {
		if (!mIsEngineFrozen) {
			mProcessor.exec(sql, bindArgs);
		}
	}

	@Override
	public void execBind(SqlCommandSource cmdSrc, Object[] bindArgs) {
		if (!mIsEngineFrozen) {
			mProcessor.exec(cmdSrc.getSqlCommand(), bindArgs);
			ChangeListener[] listeners;
			synchronized (this) {
				listeners = mEventPropeller.getAffected(cmdSrc.getNotificationTags());
			}
			for (ChangeListener listener : listeners) {
				listener.onChanged(cmdSrc);
			}
		}
	}

	@Override
	public long replace(String table, String nullColumnHack, ContentValues initialValues) {
		if (!mIsEngineFrozen) {
			List<Object> bindArgs = new LinkedList<Object>();
			StringBuilder sql = new StringBuilder(152);
			sql.append("REPLACE INTO ");
			sql.append(table);
			StringBuilder values = new StringBuilder(40);
			Set<Map.Entry<String, Object>> entrySet = null;
			if (initialValues != null && initialValues.size() > 0) {
				entrySet = initialValues.valueSet();
				Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
				sql.append('(');
				boolean needSeparator = false;
				while (entriesIter.hasNext()) {
					if (needSeparator) {
						sql.append(", ");
						values.append(", ");
					}
					needSeparator = true;
					Map.Entry<String, Object> entry = entriesIter.next();
					sql.append(entry.getKey());
					values.append('?');
					bindArgs.add(entry.getValue());
				}
				sql.append(')');
			} else {
				sql.append("(" + nullColumnHack + ") ");
				values.append("NULL");
			}
			sql.append(" VALUES(");
			sql.append(values);
			sql.append(");");

			mProcessor.exec(sql.toString(), bindArgs.toArray());
			return 1;
		}
		return 0;
	}

	private void ensureNotUsed(CursorOrder cursorOrder) {
		if (cursorOrder.getUsed()) {
			throw new InvalidParameterException("Attempt to request additional cursor for the same cursorOrder.");
		}
	}
}
