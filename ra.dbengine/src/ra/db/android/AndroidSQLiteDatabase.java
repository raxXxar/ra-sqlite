package ra.db.android;

import java.util.Locale;
import ra.dbengine.ISQLDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteTransactionListener;

public class AndroidSQLiteDatabase implements ISQLDatabase {

	public AndroidSQLiteDatabase(SQLiteDatabase db) {
		if (db == null) {
			throw new IllegalArgumentException("null database");
		}
		this.db = db;
	}

	@Override
	public void beginTransaction() {
		db.beginTransaction();
	}

	@Override
	public void endTransaction() {
		db.endTransaction();
	}

	@Override
	public void setTransactionSuccessful() {
		db.setTransactionSuccessful();
	}

	@Override
	public boolean inTransaction() {
		return db.inTransaction();
	}

	@Override
	public void setLocale(String localeStr) {
		//TODO: find locale identified by "localeStr"
		db.setLocale(Locale.getDefault());
	}
	
	@Override
	public void close() {
		db.close();
	}

	@Override
	public int getVersion() {
		return db.getVersion();
	}

	@Override
	public void setVersion(int version) {
		db.setVersion(version);
	}

	@Override
	public Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having,
			String orderBy, String limit) {
		return db.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}

	@Override
	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
		return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
	}

	@Override
	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
		return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}

	@Override
	public Cursor rawQuery(String sql, String[] selectionArgs) {
		return db.rawQuery(sql, selectionArgs);
	}

	@Override
	public long insert(String table, String nullColumnHack, ContentValues values) {
		return db.insert(table, nullColumnHack, values);
	}

	@Override
	public long replace(String table, String nullColumnHack, ContentValues initialValues) {
		return db.replace(table, nullColumnHack, initialValues);
	}

	@Override
	public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
		return db.update(table, values, whereClause, whereArgs);
	}

	@Override
	public int execSQL(String sql) {
		db.execSQL(sql);
		return 0;
	}

	@Override
	public int execSQL(String sql, Object[] bindArgs) {
		db.execSQL(sql, bindArgs);
		return 0;
	}

	@Override
	public boolean isReadOnly() {
		return db.isReadOnly();
	}

	@Override
	public boolean isOpen() {
		return db.isOpen();
	}

	@Override
	public boolean needUpgrade(int newVersion) {
		return db.needUpgrade(newVersion);
	}

	@Override
	public void beginTransactionWithListener(SQLiteTransactionListener listener) {
		db.beginTransactionWithListener(listener);
	}

	@Override
	public long replaceOrThrow(String table, String nullColumnHack, ContentValues initialValues) throws SQLException {
		return db.replaceOrThrow(table, nullColumnHack, initialValues);
	}

	@Override
	public int delete(String table, String whereClause, String[] whereArgs) {
		return db.delete(table, whereClause, whereArgs);
	}

	private final SQLiteDatabase db;
}
