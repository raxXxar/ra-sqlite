#include <stdlib.h>
#include <malloc.h>
#include <jni.h>
#include "ra_extent.h"

/* returns created extent of specified (size), (size) in bytes */
Extent* Extent_Create(jint size) {
	Extent* pExtent = (Extent*) malloc(sizeof(Extent));

	pExtent->freeSpacePtr = pExtent->ptr = calloc(size, sizeof(jint));
	pExtent->size = size;

	return (Extent*) pExtent;
}

/* frees extent (pExtent) */
void Extent_Free(Extent* pExtent) {
	free(pExtent->ptr);
	free(pExtent);
}
#define __getFreeSize(pExt) ((pExt)->ptr - (pExt)->freeSpacePtr + (pExt)->size)

/*
 * returns pointer to allocated (size) bytes from extent (pExtent),
 * null if no (size) free space in extent (pExtent)
 */
void* Extent_CallocMem(Extent* pExtent, jint size) {
	if (__getFreeSize( pExtent ) < size) {
		return NULL;
	} else {
		jint* mem = pExtent->freeSpacePtr;
		pExtent->freeSpacePtr += size;
		return mem;
	}
}

/* returns amount of free space in (pExtent) in bytes */
jint Extent_GetFreeSize(Extent* pExtent) {
	return __getFreeSize( pExtent );
}

/* resets extent, make it ready for reuse */
//void Extent_Reset(Extent* pExtent) {
//	pExtent->freeSpacePtr = pExtent->ptr;
//	memset(pExtent->ptr, 0, pExtent->size);
//}
