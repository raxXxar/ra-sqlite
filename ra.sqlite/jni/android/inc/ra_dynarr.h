/*
 * ra_dynarr.h
 *
 *      Author: =ra=
 */

#ifndef RA_DYNARR_H_
#define RA_DYNARR_H_

#include <jni.h>

typedef void* Item;
typedef struct Array Array;
typedef void (*FnPDestructor)(Item);
typedef void (*FnPTransformer)(Item);
typedef jint (*FnPEquivalence)(Item, Item);

Array* Array_Create(jint initialCapacity,
                    jint growthSize,
                    FnPEquivalence equivalenceFunction,
                    FnPDestructor destructorFunction);

void Array_Free(Array* pArray);

void Array_ForEach(Array* pArray, FnPTransformer transformFunction);

jint Array_Exists(Array* pArray, Item item);

jint Array_Insert(Array* pArray, Item item);

jint Array_RemoveEntry(Array* pArray, Item item, jint bDestroy);

jint intEquals(Item a, Item b);

#endif /* RA_DYNARR_H_ */
