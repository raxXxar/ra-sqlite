/*
 * ra_pool.c
 *
 *      Author: =ra=
 */

#include <malloc.h>
#include <jni.h>
#include "ra_sqlite.h"
#include "ra_exception.h"
#include "ra_hnd_map.h"
#include "ra_sqlite_SQLiteCursor_.h"
#include "ra_sqlite_SQLiteDatabase_.h"

static HndMap* g_pool; // no extern access

#define POOL_INITIAL_SIZE (2 << 5)
#define POOL_GROW_SIZE (2 << 4)

/**
 * initialize pool, should be used only in JNI_OnLoad() once time
 */

void initPool() {
	g_pool = HndMap_Create(POOL_INITIAL_SIZE, POOL_GROW_SIZE);
}

/**
 * free pool resources, redundant, but for a purity ...
 */
void destroyPool() {
	g_pool = HndMap_Free(g_pool);
}

/**
 * register ptr in a global pool
 * return ptr handle in the pool;
 */
jint registerInPool(void* ptr) {
	return (jint) HndMap_Insert(g_pool, ptr);
}

void unregisterInPool(jint handle) {
	HndMap_removePtr(g_pool, (jint) handle);
}

void* getInPool(jint handle) {
	return HndMap_getPtr(g_pool, handle);
}

/**
 * return  pointer associated with requested dbHandle, or throw NoSuchDatabaseException
 */
Dbh* getDbhPtrOrThrow(JNIEnv* jniEnv, jint dbHandle) {

	Dbh* pDb = HndMap_getPtr(g_pool, (jint) dbHandle);
	if (NULL == pDb) {
		THROW_NO_DB_FORMAT(jniEnv,
		                   "Database with handle[%d] is not registered", dbHandle);
	}
	return pDb;
}

/**
 * return pointer associated with requested cursorHandle, or throw NoSuchCursorException
 */
Cursor* getCursorPtrOrThrow(JNIEnv* jniEnv, jint cursorHandle) {
	Cursor* pCursor = HndMap_getPtr(g_pool, (jint) cursorHandle);
	if (NULL == pCursor) {
		THROW_NO_CURSOR_FORMAT(jniEnv,
		                       "Cursor with handle[%d] is not registered", cursorHandle);
	}
	return pCursor;
}
