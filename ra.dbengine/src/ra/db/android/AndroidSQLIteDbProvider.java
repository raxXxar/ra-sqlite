package ra.db.android;

import ra.dbengine.ISQLDatabase;
import ra.dbengine.ISQLDbProvider;
import android.database.sqlite.SQLiteDatabase;

/**
 *  @author =ra=
 */

public class AndroidSQLIteDbProvider extends ISQLDbProvider {

	@Override
	protected ISQLDatabase open(String databasePath) {
		boolean success = false;
		SQLiteDatabase db = null;
		ISQLDatabase dbRA = null;
		try {
			db = config.getContext().openOrCreateDatabase(databasePath, 0, null);
			dbRA = new AndroidSQLiteDatabase(db);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		} finally {
			if (!success) {
				if (null != dbRA) {
					dbRA.close();
					dbRA = null;
				}
				if (db != null && db.isOpen()) {
					db.close();
				}
				db = null;
			}
		}
		return dbRA;
	}

}
