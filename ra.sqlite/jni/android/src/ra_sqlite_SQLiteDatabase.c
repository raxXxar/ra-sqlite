#include <jni.h>
#include <malloc.h>
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_dbut.h"
#include "ra_exception.h"
#include "ra_sqlite_SQLiteDatabase.h"
#include "ra_sqlite_SQLiteDatabase_.h"
#include "ra_sqlite_SQLiteCursor_.h"
#include "ra_collator.h"
#include "ra_pool.h"

static Dbh* __Dbh_Create(sqlite3* pDb);

/* destroy cursor, finalise statement */
static void __cursorsArrayEntryDestructor(Item entry);

/* finalise statement */
static void __statementsArrayEntryDestructor(Item entry);

static void __Dbh_TransAction(JNIEnv* jEnv, Dbh* pDbh, const char* action);

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbOpen
 * Signature: (Ljava/lang/String;I)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteDatabase_dbOpen(JNIEnv* jEnv,
                                                             jobject jSQLiteDatabase,
                                                             jstring fileName,
                                                             jint dbOpenFlags) {
	char const *fileNameStr = (*jEnv)->GetStringUTFChars(jEnv, fileName, 0);
	sqlite3* pDB = NULL;
	int err = sqlite3_open_v2(fileNameStr, &pDB, dbOpenFlags, 0);
	THROW_SQL3(jEnv, err, pDB);
	err = sqlite3_busy_timeout(pDB, 1000 /* ms */);
	THROW_SQL3(jEnv, err, pDB);
	if (NULL != fileNameStr) {
		(*jEnv)->ReleaseStringUTFChars(jEnv, fileName, fileNameStr);
	}
	Dbh* pDbh = __Dbh_Create(pDB);
	return registerInPool(pDbh);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbClose
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_dbClose(JNIEnv* jEnv, jobject jSQLiteDatabase, jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__LOCK(pDbh);
	sqlite3* pDatabase = pDbh->pSqliteDb;
	Array_Free(pDbh->pCursors);
	Array_Free(pDbh->pStatements);
	int err = sqlite3_close(pDatabase);
	if (NULL != pDbh->pDefCollator) {
		ucol_close(pDbh->pDefCollator);
		pDbh->pDefCollator = NULL;
	}
	unregisterInPool(dbHandle);
	__UNLOCK(pDbh);
	free(pDbh);
	THROW_SQL3(jEnv, err, pDatabase);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbStartTransaction
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_dbStartTransaction(JNIEnv* jEnv,
                                                                         jobject jSQLiteDatabase,
                                                                         jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__Dbh_TransAction(jEnv, pDbh, "BEGIN EXCLUSIVE");
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbCommitTransaction
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_dbCommitTransaction(JNIEnv* jEnv,
                                                                          jobject jSQLiteDatabase,
                                                                          jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__Dbh_TransAction(jEnv, pDbh, "COMMIT");
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbRollbackTransaction
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_dbRollbackTransaction(JNIEnv* jEnv,
                                                                            jobject jSQLiteDatabase,
                                                                            jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__Dbh_TransAction(jEnv, pDbh, "ROLLBACK");
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    lock
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_lock(JNIEnv* jEnv, jobject jSQLiteDatabase, jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__LOCK( pDbh);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    unlock
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_unlock(JNIEnv* jEnv, jobject jSQLiteDatabase, jint dbHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	__UNLOCK( pDbh);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindByteArray
 * Signature: (II[B)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindByteArray(JNIEnv* jEnv,
                                                                    jobject jSQLiteDatabase,
                                                                    jint statementHandle,
                                                                    jint index,
                                                                    jbyteArray valueByteArray) {
	statementBindByteArray(jEnv, (sqlite3_stmt*) statementHandle, index, valueByteArray);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindByteBuffer
 * Signature: (IILjava/nio/ByteBuffer;)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindByteBuffer(JNIEnv* jEnv,
                                                                     jobject jSQLiteDatabase,
                                                                     jint statementHandle,
                                                                     jint index,
                                                                     jobject valueByteBuffer) {
	statementBindByteBuffer(jEnv, (sqlite3_stmt*) statementHandle, index, valueByteBuffer);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindString
 * Signature: (IILjava/lang/String;)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindString(JNIEnv* jEnv,
                                                                 jobject jSQLiteDatabase,
                                                                 jint statementHandle,
                                                                 jint index,
                                                                 jstring valueString) {
	statementBindString(jEnv, (sqlite3_stmt*) statementHandle, index, valueString);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindLong
 * Signature: (IIJ)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindLong(JNIEnv* jEnv,
                                                               jobject jSQLiteDatabase,
                                                               jint statementHandle,
                                                               jint index,
                                                               jlong valueLong) {
	statementBindLong(jEnv, (sqlite3_stmt*) statementHandle, index, valueLong);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindDouble
 * Signature: (IID)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindDouble(JNIEnv* jEnv,
                                                                 jobject jSQLiteDatabase,
                                                                 jint statementHandle,
                                                                 jint index,
                                                                 jdouble valueDouble) {
	statementBindDouble(jEnv, (sqlite3_stmt*) statementHandle, index, valueDouble);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    bindNull
 * Signature: (II)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_bindNull(JNIEnv* jEnv,
                                                               jobject jSQLiteDatabase,
                                                               jint statementHandle,
                                                               jint index) {
	statementBindNull(jEnv, (sqlite3_stmt*) statementHandle, index);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    getColumnNames
 * Signature: (I)[Ljava/lang/String;
 */JNIEXPORT jobjectArray JNICALL Java_ra_sqlite_SQLiteDatabase_getColumnNames(JNIEnv* jEnv,
                                                                             jobject jSQLiteDatabase,
                                                                             jint statementHandle) {
	return getStatementColumnNames(jEnv, (sqlite3_stmt*) statementHandle);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    reset
 * Signature: (I)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_reset(JNIEnv* jEnv, jobject jSQLiteDatabase, jint statementHandle) {
	resetStatement(jEnv, (sqlite3_stmt*) statementHandle);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    prepare
 * Signature: (ILjava/lang/String;)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteDatabase_prepare(JNIEnv* jEnv,
                                                              jobject jSQLiteDatabase,
                                                              jint dbHandle,
                                                              jstring strSql) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	return prepareStatement(jEnv, pDbh, strSql);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    finalize
 * Signature: (II)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_finalize(JNIEnv* jEnv,
                                                              jobject jSQLiteDatabase,
                                                              jint dbHandle,
                                                              jint statementHandle) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	finalizeStatement(pDbh, (sqlite3_stmt*) statementHandle);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    getStatementArgsCount
 * Signature: (I)I
 */

JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteDatabase_getStatementArgsCount(JNIEnv* jEnv,
                                                                           jobject jSQLiteDatabase,
                                                                           jint statementHandle) {
	return getStatementArgsCount((sqlite3_stmt*) statementHandle);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    exec
 * Signature: (I)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteDatabase_exec(JNIEnv* jEnv, jobject jSQLiteDatabase, jint statementHandle) {
	return execStatement(jEnv, (sqlite3_stmt*) statementHandle);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    getInt64Value
 * Signature: (I)J
 */JNIEXPORT jlong JNICALL Java_ra_sqlite_SQLiteDatabase_getInt64Value(JNIEnv* jEnv,
                                                                     jobject jSQLiteDatabase,
                                                                     jint statementHandle) {
	return columnInt64Value((sqlite3_stmt*) statementHandle, 0);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    getStringValue
 * Signature: (I)Ljava/lang/String;
 */JNIEXPORT jstring JNICALL Java_ra_sqlite_SQLiteDatabase_getStringValue(JNIEnv* jEnv,
                                                                        jobject jSQLiteDatabase,
                                                                        jint statementHandle) {
	return columnStringValue(jEnv, (sqlite3_stmt*) statementHandle, 0);
}

/*
 * Class:     ra_sqlite_SQLiteDatabase
 * Method:    dbSetLocale
 * Signature: (ILjava/lang/String;)V
 */JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteDatabase_dbSetLocale(JNIEnv* jEnv,
                                                                  jobject jSQLiteDatabase,
                                                                  jint dbHandle,
                                                                  jstring locale) {
	Dbh* pDbh = getDbhPtrOrThrow(jEnv, dbHandle);
	char const* localeStr = (*jEnv)->GetStringUTFChars(jEnv, locale, 0);
	setDefaultCollation(jEnv, pDbh, localeStr);
	if (localeStr != 0) {
		(*jEnv)->ReleaseStringUTFChars(jEnv, locale, localeStr);
	}
}

/*
 * release DbHelper resources (not DbHelper*),
 * except pointer to sqlite3 database
 */
static Dbh* __Dbh_Create(sqlite3* pDB) {
	Dbh* pDbh = (Dbh*) malloc(sizeof(Dbh));
	pDbh->g_poolId = 0;
	pDbh->pDefCollator = NULL;
	pDbh->pSqliteDb = pDB;

	pDbh->pCursors = Array_Create(CURSORS_INITIAL_CAPACITY,
	                              CURSORS_GROW_SIZE,
	                              &intEquals,
	                              &__cursorsArrayEntryDestructor);

	pDbh->pStatements = Array_Create(CURSORS_INITIAL_CAPACITY,
	                                 CURSORS_GROW_SIZE,
	                                 &intEquals,
	                                 &__statementsArrayEntryDestructor);
	__INIT(pDbh);
	return pDbh;
}

static void __cursorsArrayEntryDestructor(Item entry) {
	Cursor* pCursor = (Cursor*) entry;
	Array_RemoveEntry(pCursor->pDb->pStatements, pCursor->pStatement, JNI_TRUE);
	Cursor_Free(pCursor);
}

static void __statementsArrayEntryDestructor(Item entry) {
	sqlite3_finalize((sqlite3_stmt*) entry);
}

static void __resetCursor(Item pCursor) {
	Cursor_Reset((Cursor*) pCursor);
}

static void __Dbh_TransAction(JNIEnv* jEnv, Dbh* pDbh, const char* action) {
	Array_ForEach(pDbh->pCursors, &__resetCursor);
	sqlite3_stmt* stmtHandle = NULL;
	jint errcode = sqlite3_prepare_v2(pDbh->pSqliteDb, action, -1, &stmtHandle, 0);
	THROW_SQL3(jEnv, errcode, pDbh->pSqliteDb);
	errcode = sqlite3_step(stmtHandle);
	sqlite3_finalize(stmtHandle);
	if ((errcode != SQLITE_ROW) && (errcode != SQLITE_DONE)) {
		throw_sqlite3_exception(jEnv, pDbh->pSqliteDb, errcode);
	}
}

