package ra.sqlite;

public class CantleCursorColumn {

	public CantleCursorColumn(CantleCursorInfo cursorInfo, int realColumnIndex) {
		this.cursorInfo = cursorInfo;
		this.realColumnIndex = realColumnIndex;
	}

	public CantleCursorInfo getCantleCursorInfo() {
		return cursorInfo;
	}

	public int getGlobalColumnIndex() {
		return cursorInfo.getStartGlobalColumnIndex() + realColumnIndex;
	}

	public String getName() {
		String name = cursorInfo.getCursor().getColumnName(realColumnIndex);
		return (null == name) ? null : name.toLowerCase();
	}

	public String getNameWithAlias() {
		return cursorInfo.getAlias() + "." + getName();
	}

	public String getNameWithGivenAlias(String alias) {
		return (null == alias) ? null : alias.toLowerCase() + "." + getName();
	}

	private final CantleCursorInfo cursorInfo;
	private final int realColumnIndex;
}
