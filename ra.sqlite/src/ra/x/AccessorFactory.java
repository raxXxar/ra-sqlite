package ra.x;

import ra.sqlite.NativeCursor;
import android.database.Cursor;

public class AccessorFactory {

	public static IndexAccessor createAccessor(Cursor cursor) {
		return createAccessor(cursor, null);
	}

	public static IndexAccessor createAccessor(Cursor cursor, String indexColumn) {
		IndexAccessor cursorAccessor = null;
		if (null == cursor) {
			throw new IllegalArgumentException("Cursor is null");
		}
		if (null == indexColumn) {
			indexColumn = IndexAccessor.DEF_FAST_ACCESS_COLUMN_NAME;
		}
		if (-1 != cursor.getColumnIndex(indexColumn)) {

			if (cursor instanceof NativeCursor) {
				cursorAccessor = new IndexAccessorRa(cursor, indexColumn);
			} else {
				cursorAccessor = new IndexAccessorCommon(cursor, indexColumn);
			}
		}
		return cursorAccessor;
	}
}
