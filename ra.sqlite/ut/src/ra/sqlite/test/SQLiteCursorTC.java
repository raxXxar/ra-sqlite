package ra.sqlite.test;

import java.nio.ByteBuffer;
import ra.sqlite.SQLiteDatabase;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.os.Environment;
import android.test.AndroidTestCase;
import android.util.Log;

public class SQLiteCursorTC extends AndroidTestCase {

	protected void setUp() throws Exception {
		super.setUp();
		db = SQLiteDatabase.openDatabase(DB_PATH_NAME);
		assertTrue(null != db);
	}

	protected void tearDown() throws Exception {
		db.close();
		db = null;
		getContext().deleteDatabase(DB_PATH_NAME);
		super.tearDown();
	}

	public void testSQLiteCursor() {
		long rowCount = TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertNotNull(cursor);
		assertEquals(rowCount, cursor.getCount());
		cursor.close();
	}

	public void testGetCount() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertNotNull(cursor);
		assertEquals(rowCount, cursor.getCount());
		cursor.close();
	}

	public void testGetPosition() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertNotNull(cursor);
		int cursorRowCount = cursor.getCount();
		assertEquals(rowCount, cursorRowCount);
		int cursorPosition = cursor.getPosition();
		assertEquals(-1, cursorPosition); // not in row
		int newPosition = (int) (rowCount / 2);
		cursor.moveToPosition(newPosition);
		cursorPosition = cursor.getPosition();
		assertEquals(newPosition, cursorPosition);
		cursor.close();
	}

	public void testMove() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		int offset = (int) (rowCount / 2);
		cursor.moveToFirst();
		int currentPosition = cursor.getPosition();
		assertEquals(0, currentPosition);
		cursor.move(offset);
		currentPosition = cursor.getPosition();
		assertEquals(offset, currentPosition);
		cursor.close();
	}

	public void testMoveToPosition() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		int newPosition = (int) (rowCount / 2);
		cursor.moveToPosition(newPosition);
		int currentPosition = cursor.getPosition();
		assertEquals(newPosition, currentPosition);
		cursor.close();
	}

	public void testMoveToFirst() {
		TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		boolean isFirst = cursor.isBeforeFirst();
		assertEquals(true, isFirst);
		cursor.moveToFirst();
		int currentPosition = cursor.getPosition();
		assertEquals(0, currentPosition);
		cursor.close();
	}

	public void testMoveToLast() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		cursor.moveToLast();
		int currentPosition = cursor.getPosition();
		assertEquals(rowCount - 1, currentPosition);
		cursor.close();
	}

	public void testMoveToNext() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		int count = 0;
		while (cursor.moveToNext()) {
			++count;
		}
		assertEquals(rowCount, count);
		cursor.close();
	}

	public void testMoveToPrevious() {
		int rowCount = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		int count = 0;
		cursor.moveToLast();
		cursor.moveToNext();
		assertEquals(true, cursor.isAfterLast());
		while (cursor.moveToPrevious()) {
			++count;
		}
		assertEquals(rowCount, count);
		cursor.close();
	}

	public void testIsFirst() {
		TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertEquals(true, cursor.isBeforeFirst());
		assertEquals(false, cursor.isFirst());
		cursor.moveToFirst();
		assertEquals(true, cursor.isFirst());
		assertEquals(0, cursor.getPosition());
		cursor.close();
	}

	public void testIsLast() {
		long rowCount = TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertEquals(false, cursor.isLast());
		cursor.moveToLast();
		assertEquals(true, cursor.isLast());
		assertEquals(rowCount - 1, cursor.getPosition());
		cursor.close();
	}

	public void testIsBeforeFirst() {
		TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertEquals(true, cursor.isBeforeFirst());
		cursor.moveToFirst();
		assertEquals(false, cursor.isBeforeFirst());
		cursor.moveToPrevious();
		assertEquals(true, cursor.isBeforeFirst());
		assertEquals(-1, cursor.getPosition());
		cursor.close();
	}

	public void testIsAfterLast() {
		long rowCount = TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT * FROM test");
		assertEquals(false, cursor.isAfterLast());
		cursor.moveToLast();
		assertEquals(false, cursor.isAfterLast());
		cursor.moveToNext();
		assertEquals(true, cursor.isAfterLast());
		assertEquals(rowCount, cursor.getPosition());
		cursor.close();
	}

	public void testGetColumnIndex() {
		TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertEquals(0, cursor.getColumnIndex("i"));
		assertEquals(1, cursor.getColumnIndex("F"));
		assertEquals(2, cursor.getColumnIndex("t"));
		assertEquals(3, cursor.getColumnIndex("B"));
		assertEquals(-1, cursor.getColumnIndex("FakeField"));
		cursor.close();
	}

	public void testGetColumnIndexOrThrow() {
		TestHelper.setupTable(db, 10);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertEquals(0, cursor.getColumnIndex("i"));
		assertEquals(1, cursor.getColumnIndex("F"));
		assertEquals(2, cursor.getColumnIndex("t"));
		assertEquals(3, cursor.getColumnIndex("B"));
		boolean wasException = false;
		try {
			cursor.getColumnIndexOrThrow("FakeField");
		} catch (Exception e) {
			assertEquals(true, e instanceof IllegalArgumentException);
			wasException = true;
		}
		assertEquals(true, wasException);
		cursor.close();
	}

	public void testGetColumnName() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertEquals("I", cursor.getColumnName(0));
		assertEquals("F", cursor.getColumnName(1));
		assertEquals("T", cursor.getColumnName(2));
		assertEquals("B", cursor.getColumnName(3));
		assertNull(cursor.getColumnName(-1));
		assertNull(cursor.getColumnName(20));
		cursor.close();
	}

	public void testGetColumnNames() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		String[] columnNames = cursor.getColumnNames();
		assertNotNull(columnNames);
		assertEquals(4, columnNames.length);
		cursor.close();
	}

	public void testGetColumnCount() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		int colCount = cursor.getColumnCount();
		assertEquals(4, colCount);
		cursor.close();
	}

	public void testGetBlob() {
		TestHelper.setupTable(db, 0);
		String vStr = new String("TEXT BLOB TEST");
		ByteBuffer vBuf = TestHelper.str2buf(vStr);
		Integer vInteger = Integer.valueOf(10);
		Double vDouble = Double.valueOf(.34);
		db.execSQL("INSERT INTO test(t,b,i,f) VALUES (?,?,?,?)", new Object[] { vStr, vBuf, vInteger, vDouble });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		byte[] buf = null;
		String nStr = null;
		boolean failed;
		// blob field
		buf = cursor.getBlob(0);
		assertNotNull(buf);
		nStr = TestHelper.buf2str(ByteBuffer.wrap(buf));
		assertEquals(vStr, nStr);
		// text field
		buf = cursor.getBlob(1);
		assertNotNull(buf);
		nStr = TestHelper.buf2str(ByteBuffer.wrap(buf));
		assertEquals(vStr, nStr);
		// float field
		try {
			buf = cursor.getBlob(2);
			failed = false;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		try {
			buf = cursor.getBlob(3);
			failed = false;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// null field
		assertNull(cursor.getBlob(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetString() {
		TestHelper.setupTable(db, 0);
		String vStr = new String("TEXT String TEST");
		ByteBuffer vBuf = TestHelper.str2buf(vStr);
		Integer vInteger = Integer.valueOf(10);
		Double vDouble = Double.valueOf(.34);
		db.execSQL("INSERT INTO test(t,b,i,f) VALUES (?,?,?,?)", new Object[] { vStr, vBuf, vInteger, vDouble });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		String nStr = null;
		// text field
		nStr = cursor.getString(0);
		assertNotNull(nStr);
		assertEquals(vStr, nStr);
		// blob field
		boolean failed = false;
		try {
			nStr = cursor.getString(1);
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		Integer nInteger = Integer.valueOf(cursor.getString(2));
		assertNotNull(nInteger);
		assertEquals(0, vInteger.compareTo(nInteger.intValue()));
		// float field
		Double nDouble = Double.valueOf(cursor.getString(3));
		assertNotNull(nDouble);
		assertEquals(0, vDouble.compareTo(nDouble.doubleValue()));
		// null field
		assertNull(cursor.getString(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testCopyStringToBuffer() {
		String str = "Test";
		Cursor cursor = db.rawQuery("SELECT '" + str + "'");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		android.database.CharArrayBuffer buffer = new CharArrayBuffer(0);
		cursor.copyStringToBuffer(0, buffer);
		String resStr = new String(buffer.data);
		assertEquals(0, resStr.compareTo(str));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetShort() {
		TestHelper.setupTable(db, 0);
		Short vShort = Short.valueOf((short) 10);
		String vStr = String.valueOf(vShort);
		ByteBuffer vBuf = TestHelper.str2buf("XXX");
		Double vDouble = Double.valueOf(vShort);
		assertNotNull(vStr);
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES (?,?,?,?)", new Object[] { vStr, vShort, vDouble, vBuf });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		Short nShort = null;
		boolean failed;
		// text field
		nShort = cursor.getShort(0);
		assertNotNull(nShort);
		assertEquals(0, vShort.compareTo(nShort));
		// blob field
		try {
			nShort = cursor.getShort(1);
			failed = cursor.getShort(1) == 0;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		nShort = cursor.getShort(2);
		assertNotNull(nShort);
		assertEquals(0, vShort.compareTo(nShort));
		// float field
		nShort = cursor.getShort(3);
		assertNotNull(nShort);
		assertEquals(0, vShort.compareTo(nShort));
		// null field
		assertEquals(0, cursor.getShort(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetInt() {
		TestHelper.setupTable(db, 0);
		Integer vInt = Integer.valueOf(10);
		String vStr = String.valueOf(vInt);
		ByteBuffer vBuf = TestHelper.str2buf("XXX");
		Double vDouble = Double.valueOf(vInt);
		assertNotNull(vStr);
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES (?,?,?,?)", new Object[] { vStr, vInt, vDouble, vBuf });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		Integer nInt = null;
		boolean failed;
		// text field
		nInt = cursor.getInt(0);
		assertNotNull(nInt);
		assertEquals(0, vInt.compareTo(nInt));
		// blob field
		try {
			nInt = cursor.getInt(1);
			failed = cursor.getInt(1) == 0;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		nInt = cursor.getInt(2);
		assertNotNull(nInt);
		assertEquals(0, vInt.compareTo(nInt));
		// float field
		nInt = cursor.getInt(3);
		assertNotNull(nInt);
		assertEquals(0, vInt.compareTo(nInt));
		// null field
		assertEquals(0, cursor.getInt(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetLong() {
		TestHelper.setupTable(db, 0);
		Long vLong = Long.valueOf(10);
		String vStr = String.valueOf(vLong);
		ByteBuffer vBuf = TestHelper.str2buf("XXX");
		Double vDouble = Double.valueOf(vLong);
		assertNotNull(vStr);
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES (?,?,?,?)", new Object[] { vStr, vLong, vDouble, vBuf });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		Long nLong = null;
		boolean failed;
		// text field
		nLong = cursor.getLong(0);
		assertNotNull(nLong);
		assertEquals(0, vLong.compareTo(nLong));
		// blob field
		try {
			nLong = cursor.getLong(1);
			failed = cursor.getLong(1) == 0L;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		nLong = cursor.getLong(2);
		assertNotNull(nLong);
		assertEquals(0, vLong.compareTo(nLong));
		// float field
		nLong = cursor.getLong(3);
		assertNotNull(nLong);
		assertEquals(0, vLong.compareTo(nLong));
		// null field
		assertEquals(0, cursor.getLong(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetFloat() {
		TestHelper.setupTable(db, 0);
		Integer vInteger = Integer.valueOf(10);
		Float vFloat = Float.valueOf((float) (vInteger + TestHelper.DBL_FRACTION));
		String vStr = String.valueOf(vFloat);
		ByteBuffer vBuf = TestHelper.str2buf("XXX");
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES (?,?,?,?)", new Object[] { vStr, vInteger, vFloat, vBuf });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		Float nFloat = null;
		boolean failed;
		// text field
		nFloat = cursor.getFloat(0);
		assertNotNull(nFloat);
		assertEquals(0, vFloat.compareTo(nFloat));
		// blob field
		try {
			nFloat = cursor.getFloat(1);
			failed = cursor.getFloat(1) == .0;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		nFloat = cursor.getFloat(2);
		assertNotNull(nFloat);
		assertEquals(vInteger.floatValue(), nFloat.floatValue());
		// float field
		nFloat = cursor.getFloat(3);
		assertNotNull(nFloat);
		assertEquals(0, vFloat.compareTo(nFloat));
		// null field
		nFloat = cursor.getFloat(4);
		assertEquals((float) .0, nFloat.floatValue());
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testGetDouble() {
		TestHelper.setupTable(db, 0);
		Integer vInteger = Integer.valueOf(10);
		Double vDouble = Double.valueOf(vInteger + TestHelper.DBL_FRACTION);
		String vStr = String.valueOf(vDouble);
		ByteBuffer vBuf = TestHelper.str2buf("XXX");
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES (?,?,?,?)", new Object[] { vStr, vInteger, vDouble, vBuf });
		Cursor cursor = db.rawQuery("SELECT t,b,i,f, null as x FROM test LIMIT 1");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst());
		Double nDouble = null;
		boolean failed;
		// text field
		nDouble = cursor.getDouble(0);
		assertNotNull(nDouble);
		assertEquals(0, vDouble.compareTo(nDouble));
		// blob field
		try {
			nDouble = cursor.getDouble(1);
			failed = cursor.getDouble(1) == .0;
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		// integer field
		nDouble = cursor.getDouble(2);
		assertNotNull(nDouble);
		assertEquals(vInteger.doubleValue(), nDouble.doubleValue());
		// float field
		nDouble = cursor.getDouble(3);
		assertNotNull(nDouble);
		assertEquals(0, vDouble.compareTo(nDouble));
		// null field
		nDouble = cursor.getDouble(4);
		assertEquals(.0, nDouble.doubleValue());
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testIsNull() {
		TestHelper.setupTable(db, 0);
		db.execSQL("INSERT INTO test(t,i,f,b) VALUES ('1',1,1.1,'XXX')");
		Cursor cursor = db.rawQuery("SELECT 0 as srt, t,b,i,f FROM test UNION ALL SELECT 1,null,null,null,null ORDER BY 1 ASC");
		assertNotNull(cursor);
		assertEquals(true, cursor.moveToFirst()); // first row Not NULL
		assertEquals(false, cursor.isNull(1));
		assertEquals(false, cursor.isNull(2));
		assertEquals(false, cursor.isNull(3));
		assertEquals(false, cursor.isNull(4));
		assertEquals(true, cursor.moveToNext()); // second row NULL
		assertEquals(true, cursor.isNull(1));
		assertEquals(true, cursor.isNull(2));
		assertEquals(true, cursor.isNull(3));
		assertEquals(true, cursor.isNull(4));
		// done
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	@SuppressWarnings("deprecation")
	public void testDeactivate() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertNotNull(cursor);
		assertEquals(false, cursor.isClosed());
		boolean isOk = false;
		try {
			cursor.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		cursor.deactivate();
		boolean failed = false;
		try {
			cursor.getCount();
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		cursor.requery();
		isOk = false;
		try {
			cursor.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	@SuppressWarnings("deprecation")
	public void testRequery() {
		int count0 = (int) TestHelper.setupTable(db, 11);
		int count1 = -1;
		int count2 = -1;
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertNotNull(cursor);
		assertEquals(false, cursor.isClosed());
		boolean isOk = false;
		try {
			count1 = cursor.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		cursor.deactivate();
		boolean failed = false;
		try {
			cursor.getCount();
		} catch (Exception e) {
			failed = true;
		}
		assertEquals(true, failed);
		cursor.requery();
		isOk = false;
		try {
			count2 = cursor.getCount();
			isOk = true;
		} catch (Exception e) {
		}
		assertEquals(true, isOk);
		assertEquals(count0, count1);
		assertEquals(count0, count2);
		assertEquals(count1, count2);
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testClose() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertNotNull(cursor);
		assertEquals(false, cursor.isClosed());
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testIsClosed() {
		TestHelper.setupTable(db, 1);
		Cursor cursor = db.rawQuery("SELECT i,f,t,b FROM test");
		assertNotNull(cursor);
		assertEquals(false, cursor.isClosed());
		cursor.close();
		assertEquals(true, cursor.isClosed());
	}

	public void testUkrainianSortOrder() {
		db.setLocale("uk_UA");
		Cursor xCursor = db.rawQuery("pragma collation_list");
		while (xCursor.moveToNext()) {
			Log.e("xxx", xCursor.getString(0) + " " + xCursor.getString(1));
		}
		xCursor.close();
		TestHelper.setupTable2(db, 20);
		String queryInt = "SELECT * FROM testInt ORDER BY 1 desc";
		Cursor intCursor = db.rawQuery(queryInt);
		while (intCursor.moveToNext()) {
			Log.e("INT ", intCursor.getString(0) + " : " + intCursor.getString(1));
		}
		intCursor.close();

		String query = "SELECT * FROM (" + "SELECT 'Й' UNION ALL " + "SELECT 'й' UNION ALL " + "SELECT 'Ц' UNION ALL " + "SELECT 'ц' UNION ALL "
				+ "SELECT 'У' UNION ALL " + "SELECT 'у' UNION ALL " + "SELECT 'К' UNION ALL " + "SELECT 'к' UNION ALL " + "SELECT 'Е' UNION ALL "
				+ "SELECT 'е' UNION ALL " + "SELECT 'Н' UNION ALL " + "SELECT 'н' UNION ALL " + "SELECT 'Г' UNION ALL " + "SELECT 'г' UNION ALL "
				+ "SELECT 'Ш' UNION ALL " + "SELECT 'ш' UNION ALL " + "SELECT 'Щ' UNION ALL " + "SELECT 'щ' UNION ALL " + "SELECT 'З' UNION ALL "
				+ "SELECT 'з' UNION ALL " + "SELECT 'Х' UNION ALL " + "SELECT 'х' UNION ALL " + "SELECT 'Ї' UNION ALL " + "SELECT 'ї' UNION ALL "
				+ "SELECT 'Ф' UNION ALL " + "SELECT 'ф' UNION ALL " + "SELECT 'І' UNION ALL " + "SELECT 'і' UNION ALL " + "SELECT 'В' UNION ALL "
				+ "SELECT 'в' UNION ALL " + "SELECT 'А' UNION ALL " + "SELECT 'а' UNION ALL " + "SELECT 'П' UNION ALL " + "SELECT 'п' UNION ALL "
				+ "SELECT 'Р' UNION ALL " + "SELECT 'р' UNION ALL " + "SELECT 'О' UNION ALL " + "SELECT 'о' UNION ALL " + "SELECT 'Л' UNION ALL "
				+ "SELECT 'л' UNION ALL " + "SELECT 'Д' UNION ALL " + "SELECT 'д' UNION ALL " + "SELECT 'Ж' UNION ALL " + "SELECT 'ж' UNION ALL "
				+ "SELECT 'Є' UNION ALL " + "SELECT 'є' UNION ALL " + "SELECT 'Я' UNION ALL " + "SELECT 'я' UNION ALL " + "SELECT 'Ч' UNION ALL "
				+ "SELECT 'ч' UNION ALL " + "SELECT 'С' UNION ALL " + "SELECT 'с' UNION ALL " + "SELECT 'М' UNION ALL " + "SELECT 'м' UNION ALL "
				+ "SELECT 'И' UNION ALL " + "SELECT 'и' UNION ALL " + "SELECT 'Т' UNION ALL " + "SELECT 'т' UNION ALL " + "SELECT 'Ь' UNION ALL "
				+ "SELECT 'ь' UNION ALL " + "SELECT 'Б' UNION ALL " + "SELECT 'б' UNION ALL " + "SELECT 'Ю' UNION ALL " + "SELECT 'ю' UNION ALL "
				+ "SELECT 'Ґ' UNION ALL " + "SELECT 'ґ' UNION ALL " + "SELECT 'A' UNION ALL " + "SELECT 'a' UNION ALL " + "SELECT 'B' UNION ALL "
				+ "SELECT 'b' " + ") ORDER BY 1  COLLATE LOCALIZED ASC";
		Cursor cursor = db.rawQuery(query);
		while (cursor.moveToNext()) {
			// Log.e("TEST", cursor.getString(0));
		}
		cursor.close();
	}

	private SQLiteDatabase db = null;
	private static final String DB_NAME = "test_fake.db";
	private static final String DB_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private static final String DB_PATH_NAME = DB_PATH + "/" + DB_NAME;
}
