package ra.dbengine.interfaces;

public interface DbDisconnectingListener {

	public void onDbDisconnecting();

}

