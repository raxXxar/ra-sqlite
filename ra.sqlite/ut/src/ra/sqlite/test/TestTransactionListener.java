package ra.sqlite.test;

import android.database.sqlite.SQLiteTransactionListener;

public class TestTransactionListener implements SQLiteTransactionListener {

	public boolean wasBegin;
	public boolean wasRollback;
	public boolean wasCommit;

	public void reset() {
		wasBegin = false;
		wasRollback = false;
		wasCommit = false;
	}

	public void onRollback() {
		wasRollback = true;
	}

	public void onCommit() {
		wasCommit = true;
	}

	public void onBegin() {
		wasBegin = true;
	}
};