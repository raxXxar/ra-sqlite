package ra.dbengine.interfaces;

import java.util.UUID;
import android.database.Cursor;

public interface CursorTechHole {

	public UUID getUUID();

	public void replaceCursorWith(Cursor cursor);

	public void notifyDataChanged();

	public void notifyContentChanged();

	public Cursor getCursor();

	public ChangeListener getChangeListener();
}
