/*
 * ra_dbut.h
 *
 *  Created on: 10 груд. 2012
 *      Author: akulyn
 */

#ifndef RA_DBUT_H_
#define RA_DBUT_H_

#include <jni.h>
#include "sqlite3.h"
#include "ra_sqlite_SQLiteDatabase_.h"

jint prepareStatement(JNIEnv* jniEnv, Dbh* pDbh, jstring strSql);

void resetStatement(JNIEnv* jniEnv, sqlite3_stmt* handle);

void finalizeStatement(Dbh* pDbh, sqlite3_stmt* handle);

jobjectArray getStatementColumnNames(JNIEnv* jniEnv, sqlite3_stmt* handle);

jstring getStatementColumnName(JNIEnv* jniEnv,
                               sqlite3_stmt* handle,
                               jint columnIndex);
/**
 * case sensitive
 */
int getStatementColumnIndexCS(sqlite3_stmt* handle, const char* columnName);

jint getStatementArgsCount(sqlite3_stmt* handle);

jint execStatement(JNIEnv* jniEnv, sqlite3_stmt* handle);

jlong columnInt64Value(sqlite3_stmt* handle, jint columnIndex);

jstring columnStringValue(JNIEnv* jniEnv,
                          sqlite3_stmt* handle,
                          jint columnIndex);

void statementBindNull(JNIEnv* jniEnv, sqlite3_stmt* handle, jint index);

void statementBindDouble(JNIEnv* jniEnv,
                         sqlite3_stmt* handle,
                         jint index,
                         jdouble valueDouble);

void statementBindLong(JNIEnv* jniEnv,
                       sqlite3_stmt* handle,
                       jint index,
                       jlong valueLong);

void statementBindString(JNIEnv* jniEnv,
                         sqlite3_stmt* handle,
                         jint index,
                         jstring valueString);

void statementBindByteBuffer(JNIEnv* jniEnv,
                             sqlite3_stmt* handle,
                             jint index,
                             jobject valueByteBuffer);

void statementBindByteArray(JNIEnv* jniEnv,
                            sqlite3_stmt* handle,
                            jint index,
                            jbyteArray valueByteArray);

#endif /* RA_DBUT_H_ */
