/*
 * ra_hnd_map.c
 *
 *       Author: =ra=
 */

#include <malloc.h>
#include <stdint.h>
#include <string.h>
#include "thread_lock.h"
#include "ra_sqlite.h"

#include "ra_hnd_map.h"

typedef union _Handle {
	struct {
		uint16_t _idx;
		uint16_t _id;
	};
	Handle handle;
} _Handle;

typedef struct HndEntry {
	uint16_t id;
	void* ptr;
} HndEntry;

struct HndMap {
	uint16_t _id;
	HndEntry* pEntries;
	uint16_t size;
	uint16_t growthSize;
	// thread safety
	__LMEMBER;
};

#define MK_HND(idx,id)		{idx, id}
#define GET_IDX(hndl)		(((_Handle) (hndl))._idx)
#define GET__ID(hndl)		(((_Handle) (hndl))._id)
#define GET_HND(hndl)		(((_Handle) (hndl)).handle)

/**
 * return old HndMap size
 */
static uint16_t __HndMap_Resize(HndMap* pHndMap /*assume !NULL*/);
static uint16_t __findEmptyIdx(HndMap* pHndMap/*assume !NULL*/);

HndMap* HndMap_Create(uint16_t size, uint16_t growthSize) {
	HndMap* pHndMap = (HndMap*) malloc(sizeof(HndMap));
	if (NULL != pHndMap) {
		pHndMap->_id = 0;
		pHndMap->pEntries = (HndEntry*) calloc(size, sizeof(HndEntry));
		if (NULL != pHndMap->pEntries) {
			pHndMap->size = size;
			pHndMap->growthSize = growthSize;
			__INIT(pHndMap);
		} else {
			free(pHndMap);
			pHndMap = NULL;
		}
	}
	return pHndMap;
}

HndMap* HndMap_Free(HndMap* pHndMap) {
	if (NULL != pHndMap) {
		__LOCK(pHndMap);
		free(pHndMap->pEntries);
		pHndMap->pEntries = NULL;
		pHndMap->size = 0;
		__UNLOCK(pHndMap);
		free(pHndMap);
	}
	return NULL;
}

Handle HndMap_Insert(HndMap* pHndMap, void* ptr) {
	Handle result = 0;
	if ((NULL != ptr) && (NULL != pHndMap)) {
		__LOCK(pHndMap);
		uint16_t freeEntryIdx = __findEmptyIdx(pHndMap);
		if (freeEntryIdx == pHndMap->size) {
			uint16_t oldSize = __HndMap_Resize(pHndMap);
			if (oldSize == pHndMap->size) {
				/* no room to hold ptr */
				__UNLOCK(pHndMap);
				return 0;
			}
		}
		_Handle handle = MK_HND(freeEntryIdx, ++pHndMap->_id); // enough
		pHndMap->pEntries[freeEntryIdx].id = GET__ID(handle);
		pHndMap->pEntries[freeEntryIdx].ptr = ptr;
		result = GET_HND(handle);
		__UNLOCK(pHndMap);
	}
	return result;
}

void* HndMap_getPtr(HndMap* pHndMap, Handle handle) {
	void* result = NULL;
	if ((0 != handle) && (NULL != pHndMap)) {
		__LOCK(pHndMap);
		uint16_t idx = GET_IDX(handle);
		if (idx >= 0 && idx < pHndMap->size) {
			if (pHndMap->pEntries[idx].id == GET__ID(handle) ) {
				result = pHndMap->pEntries[idx].ptr;
			}
		}
		__UNLOCK(pHndMap);
	}
	return result;
}

void HndMap_removePtr(HndMap* pHndMap, Handle handle) {
	if ((0 != handle) && (NULL != pHndMap)) {
		__LOCK(pHndMap);
		uint16_t idx = GET_IDX(handle);
		if (idx >= 0 && idx < pHndMap->size) {
			if (pHndMap->pEntries[idx].id == GET__ID(handle) ) {
				pHndMap->pEntries[idx].id = 0;
				pHndMap->pEntries[idx].ptr = NULL;
			}
		}
		__UNLOCK(pHndMap);
	}
}

static uint16_t __findEmptyIdx(HndMap* pHndMap/*assume !NULL*/) {
	uint16_t idx;
	for (idx = 0; idx < pHndMap->size; ++idx) {
		if (0 == pHndMap->pEntries[idx].id) {
			return idx;
		}
	}
	return pHndMap->size;
}

/**
 * return old HndMap size
 */
static uint16_t __HndMap_Resize(HndMap* pHndMap /*assume !NULL*/) {
	uint16_t oldSize = pHndMap->size;
	uint16_t newSize = oldSize + pHndMap->growthSize;
	HndEntry* pNewEntries = (HndEntry*) realloc(pHndMap->pEntries,
	                                            newSize * sizeof(HndEntry));
	if (NULL != pNewEntries) {
		int zeroedSize = newSize - pHndMap->size;
		if (zeroedSize > 0) {
			memset(pNewEntries + pHndMap->size,
			       0,
			       sizeof(HndEntry) * zeroedSize);
		}
		pHndMap->pEntries = pNewEntries;
		pHndMap->size = newSize;
	}
	return oldSize;
}
