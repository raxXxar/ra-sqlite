LOCAL_PATH := $(call my-dir)

src_android_files := 				\
	ra_sqlite.c						\
	ra_exception.c					\
	ra_list.c						\
	ra_extent.c						\
	ra_dynarr.c						\
	ra_recordset.c					\
	ra_cursor.c						\
	ra_sqlite_SQLiteDatabase.c		\
	ra_sqlite_SQLiteCursor.c		\
	ra_x_IndexAccessorRa.c			\
	ra_dbut.c						\
	ra_pool.c						\
	ra_collator.c					\
	ra_hnd_map.c					\
	ra_accessor.c
	
			
src_files := $(addprefix src/,$(src_android_files))


local_ldlibs := -lz -lm -llog -lc -L$(call host-path, $(LOCAL_PATH))/$(TARGET_ARCH_ABI) 

c_includes := $(LOCAL_PATH)	

cf_includes_local := inc
cf_includes_local_unicode := inc
	
	 
cf_includes := $(addprefix -Ijni/android/,$(cf_includes_local))  $(addprefix -Ijni/sqlite/,$(cf_includes_local_unicode)) 

export_c_includes := $(c_includes)

# We make the ICU data directory relative to $ANDROID_ROOT on Android, so both
# device and sim builds can use the same codepath, and it's hard to break one
# without noticing because the other still works.
local_cflags := -O3 -DPIC -fPIC -DANDROID_NDK -DTHREAD_SAFE


include $(CLEAR_VARS)

LOCAL_MODULE			:= libra_sqlite
LOCAL_MODULE_TAGS		:= optional
LOCAL_CFLAGS 			:= $(local_cflags) 
LOCAL_CFLAGS 			+= $(cf_includes)
LOCAL_LDLIBS			+= $(local_ldlibs)
LOCAL_EXPORT_C_INCLUDES := $(export_c_includes)
LOCAL_C_INCLUDES		:= $(c_includes)
LOCAL_SRC_FILES			:= $(src_files)
LOCAL_STATIC_LIBRARIES  := libicusqlite cpufeatures 


include $(BUILD_SHARED_LIBRARY)
$(call import-module,android/cpufeatures)
