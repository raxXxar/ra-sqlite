package ra.dbengine.interfaces;

import java.util.UUID;
import ra.dbengine.CursorOrder;
import ra.dbengine.QProcessor;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import android.content.ContentValues;
import android.database.Cursor;

/**
 * The Interface <b>DbEngine</b> used together with <b>DbEngineProvider</b>.
 * e.g. DbEngineProvider.getDbEngine().openCursorA(someCursorOrder);
 * 
 * <br>DbEngine may be cached not longer than function execution time.
 * <br>Please do not cache it as a class member.
 * 
 * <br>DbEngineProvider can switch active DbEngine at any time. 
 * <br><b>Please don't trust it immutable during application execution.</b>
 */
/**
 *  @author =ra=
 */
public interface DbEngine {

	/**
	 * Create, but not open cursor.
	 * 
	 * WARNING: do not try to use the same CursorOrder object to obtain new cursor. 
	 * 
	 * @param cursorOrder the cursor order
	 * @return the CursorProxy object with a NullCursor immediately
	 */
	public Cursor createCursor(CursorOrder cursorOrder);

	/**
	 * Open cursor asynchronously in background thread.
	 * 
	 * WARNING: do not try to use the same CursorOrder object to obtain new cursor. 
	 * 
	 * @param cursorOrder the cursor order
	 * @return the CursorProxy object with NullCursor as a real cursor immediately.
	 * 
	 * Asynchronous cursor preparation task is fired. After task completion NullCursor is
	 * transparently replaced with real DB cursor, CursorProxy receives data/content 
	 * change notifications.  
	 * 
	 */
	public Cursor openCursorA(CursorOrder cursorOrder);

	/**
	 * Open cursor associated with lockKey asynchronously in background thread.
	 * 
	 * WARNING: do not try to use the same CursorOrder object to obtain new cursor. 
	 *  
	 *  <br>if there no associated cursor found - new, based on cursorOrder parameters cursor 
	 *  <br>is being created ( @see {@link #openCursorA(CursorOrder)} ).
	 *  
	 *  <br>if the associated cursor found - cursorOrder is populated with arguments this cursor belongs
	 *  
	 *  <br>in any way after method executing is not locked anymore.
	 *
	 * @param cursorOrder the cursor order
	 * @param lockKey the lock key
	 * @return the cursor
	 */
	public Cursor openCursorA(CursorOrder cursorOrder, String lockKey);

	/**
	 * Open cursor synchronously in caller thread.
	 * 
	 * WARNING: do not try to use the same CursorOrder object to obtain new cursor. 
	 * 
	 * @param cursorOrder the cursor order
	 * @return the CursorProxy object with a real cursor immediately
	 */
	public Cursor openCursor(CursorOrder cursorOrder);

	/**
	 * Open cursor associated with lockKey synchronously in caller thread.
	 * 
	 * WARNING: do not try to use the same CursorOrder object to obtain new cursor. 
	 * 
	 *  <br>if there no associated cursor found - new, based on cursorOrder parameters cursor 
	 *  <br>is being created ( @see {@link #openCursor(CursorOrder)} ).
	 *  
	 *  <br>if the associated cursor found - cursorOrder is populated with arguments this cursor belongs
	 *  
	 *  <br>in any way after method executing is not locked anymore.
	 *
	 * @param cursorOrder the cursor order
	 * @param lockKey the lock key
	 * @return the CursorProxy object with a real cursor immediately
	 */
	public Cursor openCursor(CursorOrder cursorOrder, String lockKey);

	/**
	 * Lock cursor associated with specified uuid with lockKey string.
	 * <br>Cursor locked means that cursor, associated with cursor cursorOrder become 
	 * <br>immortable during application executing despite it is out of scope. 
	 *
	 * @param uuid the uuid
	 * @param lockKey the lock key
	 */
	public void lockCursor(UUID uuid, String lockKey);

	/**
	 * Asynchronously changes cursor associated with cursor order according to cursorOrders SqlCommandSource.
	 *
	 * @param cursorOrder the cursor order
	 */
	public void changeCursorA(CursorOrder cursorOrder);

	/**
	 *  Changes cursor associated with cursor order according to cursorOrders SqlCommandSource.
	 *
	 * @param cursorOrder the cursor order
	 */
	public void changeCursor(CursorOrder cursorOrder);

	/**
	 * Executes DDL or DML statement contained in argument.
	 *
	 * @param cmdSrc the SqlCommandSource to proceed
	 */
	public void execute(SqlCommandSource cmdSrc);

	/**
	 * Executes block of DDL or DML statement contained in argument.
	 *
	 * @param cmdSrc the SqlCommandSource to proceed
	 */
	public void executeBlock(SqlCommandSource cmdSrc);

	/**
	 * Gets the DbEngine SQL/DDL/DML processor.
	 *
	 * @return the QL/DDL/DML processor
	 */
	public QProcessor getProcessor();

	/**
	 *  Disconnects DataBase Engine from real database.
	 */
	public void dbDisconnect();

	/**
	 * Connects engine to DataBase.
	 *
	 * @throws DbIncorrectVersionException the DataBase incorrect version exception
	 * @throws DbOpenErrorException the DataBase open error exception
	 */
	public void dbConnect() throws DbIncorrectVersionException, DbOpenErrorException;

	/**
	 * Register cursor data change listener.
	 *
	 * @param cursorOrder the cursor order
	 * @param changeListener the change listener
	 */
	public void registerCursorDataChangeListener(CursorOrder cursorOrder, ChangeListener changeListener);

	/**
	 * Unregister cursor data change listener.
	 *
	 * @param cursorOrder the cursor order
	 * @param changeListener the change listener
	 */
	public void unRegisterCursorDataChangeListener(CursorOrder cursorOrder, ChangeListener changeListener);

	/**
	 * Register tag change listener.
	 *
	 * @param inciterTags the inciter tags
	 * @param changeListener the change listener
	 */
	public void registerTagChangeListener(String[] inciterTags, ChangeListener changeListener);

	/**
	 * Unregister tag change listener.
	 *
	 * @param inciterTags the inciter tags
	 * @param changeListener the change listener
	 */
	public void unRegisterTagChangeListener(String[] inciterTags, ChangeListener changeListener);
	
	/**
	 * Register database disconnecting listener.
	 * 
	 * @param disconnectingListener the disconnecting listener
	 */
	public void registerDbDisconnectingListener(DbDisconnectingListener disconnectingListener);
	
	/**
	 * Unregister database disconnecting listener.
	 * 
	 * @param disconnectingListener the disconnecting listener
	 */
	public void unRegisterDbDisconnectingListener(DbDisconnectingListener disconnectingListener);

	@Deprecated
	public void notifyEventPropeller(String[] affectedTablesList);

	public void execBind(String sql, Object[] bindArgs);
	
	public void execBind(SqlCommandSource cmdSrc, Object[] bindArgs);

	public long insert(String table, String nullColumnHack, ContentValues initialValues);

	public int update(String table, ContentValues values, String whereClause, String[] whereArgs);

	public long replace(String table, String nullColumnHack, ContentValues initialValues);

	public int delete(String table, String whereClause, String[] whereArgs);
}
