/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class ra_x_IndexAccessorRa */
#ifndef _Included_ra_x_IndexAccessorRa
#define _Included_ra_x_IndexAccessorRa
#undef ra_x_IndexAccessorRa_POSITION_UNDEFINED
#define ra_x_IndexAccessorRa_POSITION_UNDEFINED -1
#undef ra_x_IndexAccessorRa_ACCESSOR_UNDEFINED
#define ra_x_IndexAccessorRa_ACCESSOR_UNDEFINED 0
/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Deactivate
 * Signature: (I)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Deactivate(JNIEnv *, jobject, jint);

/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Init
 * Signature: (ILjava/lang/String;)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Init(JNIEnv *, jobject, jint, jstring);

/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Locate
 * Signature: (IJ)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Locate(JNIEnv* jniEnv,
                                                        jobject accessor,
                                                        jint accessorId,
                                                        jlong indexValue);

#endif
