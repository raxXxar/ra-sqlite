package ra.dbengine.utils;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

/**
 * The NullCursor is a fake cursor implementation invented to simplify Cursor-around business logic. 
 */
/**
 *  @author =ra=
 */
public class NullCursor implements Cursor {

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public int getPosition() {
		return 0;
	}

	@Override
	public boolean move(int offset) {
		return true;
	}

	@Override
	public boolean moveToPosition(int position) {
		return true;
	}

	@Override
	public boolean moveToFirst() {
		return true;
	}

	@Override
	public boolean moveToLast() {
		return true;
	}

	@Override
	public boolean moveToNext() {
		return true;
	}

	@Override
	public boolean moveToPrevious() {
		return true;
	}

	@Override
	public boolean isFirst() {
		return true;
	}

	@Override
	public boolean isLast() {
		return true;
	}

	@Override
	public boolean isBeforeFirst() {
		return true;
	}

	@Override
	public boolean isAfterLast() {
		return true;
	}

	@Override
	public int getColumnIndex(String columnName) {
		return 0;
	}

	@Override
	public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
		return 0;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return null;
	}

	@Override
	public String[] getColumnNames() {
		return null;
	}

	@Override
	public int getColumnCount() {
		return 0;
	}

	@Override
	public byte[] getBlob(int columnIndex) {
		return null;
	}

	@Override
	public String getString(int columnIndex) {
		return null;
	}

	@Override
	public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
	}

	@Override
	public short getShort(int columnIndex) {
		return 0;
	}

	@Override
	public int getInt(int columnIndex) {
		return 0;
	}

	@Override
	public long getLong(int columnIndex) {
		return 0;
	}

	@Override
	public float getFloat(int columnIndex) {
		return 0;
	}

	@Override
	public double getDouble(int columnIndex) {
		return 0;
	}

	@Override
	public boolean isNull(int columnIndex) {
		return false;
	}

	@Override
	public void deactivate() {

	}

	@Override
	public boolean requery() {
		return true;
	}

	@Override
	public void close() {
		mIsClosed = true;
	}

	@Override
	public boolean isClosed() {
		return mIsClosed;
	}

	@Override
	public void registerContentObserver(ContentObserver observer) {
	}

	@Override
	public void unregisterContentObserver(ContentObserver observer) {
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public void setNotificationUri(ContentResolver cr, Uri uri) {
	}

	@Override
	public boolean getWantsAllOnMoveCalls() {
		return false;
	}

	@Override
	public Bundle getExtras() {
		return Bundle.EMPTY;
	}

	@Override
	public Bundle respond(Bundle extras) {
		return Bundle.EMPTY;
	}

	private boolean mIsClosed = false;

	@Override
	public int getType(int columnIndex) {
		return 0;
	}
}
