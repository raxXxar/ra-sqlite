package ra.dbengine.interfaces;

import ra.dbengine.QProcessor;

/**
 *  @author =ra=
 */
public interface QProcessorProvider extends Provider<QProcessor> {
}
