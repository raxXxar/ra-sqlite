package ra.db;

import ra.dbengine.ISQLDatabase;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import ra.dbengine.interfaces.DbProvider;
import ra.dbengine.interfaces.QProcessorProvider;
import android.content.Context;

public abstract class DbConfig {

	private DbConfig() {
	};

	protected DbConfig(Class<? extends DbProvider<?>> dbProviderClass, Class<? extends QProcessorProvider> qProcessorProviderClass, Context context,
			String localeStr) {
		this();
		this.dbProviderClass = dbProviderClass;
		this.qProcessorProviderClass = qProcessorProviderClass;
		this.context = context;
		this.localeStr = localeStr;
		if (null == this.context) {
			throw new IllegalArgumentException("DbConfig: context is null");
		}
	}

	public abstract String getDbName();

	public abstract int getDbVersion();

	public abstract void prepareDb(ISQLDatabase db);

	public Context getContext() {
		return context;
	}

	public ISQLDatabase getDB(Context context) throws DbIncorrectVersionException, DbOpenErrorException {
		return (ra.dbengine.ISQLDatabase) getDBProvider(context).get();
	}

	public DbProvider<?> getDBProvider(Context context) {
		DbProvider<?> result = null;
		try {
			result = dbProviderClass.newInstance();
			result.setConfig(this);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		return result;
	}

	public QProcessorProvider getQProcessorProvider() {
		QProcessorProvider result = null;
		try {
			result = qProcessorProviderClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void setLocale(ISQLDatabase db) {
		if (null != localeStr) {
			db.setLocale(localeStr);
		}
	}

	protected String localeStr = "en_US";
	protected Context context;
	protected Class<? extends DbProvider<?>> dbProviderClass;
	protected Class<? extends QProcessorProvider> qProcessorProviderClass;

}
