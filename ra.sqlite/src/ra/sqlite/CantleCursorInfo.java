package ra.sqlite;

import ra.x.AccessorFactory;
import ra.x.IndexAccessor;
import android.database.Cursor;

public class CantleCursorInfo {

	/**
	 *  Constructs master cursor information
	 * @param cursor
	 */
	public CantleCursorInfo(Cursor cursor) {
		this(null, cursor, null, null);
	}

	/**
	 * Constructs master cursor information
	 * @param cursor
	 * @param alias
	 */
	public CantleCursorInfo(Cursor cursor, String alias) {
		this(null, cursor, null, alias);
	}

	/**
	 * Constructs detail cursor information
	 * @param mastertColumn
	 * @param cursor
	 * @param indexColumn
	 */
	public CantleCursorInfo(String mastertColumn, Cursor cursor, String indexColumn) {
		this(mastertColumn, cursor, indexColumn, null);
	}

	/**
	 * Constructs detail cursor information
	 * @param mastertColumn
	 * @param cursor
	 * @param indexColumn
	 * @param alias
	 */
	public CantleCursorInfo(String mastertColumn, Cursor cursor, String indexColumn, String alias) {
		if ((null != mastertColumn && null == indexColumn) || null == cursor) {
			throw new IllegalArgumentException("Cursor or masterColumn name is null");
		}
		this.mastertColumn = mastertColumn;
		this.indexColumn = indexColumn;
		this.cursor = cursor;
		this.alias = (null == alias) ? null : alias.toLowerCase();
	}

	public void setUp(String alias, int globalStartColumnIndex, int masterIndexColumn) {
		this.alias = (null == alias) ? null : alias.toLowerCase();
		this.globalStartColumnIndex = globalStartColumnIndex;
		this.masterColumnIndex = masterIndexColumn;
		accessor = AccessorFactory.createAccessor(cursor, indexColumn);
	}

	public IndexAccessor getAccessor() {
		return accessor;
	}

	public String getMasterColumn() {
		return mastertColumn;
	}

	public int getMasterColumnIndex() {
		return masterColumnIndex;
	}

	public String getIndexColumn() {
		return indexColumn;
	}

	public String getAlias(String defaultAlias) {
		return (alias != null) ? alias : defaultAlias;
	}

	public String getAlias() {
		return alias;
	}

	public Cursor getCursor() {
		return cursor;
	}

	public int getStartGlobalColumnIndex() {
		return globalStartColumnIndex;
	}

	public int getLastGlobalColumnIndex() {
		return globalStartColumnIndex + cursor.getColumnCount() - 1;
	}

	public CantleCursorColumn[] getColumnsInfo() {
		CantleCursorColumn[] columnsInfo = new CantleCursorColumn[cursor.getColumnCount()];
		for (int idx = 0; idx < columnsInfo.length; ++idx) {
			columnsInfo[idx] = new CantleCursorColumn(this, idx);
		}
		return columnsInfo;
	}

	private int masterColumnIndex;
	private int globalStartColumnIndex;
	private String alias;
	private IndexAccessor accessor;

	private final String mastertColumn;
	private final String indexColumn;
	private final Cursor cursor;

}
