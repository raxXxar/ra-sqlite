/*
 * ra_dynarr.c
 *
 *       Author: =ra=
 */

#include <jni.h>
#include <stdlib.h>
#include <string.h>

#include "thread_lock.h"
#include "ra_sqlite.h"
#include "ra_dynarr.h"

struct Array {
	Item* items;
	jint size;
	jint capacity;
	jint growthSize;
	FnPEquivalence equivalenceFunction;
	FnPDestructor destructorFunction;
	jint removed;
	// thread safety
	__LMEMBER;
};

static void __Array_Resize(Array* pArray, jint newCapacity);
static void __Array_Pack(Array* pArray);
static jint __Array_Exists(Array* pArray, Item item);

Array* Array_Create(jint initialCapacity,
                    jint growthSize,
                    FnPEquivalence equivalenceFunction,
                    FnPDestructor destructorFunction) {
	Array* pArray = (Array*) malloc(sizeof(Array));
	pArray->size = 0;
	pArray->capacity = 0;
	pArray->removed = 0;
	pArray->growthSize = growthSize;
	pArray->equivalenceFunction = equivalenceFunction;
	pArray->destructorFunction = destructorFunction;
	pArray->items = NULL;
	__Array_Resize(pArray, initialCapacity);
	__INIT(pArray);
	return pArray;
}

void Array_Free(Array* pArray) {
	__LOCK(pArray);
	if (NULL != pArray->destructorFunction) {
		jint idx;
		for (idx = 0; idx < pArray->size; ++idx) {
			if (NULL != pArray->items[idx]) {
				pArray->destructorFunction(pArray->items[idx]);
			}
		}
	}
	free(pArray->items);
	__UNLOCK(pArray);
	free(pArray);
}

void Array_ForEach(Array* pArray, FnPTransformer transformFunction) {
	__LOCK(pArray);
	jint idx;
	for (idx = 0; idx < pArray->size; ++idx) {
		if (NULL != pArray->items[idx]) {
			transformFunction(pArray->items[idx]);
		}
	}
	__UNLOCK(pArray);
}

static jint __Array_Exists(Array* pArray, Item item) {
	jint result = JNI_FALSE;
	jint idx;
	for (idx = 0; idx < pArray->size; ++idx) {
		if ((NULL != pArray->items[idx])
		    && (pArray->equivalenceFunction(item, pArray->items[idx]))) {
			result = JNI_TRUE;
			break;
		}
	}
	return result;
}

jint Array_Exists(Array* pArray, Item item) {
	__LOCK(pArray);
	jint result = __Array_Exists(pArray, item);
	__UNLOCK(pArray);
	return result;
}

jint Array_Insert(Array* pArray, Item item) {
	__LOCK(pArray);
	jint isInserted = JNI_FALSE;
	if (!__Array_Exists(pArray, item)) {
		if (pArray->size == pArray->capacity) {
			if (pArray->removed) {
				__Array_Pack(pArray);
			} else {
				__Array_Resize(pArray, pArray->capacity + pArray->growthSize);
			}
		}
		pArray->items[pArray->size++] = item;
		isInserted = JNI_TRUE;
	}
	__UNLOCK(pArray);
	return isInserted;
}

jint Array_RemoveEntry(Array* pArray, Item item, jint bDestroy) {
	__LOCK(pArray);
	jint isRemoved = JNI_FALSE;
	jint idx;
	for (idx = 0; idx < pArray->size; ++idx) {
		if ((NULL != pArray->items[idx])
		    && (pArray->equivalenceFunction(item, pArray->items[idx]))) {
			if (bDestroy && pArray->destructorFunction) {
				pArray->destructorFunction(pArray->items[idx]);
				pArray->items[idx] = 0;
			}
			isRemoved = JNI_TRUE;
			pArray->removed++;
			break;
		}
	}
	__UNLOCK(pArray);
	return isRemoved;
}

jint intEquals(Item a, Item b) {
	return ((jint) a) == ((jint) b);
}

static void __Array_Resize(Array* pArray, jint newCapacity) {
	Item* newPtr = (Item*) realloc(pArray->items, newCapacity * sizeof(Item));
	if (NULL != newPtr) {
		pArray->items = newPtr;
		pArray->capacity = newCapacity;
		pArray->size =
		    (newCapacity < pArray->size) ? newCapacity : pArray->size;
	}
}

static void __Array_Pack(Array* pArray) {
	Item* beg = pArray->items;
	Item* end = pArray->items + ((pArray->size - 1));
	while (beg < end) {
		while ((beg < end) && (0 != *beg)) {
			++beg;
		}
		if ((beg < end) /* && 0 == *beg */) {
			while ((beg < end) && (0 == *end)) {
				--end;
			}
			if (beg < end /* && (0 != *end) */) {
				*beg = *end;
				*end = 0;
				++beg;
				--end;
			}
		}
	}
	pArray->size -= pArray->removed;
	pArray->removed = 0;
}
