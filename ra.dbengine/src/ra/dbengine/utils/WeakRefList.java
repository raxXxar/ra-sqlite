package ra.dbengine.utils;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *  @author =ra=
 */
public class WeakRefList<E> extends AbstractList<E> {

	public WeakRefList() {
	}

	public WeakRefList(Collection<E> initialCollection) {
		addAll(0, initialCollection);
	}

	@Override
	public void add(int index, E element) {
		addElement(index, element);
	}

	public WeakReference<Object> addElement(int index, E element) {
		synchronized (this) {
			WeakReference<Object> obj = new WeakReference<Object>(element, mReleasedQueue);
			mReferences.add(index, obj);
			return obj;
		}
	}

	@Override
	public boolean add(E element) {
		synchronized (this) {
			return mReferences.add(new WeakReference<Object>(element, mReleasedQueue));
		}
	}

	public Iterator<E> iterator() {
		return new Iterator<E>() {

			private int currentIndex = 0;
			private Object[] arrayList = toArray();

			@Override
			public boolean hasNext() {
				return currentIndex < arrayList.length && arrayList[currentIndex] != null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public E next() {
				return (E) arrayList[currentIndex++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public int size() {
		synchronized (this) {
			processQueue();
			return mReferences.size();
		}
	}

	@Override
	public E get(int index) {
		synchronized (this) {
			processQueue();
			@SuppressWarnings("unchecked")
			E result = (E) mReferences.get(index).get();
			return result;
		}
	}

	public WeakReference<Object> getCorrespondingWeakReference(Object object) {
		synchronized (this) {
			ListIterator<WeakReference<Object>> referenceListIterator = mReferences.listIterator();
			WeakReference<Object> weakReference;
			Object element;
			while (referenceListIterator.hasNext()) {
				weakReference = referenceListIterator.next();
				if (null != (element = weakReference.get()) && element.equals(object)) {
					return weakReference;
				}
			}
			return null;
		}
	}

	@Override
	public boolean remove(Object object) {
		synchronized (this) {
			ListIterator<WeakReference<Object>> referenceListIterator = mReferences.listIterator();
			WeakReference<Object> weakReference;
			Object element;
			while (referenceListIterator.hasNext()) {
				weakReference = referenceListIterator.next();
				if (null != (element = weakReference.get()) && element.equals(object)) {
					return mReferences.remove(weakReference);
				}
			}
			return false;
		}
	}

	public boolean removeWeakReference(WeakReference<Object> weakReference) {
		synchronized (this) {
			return mReferences.remove(weakReference);
		}
	}

	@Override
	public Object[] toArray() {
		synchronized (this) {
			processQueue();
			int size = mReferences.size();
			Object[] copy = new Object[size];
			int copyIndex = 0;
			for (WeakReference<Object> item : mReferences) {
				copy[copyIndex++] = item.get();
			}
			return copy;
		}
	}

	@SuppressWarnings("unchecked")
	public E[] asArray(E[] templateArray) {
		synchronized (this) {
			processQueue();
			int size = mReferences.size();
			E[] copy = (E[]) Array.newInstance(templateArray.getClass().getComponentType(), size);
			int copyIndex = 0;
			for (WeakReference<Object> item : mReferences) {
				copy[copyIndex++] = (E) item.get();
			}
			return copy;
		}
	}

	@Override
	public <T> T[] toArray(T[] templateArray) {
		throw new UnsupportedOperationException();
	}

	private void processQueue() {
		Reference<?> clearedReference;
		while (null != (clearedReference = mReleasedQueue.poll())) {
			mReferences.remove(clearedReference);
		}
	}

	private final List<WeakReference<Object>> mReferences = new LinkedList<WeakReference<Object>>();
	private final ReferenceQueue<? super Object> mReleasedQueue = new ReferenceQueue<Object>();
}
