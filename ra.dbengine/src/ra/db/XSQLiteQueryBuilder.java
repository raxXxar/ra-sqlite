package ra.db;

import ra.dbengine.ISQLDatabase;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;

public class XSQLiteQueryBuilder extends SQLiteQueryBuilder {

	public Cursor query(ISQLDatabase db, String[] projectionIn, String selection, String[] selectionArgs, String groupBy, String having, String sortOrder) {
		return query(db, projectionIn, selection, selectionArgs, groupBy, having, sortOrder, null /* limit */);
	}

	@SuppressWarnings("deprecation")
	public Cursor query(ISQLDatabase db, String[] projectionIn, String selection, String[] selectionArgs, String groupBy, String having, String sortOrder,
			String limit) {
		if (null == getTables()) {
			return null;
		}
		String sql = buildQuery(projectionIn, selection, selectionArgs, groupBy, having, sortOrder, limit);
		return db.rawQuery(sql, selectionArgs);
	}
}
