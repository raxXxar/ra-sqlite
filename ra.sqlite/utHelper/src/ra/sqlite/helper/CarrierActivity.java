package ra.sqlite.helper;

import ra.retainer.DataRetainer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class CarrierActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_carrier);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d("XXX", "onSaveInstanceState");
		super.onSaveInstanceState(outState);
		DataRetainer.retain(this, outState);
	}
}
