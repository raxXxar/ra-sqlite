/*
 * ra_accessor.c
 *
 *  Author: =ra=
 */

#include <jni.h>
#include <malloc.h>
#include <string.h>
#include "ra_accessor.h"
#include "thread_lock.h"
#include "ra_hnd_map.h"
#include "ra_x_IndexAccessorRa.h"
#include "ra_pool.h"
#include "ra_sqlite_SQLiteCursor_.h"

#define ACCESSOR_DEF_CAPACITY 10

typedef struct BlockIndex {
	jint blockStartPosition;
	jint blockSize;
	jlong maxBlockIndexValue;
} BlockIndex;

struct Accessor {
	// pFLIndex service info
	jint FLIndexSize;
	jint FLIndexCapacity;
	BlockIndex* pFLIndex;
	// cursor handle in g_pool
	Handle cursorHandle;
	// accessor column
	char* columnName;
	jint columnPosition;
	// last accessed value (start search position optimisation)
	jint curentValueBlockPosition; // currentValue block position in pFLIndex
	jint curentValueRSPosition; //  currentValue row position in Cursor
	jlong currentValue;
	Handle g_poolId;
	// thread safety
	__LMEMBER
	;
};

static jint __Accessor_Resize(Accessor* pAccessor /* assume !NULL */);

static jint __Accessor_CheckIsSame(Accessor* pAccessor /* assume !NULL */,
                                   const char* newColumnName,
                                   jint newColumnPosition);

static jint __IsDefaultColumn(const char* newColumnName);

static void __Accessor_Deactivate(Accessor* pAccessor /* assume !NULL */);

static void __Accessor_AppendBlockIndex(Accessor* pAccessor/* assume !NULL */,
                                        jint blockStartPosition,
                                        jint blockSize,
                                        jlong maxBlockIndexValue);

static void Accessor_RebuildIndex(Handle cursorHandle);

static jint skip_N_read(sqlite3_stmt* pStatement, jint skipCount, jint columnIndex2Read, jlong* lastReadColumnValue);

Accessor* Accessor_Create(Handle cursorHandle, const char* columnName, jint columnPosition) {
	Cursor* pCursor = (Cursor*) getInPool(cursorHandle);
	if (!pCursor || !columnName || columnPosition < 0 || (columnPosition >= Cursor_GetColumnCount(pCursor))) {
		return NULL ;
	}
	char* columnNameCopy = strdup(columnName);
	if (!columnNameCopy) {
		return NULL ;
	}
	Accessor* pAccessor = (Accessor*) malloc(sizeof(Accessor));
	if (!pAccessor) {
		return NULL ;
	}
	pAccessor->FLIndexCapacity = ACCESSOR_DEF_CAPACITY;

	pAccessor->pFLIndex = (BlockIndex*) malloc(pAccessor->FLIndexCapacity * sizeof(BlockIndex));
	if (NULL != pAccessor->pFLIndex) {
		pAccessor->FLIndexSize = 0;
		pAccessor->cursorHandle = cursorHandle;
		pAccessor->columnName = columnNameCopy;
		pAccessor->columnPosition = columnPosition;
		pAccessor->curentValueBlockPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
		pAccessor->curentValueRSPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
		pAccessor->currentValue = INT64_MIN;
		pAccessor->g_poolId = registerInPool(pAccessor);
		__INIT(pAccessor);
	} else {
		free(columnNameCopy);
		free(pAccessor);
		pAccessor = NULL;
	}
	return pAccessor;
}

Accessor* Accessor_Free(Accessor* pAccessor) {
	return NULL ;
	if (NULL != pAccessor) {
		__LOCK(pAccessor);
		free(pAccessor->columnName);
		pAccessor->columnName = NULL;
		pAccessor->columnPosition = 0;
		free(pAccessor->pFLIndex);
		pAccessor->pFLIndex = NULL;
		pAccessor->FLIndexCapacity = 0;
		pAccessor->FLIndexSize = 0;
		pAccessor->curentValueBlockPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
		pAccessor->curentValueRSPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
		pAccessor->currentValue = INT64_MIN;
		unregisterInPool(pAccessor->g_poolId);
		pAccessor->g_poolId = 0;
		__UNLOCK(pAccessor);
		free(pAccessor);
	}
	return NULL ;
}

jint Accessor_GeIndexColumnIndex(Accessor* pAccessor) {
	jint indexColumn = -1;
	if (NULL != pAccessor) {
		indexColumn = pAccessor->columnPosition;
	}
	return indexColumn;
}

void Accessor_Deactivate(Accessor* pAccessor) {
	if (NULL != pAccessor) {
		__LOCK(pAccessor);
		__Accessor_Deactivate(pAccessor);
		__UNLOCK(pAccessor);
	}
}

/**
 * return Accessor handle on success, ra_x_IndexAccessorRa_ACCESSOR_UNDEFINED otherwise
 */
jint Accessor_ConnectToCursor(Handle cursorHandle, const char* columnName) {
	jint result = ra_x_IndexAccessorRa_ACCESSOR_UNDEFINED;
	if (NULL != columnName) {
		Cursor* pCursor = (Cursor*) getInPool(cursorHandle);
		if (NULL != pCursor) {
			__LOCK(pCursor);
			jint columnPosition = getStatementColumnIndexCS(pCursor->pStatement, columnName);
			if (-1 != columnPosition) {
				if (NULL != pCursor->pAccessor
				    && !__Accessor_CheckIsSame(pCursor->pAccessor, columnName, columnPosition)) {
					pCursor->pAccessor = Accessor_Free(pCursor->pAccessor);
				}
				if (NULL == pCursor->pAccessor) {
					pCursor->pAccessor = Accessor_Create(cursorHandle, columnName, columnPosition);

				}
				if (NULL != pCursor->pAccessor) {
					if (!__IsDefaultColumn(columnName)) {
						Accessor_RebuildIndex(cursorHandle);
					}
					result = pCursor->pAccessor->g_poolId;
				}
			}__UNLOCK(pCursor);
		}
	}
	return result;
}

jint Accessor_Locate(Accessor* pAccessor /* assume !NULL */, jlong indexValue) {
	jint result = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
	__LOCK(pAccessor);
	Cursor* pCursor = (Cursor*) getInPool(pAccessor->cursorHandle);
	if (NULL != pCursor) {
		if (indexValue == pAccessor->currentValue) {
			result = pAccessor->curentValueRSPosition;
		} else {
			jint firstBlock2Analyse;
			jint lastBlock2Analyse;
			// look for matching block range
			jint curentValueBlockPosition;
			// ensure pAccessor->pFLIndex[index] starts from 0
			if (ra_x_IndexAccessorRa_POSITION_UNDEFINED == pAccessor->curentValueBlockPosition) {
				curentValueBlockPosition = 0;
			} else {
				curentValueBlockPosition = pAccessor->curentValueBlockPosition;
			}
			if (indexValue > pAccessor->pFLIndex[curentValueBlockPosition].maxBlockIndexValue) {
				firstBlock2Analyse = curentValueBlockPosition + 1;
				lastBlock2Analyse = pAccessor->FLIndexSize;
			} else {
				firstBlock2Analyse = 0;
				lastBlock2Analyse = curentValueBlockPosition + 1;
			}
			// ensure  pAccessor->pFLIndex[index] ends with pAccessor->FLIndexSize
			if (lastBlock2Analyse > pAccessor->FLIndexSize) {
				lastBlock2Analyse = pAccessor->FLIndexSize;
			}
			// look for matching index block
			jint idx;
			jint matchingBlockPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
			for (idx = firstBlock2Analyse; idx < lastBlock2Analyse; ++idx) {
				if (indexValue <= pAccessor->pFLIndex[idx].maxBlockIndexValue) {
					matchingBlockPosition = idx;
					break;
				}
			}
			jint matchingRowPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
			jint currentRowPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
			if (ra_x_IndexAccessorRa_POSITION_UNDEFINED != matchingBlockPosition) {
				// block index found, search indexValue row position
				BlockIndex* pBlock = pAccessor->pFLIndex + matchingBlockPosition;
				jlong rowIndexValue;
				for (idx = pBlock->blockStartPosition; idx < (pBlock->blockStartPosition + pBlock->blockSize); ++idx) {
					if ((SQLITE_OK == Cursor_MoveToPosition(pCursor, idx, &currentRowPosition))
					    && (CURSOR_ERROR_CODE_OK
					        != Cursor_GetColumnInt64(pCursor, pAccessor->columnPosition, &rowIndexValue))) {
						break;
					} else {
						if (rowIndexValue == indexValue) {
							matchingRowPosition = idx;
							break;
						}
					}
				}
			}
			// job done handle results, in any way...
			pAccessor->currentValue = indexValue;
			pAccessor->curentValueBlockPosition = matchingBlockPosition;
			result = pAccessor->curentValueRSPosition = matchingRowPosition;
		}
	}__UNLOCK(pAccessor);
	return result;
}

void Accessor_AppendBlockIndex(Accessor* pAccessor, jint blockStartPosition, jint blockSize, jlong maxBlockIndexValue) {
	if (NULL != pAccessor) {
		__LOCK(pAccessor);
		__Accessor_AppendBlockIndex(pAccessor, blockStartPosition, blockSize, maxBlockIndexValue);
		__UNLOCK(pAccessor);
	}
}

static void Accessor_RebuildIndex(Handle cursorHandle) {
	Cursor* pCursor = (Cursor*) getInPool(cursorHandle);
	if (NULL != pCursor) {
		if (NULL != pCursor->pAccessor) {
			if (SQLITE_OK == cursor_InnerReset(pCursor)) {
				__Accessor_Deactivate(pCursor->pAccessor);
				jlong maxBlockIndexValue; // last is max by design
				jint skippedRows;
				jint blockStartPosition = 0;
				for (;;) {
					skippedRows = skip_N_read(pCursor->pStatement,
					                          pCursor->rowsPerExtent,
					                          pCursor->pAccessor->columnPosition,
					                          &maxBlockIndexValue);
					__Accessor_AppendBlockIndex(pCursor->pAccessor,
					                            blockStartPosition,
					                            skippedRows,
					                            maxBlockIndexValue);
					blockStartPosition += skippedRows;
					if (pCursor->rowsPerExtent > skippedRows) {
						break;
					}
				}
				sqlite3_reset(pCursor->pStatement);
			}
		}
	}
}

static jint __Accessor_CheckIsSame(Accessor* pAccessor /* assume !NULL */,
                                   const char* newColumnName,
                                   jint newColumnPosition) {
	return (newColumnPosition == pAccessor->columnPosition) && (0 == strcmp(newColumnName, pAccessor->columnName));
}

static jint __IsDefaultColumn(const char* newColumnName) {
	return (0 == strcmp(newColumnName, DEF_FAST_ACCESS_COLUMN_NAME));
}

static jint __Accessor_Resize(Accessor* pAccessor /* assume !NULL */) {
	jint result = 0;
	BlockIndex* pNewFLIndex = (BlockIndex*) realloc(pAccessor->pFLIndex,
	                                                (pAccessor->FLIndexCapacity + ACCESSOR_DEF_CAPACITY)
	                                                    * sizeof(BlockIndex));
	if (NULL != pNewFLIndex) {
		pAccessor->pFLIndex = pNewFLIndex;
		pAccessor->FLIndexCapacity += ACCESSOR_DEF_CAPACITY;
		result = 1;
	}
	return result;
}
static void __Accessor_Deactivate(Accessor* pAccessor) {
	pAccessor->FLIndexSize = 0;
	pAccessor->curentValueBlockPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
	pAccessor->curentValueRSPosition = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
	pAccessor->currentValue = INT64_MIN;
}

static void __Accessor_AppendBlockIndex(Accessor* pAccessor/* assume !NULL */,
                                        jint blockStartPosition,
                                        jint blockSize,
                                        jlong maxBlockIndexValue) {
	if (pAccessor->FLIndexSize == pAccessor->FLIndexCapacity) {
		if (!__Accessor_Resize(pAccessor)) {
			__LE("Can't resize index block");
			return;
		}
	}
	pAccessor->pFLIndex[pAccessor->FLIndexSize].blockStartPosition = blockStartPosition;
	pAccessor->pFLIndex[pAccessor->FLIndexSize].blockSize = blockSize;
	pAccessor->pFLIndex[pAccessor->FLIndexSize].maxBlockIndexValue = maxBlockIndexValue;
	pAccessor->FLIndexSize++;
}

static jint skip_N_read(sqlite3_stmt* pStatement, jint skipCount, jint columnIndex2Read, jlong* lastReadColumnValue) {
	jint readCount = 0;
	while (readCount < skipCount) {
		if (SQLITE_ROW == sqlite3_step(pStatement)) {
			*lastReadColumnValue = sqlite3_column_int64(pStatement, columnIndex2Read);
			readCount++;
		} else {
			break;
		}
	}
	return readCount;
}
