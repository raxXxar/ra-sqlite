package ra.db.ra;

import ra.dbengine.ISQLDatabase;
import ra.sqlite.SQLiteDatabase;
import android.util.Log;

public class RaSQLiteDatabase extends SQLiteDatabase implements ISQLDatabase {

	public static RaSQLiteDatabase openDatabase(String path) {
		return openDatabase(path, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE);
	}

	public static RaSQLiteDatabase openDatabase(String path, int flags) {
		RaSQLiteDatabase sqliteDatabase = null;
		try {
			// Open the database.
			sqliteDatabase = new RaSQLiteDatabase(path, flags);
		} catch (Exception e) {
			Log.e(TAG, "database " + path, e);
		}
		return sqliteDatabase;
	}

	protected RaSQLiteDatabase(String path, int flags) {
		super(path, flags);
	}

	private static final String TAG = RaSQLiteDatabase.class.getSimpleName();
}
