#include <jni.h>
#include <malloc.h>
#include <stddef.h>
#include "ra_sqlite.h"
#include "ra_list.h"

List* List_Create(FnListRecordDestructor listRecordDestructor) {
	List* pList = (List*) malloc(sizeof(List));
	pList->head = NULL;
	pList->recordDestructor = listRecordDestructor;
	__INIT(pList);
	return pList;
}

void List_Free(List* pList) {
	__LOCK(pList);
	Link* pLink;
	while (pList->head) {
		pLink = pList->head;
		pList->head = pList->head->next;
		if (NULL != pList->recordDestructor) {
			pList->recordDestructor(pLink->record);
		}
		free(pLink);
	}
	__UNLOCK(pList);
	free(pList);
}

void List_Push(List* pList, ListRecord record) {
	__LOCK(pList);
	Link* pLink = malloc(sizeof(Link));
	pLink->next = pList->head;
	pLink->record = record;
	pList->head = pLink;
	__UNLOCK(pList);
}

ListRecord List_Pop(List* pList) {
	__LOCK(pList);
	ListRecord record = NULL;
	if (NULL != pList->head) {
		Link* pLink = pList->head;
		pList->head = pLink->next;
		record = pLink->record;
		free(pLink);
	}
	__UNLOCK(pList);
	return record;
}
