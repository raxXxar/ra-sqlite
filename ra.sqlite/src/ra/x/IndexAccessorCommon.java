package ra.x;

import android.database.Cursor;

public class IndexAccessorCommon extends IndexAccessor {

	public static final String DEF_FAST_ACCESS_COLUMN_NAME = "pk$join$column";

	 IndexAccessorCommon(Cursor cursor, String fastAccessColumnName) {
		super(cursor, fastAccessColumnName);
	}

	protected IndexAccessorCommon(Cursor cursor) {
		super(cursor);
	}

	@Override
	public int locate(long indexValue) {
		synchronized (syncObject) {
			int position = POSITION_UNDEFINED;
			if (isValid()) {
				for (int i = 0; i < firstLevelIndex.length; ++i) {
					if (indexValue <= firstLevelIndex[i]) {
						position = i;
						break;
					}
				}
				if (POSITION_UNDEFINED != position /* block found */) {
					int localIdxStartPos = position * ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE;
					int localIdxEndPos = Math.min((position + 1 * ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE), cursor.getCount());
					for (int i = localIdxStartPos; i < localIdxEndPos; i++) {
						if (cursor.moveToPosition(i)) {
							if (indexValue == cursor.getLong(fastAccessColumnIdx)) {
								break;
							}
						}
					}
				}
			}
			return position;
		}
	}

	@Override
	protected void deactivateInner() {
		fastAccessColumnIdx = POSITION_UNDEFINED;
		firstLevelIndex = null;
	}

	@Override
	protected boolean initInner() {
		int rowCount = cursor.getCount();
		// get fastAccessColumnName column index
		fastAccessColumnIdx = cursor.getColumnIndex(fastAccessColumnName);
		if (POSITION_UNDEFINED == fastAccessColumnIdx) {
			throw new IllegalArgumentException("There is no column named \"" + fastAccessColumnName + "\" in cursor");
		}
		// calculate firstLevelIndex size
		int firstLevelIndexSize = rowCount / ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE + ((0 == rowCount % ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE) ? 0 : 1);
		// init firstLevelIndex
		firstLevelIndex = new long[firstLevelIndexSize];
		// cache index data
		for (int i = 0; i < firstLevelIndexSize - 1; ++i) {
			cursor.moveToPosition(i * ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE);
			firstLevelIndex[i] = cursor.getLong(fastAccessColumnIdx);
		}
		cursor.moveToLast();
		firstLevelIndex[firstLevelIndex.length - 1] = cursor.getLong(fastAccessColumnIdx);
		return (null != firstLevelIndex);
	}

	private int fastAccessColumnIdx = POSITION_UNDEFINED;
	private long[] firstLevelIndex = null;

	private static final int ANDROID_SQLITE_DESIRED_TABLE_BLOCK_SIZE = 100;
}
