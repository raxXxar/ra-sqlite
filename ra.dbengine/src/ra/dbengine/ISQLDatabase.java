package ra.dbengine;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteTransactionListener;

public interface ISQLDatabase {

	public void beginTransaction();

	public void beginTransactionWithListener(SQLiteTransactionListener listener);

	public void endTransaction();

	public void setTransactionSuccessful();

	public boolean inTransaction();

	public void setLocale(String localeStr);

	public void close();

	public int getVersion();

	public void setVersion(int version);

	public Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having,
			String orderBy, String limit);

	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy);

	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit);

	public Cursor rawQuery(String sql, String[] selectionArgs);

	public long insert(String table, String nullColumnHack, ContentValues values);

	public long replace(String table, String nullColumnHack, ContentValues initialValues);

	public long replaceOrThrow(String table, String nullColumnHack, ContentValues initialValues) throws SQLException;

	public int update(String table, ContentValues values, String whereClause, String[] whereArgs);

	public int delete(String table, String whereClause, String[] whereArgs);

	public int execSQL(String sql);

	public int execSQL(String sql, Object[] bindArgs);

	public boolean isReadOnly();

	public boolean isOpen();

	public boolean needUpgrade(int newVersion);

}
