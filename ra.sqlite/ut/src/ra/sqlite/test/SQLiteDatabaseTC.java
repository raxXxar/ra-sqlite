package ra.sqlite.test;

import java.io.File;
import java.nio.ByteBuffer;
import ra.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.os.Environment;
import android.test.AndroidTestCase;

public class SQLiteDatabaseTC extends AndroidTestCase {

	public SQLiteDatabaseTC() {
		super();
	}

	protected void setUp() throws Exception {
		super.setUp();
		db = SQLiteDatabase.openDatabase(DB_PATH_NAME);
		assertTrue(null != db);
	}

	protected void tearDown() throws Exception {
		db.close();
		db = null;
		getContext().deleteDatabase(DB_PATH_NAME);
		super.tearDown();
	}

	public void testSQLiteDatabase() {
		File file = new File(DB_PATH, DB_NAME);
		assertTrue(file.exists());
	}

	public void testTransactions() {
		assertEquals(false, db.inTransaction());
		long rowsCountStart = TestHelper.setupTable(db, 10);
		// test rollback
		db.beginTransaction();
		assertEquals(true, db.inTransaction());
		db.execSQL("INSERT INTO test(i) VALUES(1)");
		db.execSQL("INSERT INTO test(i) VALUES(2)");
		db.execSQL("INSERT INTO test(i) VALUES(3)");
		db.execSQL("INSERT INTO test(i) VALUES(4)");
		long rowCountAfterInsert = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(rowsCountStart + 4, rowCountAfterInsert);
		db.endTransaction(); // cause rollback
		assertEquals(false, db.inTransaction());
		long rowCountAfterRollback = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(rowsCountStart, rowCountAfterRollback);

		// test commit
		db.beginTransaction();
		assertEquals(true, db.inTransaction());
		db.execSQL("INSERT INTO test(i) VALUES(1)");
		rowCountAfterInsert = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(rowsCountStart + 1, rowCountAfterInsert);
		db.setTransactionSuccessful();
		db.endTransaction(); // cause commit
		assertEquals(false, db.inTransaction());
		long rowCountAfterCommit = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(rowsCountStart + 1, rowCountAfterCommit);
	}

	public void testTransactionsWithListener() {

		TestTransactionListener listener = new TestTransactionListener();

		// test rollback
		listener.reset();
		assertEquals(false, db.inTransaction());
		db.beginTransactionWithListener(listener);
		assertEquals(true, db.inTransaction());
		db.endTransaction();
		assertEquals(false, db.inTransaction());
		assertEquals(true, listener.wasBegin);
		assertEquals(true, listener.wasRollback);
		assertEquals(false, listener.wasCommit);

		// test commit
		listener.reset();
		assertEquals(false, db.inTransaction());
		db.beginTransactionWithListener(listener);
		assertEquals(true, db.inTransaction());
		db.setTransactionSuccessful();
		db.endTransaction();
		assertEquals(false, db.inTransaction());
		assertEquals(true, listener.wasBegin);
		assertEquals(false, listener.wasRollback);
		assertEquals(true, listener.wasCommit);
	}

	public void testClose() {
		assertEquals(true, db.isOpen());
		db.close();
		assertEquals(false, db.isOpen());
	}

	public void testGetSetVersion() {
		db.setVersion(10);
		int ver = db.getVersion();
		assertEquals(10, ver);
	}

	public void testExecSQLString() {
		long initialCount = TestHelper.setupTable(db, 0);
		db.execSQL("INSERT INTO test(i) VALUES(1)");
		long rowCount = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(initialCount + 1, rowCount);
	}

	public void testExecSQLStringObjectArray() {
		TestHelper.setupTable(db, 0);
		Integer vInt = Integer.valueOf(200);
		Double vDbl = Double.valueOf(TestHelper.DBL_FRACTION);
		String vStr = new String("TEXT " + String.valueOf(vInt));
		ByteBuffer vBuf = TestHelper.str2buf(vStr);
		Object args[] = new Object[] { vInt, vDbl, vStr, vBuf };
		db.execSQL("INSERT INTO test(i,f,t,b) VALUES (?, ?, ?, ?)", args);
		args = new Object[] { vInt };
		long rowCount = db.simpleQueryForLong("SELECT count(*) FROM test WHERE i = ?", args);
		assertEquals(1, rowCount);
	}

	public void testIsOpen() {
		assertEquals(true, db.isOpen());
		db.close();
		assertEquals(false, db.isOpen());
	}

	public void testIsReadOnly() {
		assertEquals(false, db.isReadOnly());
	}

	public void testNeedUpgrade() {
		int version = 10;
		db.setVersion(version);
		assertEquals(false, db.needUpgrade(version));
		assertEquals(true, db.needUpgrade(version + 1));
	}

	public void testSimpleQueryForLongString() {
		long initialRowCount = TestHelper.setupTable(db, 2);
		long rowCount = db.simpleQueryForLong("SELECT count(*) FROM test");
		assertEquals(initialRowCount, rowCount);
	}

	public void testSimpleQueryForLongStringObjectArray() {
		TestHelper.setupTable(db, 1);
		Integer vInt = Integer.valueOf(0);
		Double vDbl = Double.valueOf(TestHelper.DBL_FRACTION + vInt);
		String vStr = new String("TEXT " + String.valueOf(vInt));
		ByteBuffer vBuf = TestHelper.str2buf(vStr);
		Object args[] = new Object[] { vInt, vDbl, vStr, vBuf };
		db.execSQL("INSERT INTO test(i,f,t,b) VALUES (?, ?, ?, ?)", args);
		long rowCount = db.simpleQueryForLong("SELECT count(*) FROM test " + "WHERE i = ? AND f = ? AND t = ? AND b = ?", args);
		assertEquals(2, rowCount);
	}

	public void testSimpleQueryForStringString() {
		TestHelper.setupTable(db, 0);
		String str = new String("XXX");
		Object args[] = new Object[] { str };
		db.execSQL("INSERT INTO test(t) VALUES (?)", args);
		String result = db.simpleQueryForString("SELECT t FROM test WHERE t = '" + str + "' LIMIT 1");
		assertEquals(true, str.equals(result));
		result = db.simpleQueryForString("SELECT t FROM test WHERE t = '" + str + "' LIMIT 0");
		assertEquals(false, str.equals(result));
	}

	public void testSimpleQueryForStringStringObjectArray() {
		TestHelper.setupTable(db, 0);
		String str = new String("XXX");
		Object args[] = new Object[] { str };
		db.execSQL("INSERT INTO test(t) VALUES (?)", args);
		String result = db.simpleQueryForString("SELECT t FROM test WHERE t = ? LIMIT 1", args);
		assertEquals(true, str.equals(result));
		result = db.simpleQueryForString("SELECT t FROM test WHERE t = ? LIMIT 0", args);
		assertEquals(false, str.equals(result));
	}

	public void testBindStatement() {
		TestHelper.setupTable(db, 1);
		Integer vInt = Integer.valueOf(0);
		Double vDbl = Double.valueOf(TestHelper.DBL_FRACTION + vInt);
		String vStr = new String("TEXT " + String.valueOf(vInt));
		ByteBuffer vBuf = TestHelper.str2buf(vStr);
		Object args[] = new Object[] { vInt, vDbl, vStr, vBuf };
		db.execSQL("INSERT INTO test(i,f,t,b) VALUES (?, ?, ?, ?)", args);
		long rowCount = db.simpleQueryForLong("SELECT count(*) FROM test " + "WHERE i = ? AND f = ? AND t = ? AND b = ?", args);
		assertEquals(2, rowCount);
		byte[] byteBuffer = new byte[] { 20, 20, 20, 20 };
		Object args2[] = new Object[] { vInt, vDbl, vStr, byteBuffer };
		db.execSQL("INSERT INTO test(i,f,t,b) VALUES (?, ?, ?, ?)", args2);
		rowCount = db.simpleQueryForLong("SELECT count(*) FROM test " + "WHERE i = ? AND f = ? AND t = ? AND b = ?", args2);
		assertEquals(1, rowCount);
	}

	public void testRawQuery() {
		int count = (int) TestHelper.setupTable(db, 10);
		Cursor cursor = null;
		int rowCount = -1;
		try {
			cursor = db.rawQuery("SELECT * FROM test");
			rowCount = cursor.getCount();
		} finally {
			if (null != cursor && !cursor.isClosed()) {
				cursor.close();
			}
		}
		assertEquals(count, rowCount);
	}

	public void testRawQueryBind() {
		int count = (int) TestHelper.setupTable(db, 20);
		Cursor cursor = null;
		int rowCount = -1;
		try {
			cursor = db.rawQueryBind("SELECT * FROM test WHERE i > ?", new Integer[] { Integer.valueOf(-20) });
			rowCount = cursor.getCount();
		} finally {
			if (null != cursor && !cursor.isClosed()) {
				cursor.close();
			}
		}
		assertEquals(count, rowCount);
	}

	private SQLiteDatabase db = null;
	private static final String DB_NAME = "test_fake.db";
	private static final String DB_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private static final String DB_PATH_NAME = DB_PATH + "/" + DB_NAME;
}
