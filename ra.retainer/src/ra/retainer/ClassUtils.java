package ra.retainer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>Operates on classes with help of reflection.</p>
 *
 * This class handles invalid <code>null</code> inputs as best it can.
 * Each method documents its behaviour in more detail.
 *
 */
public abstract class ClassUtils {

	public static interface FieldAcceptCriteria {

		/**
		 * <p>Check if field match criteria</p> 
		 *
		 * @param field field is being tested
		 * @return match criteria result
		 */
		public boolean match(Field field);
	}

	public static interface MethodAcceptCriteria {

		/**
		 * <p>Check if method match criteria</p>
		 *
		 * @param method method is being tested
		 * @return match criteria result
		 */
		public boolean match(Method method);
	}

	/**
	 *  <p>Retrieving target class methods list</p> 
	 *
	 * @param targetClass class which methods are searching
	 * @param recursively If true, retrieving methods from all target class hierarchy
	 * @param criteria If null all methods accepted, if match otherwise   
	 * @return list of methods
	 */
	public static Method[] getDeclaredMethods(Class<?> targetClass, boolean recursively, MethodAcceptCriteria criteria) {
		List<Method> methods = new LinkedList<Method>();
		Method[] declaredMethods = targetClass.getDeclaredMethods();
		if (null == criteria) {
			Collections.addAll(methods, declaredMethods);
		} else {
			for (Method method : declaredMethods) {
				if (criteria.match(method)) {
					methods.add(method);
				}
			}
		}
		if (recursively) {
			Class<?> superClass = targetClass.getSuperclass();
			if (superClass != null) {
				Method[] declaredMethodsOfSuper = getDeclaredMethods(superClass, recursively, criteria);
				if (declaredMethodsOfSuper.length > 0)
					Collections.addAll(methods, declaredMethodsOfSuper);
			}
		}
		return methods.toArray(new Method[methods.size()]);
	}

	/**
	 * <p>Retrieving target class methods list</p>
	 *
	 * @param targetClass class which methods are searching
	 * @param recursively If true, retrieving methods from all target class hierarchy
	 * @return list of methods
	 */
	public static Method[] getDeclaredMethods(Class<?> targetClass, boolean recursively) {
		return getDeclaredMethods(targetClass, recursively, null);
	}

	/**
	 * <p>Retrieving target class fields list</p>
	 *
	 * @param targetClass class which fields are searching
	 * @param recursively If true, retrieving fields from all target class hierarchy
	 * @param criteria If null all fields accepted, if match otherwise   
	 * @return list of fields
	 */
	public static Field[] getDeclaredFields(Class<?> targetClass, boolean recursively, FieldAcceptCriteria criteria) {
		List<Field> fields = new LinkedList<Field>();
		Field[] declaredFields = targetClass.getDeclaredFields();
		if (null == criteria) {
			Collections.addAll(fields, declaredFields);
		} else {
			for (Field field : declaredFields) {
				if (criteria.match(field)) {
					fields.add(field);
				}
			}
		}
		if (recursively) {
			Class<?> superClass = targetClass.getSuperclass();
			if (superClass != null) {
				Field[] declaredFieldsOfSuper = getDeclaredFields(superClass, recursively, criteria);
				if (declaredFieldsOfSuper.length > 0)
					Collections.addAll(fields, declaredFieldsOfSuper);
			}
		}
		return fields.toArray(new Field[fields.size()]);
	}

	/**
	 * <p>Retrieving target class fields list</p>
	 *
	 * @param targetClass class which fields are searching
	 * @param recursively If true, retrieving fields from all target class hierarchy
	 * @return list of fields
	 */
	public static Field[] getDeclaredFields(Class<?> targetClass, boolean recursively) {
		return getDeclaredFields(targetClass, recursively, null);
	}

	/**
	 * <p>Retrieving target class list of fields annotated by specified annotation class
	 * If recursively is true, retrieving fields from all class hierarchy
	 * Only annotated fields are tested to match criteria</p> 
	 *
	 * @param targetClass - where fields are searching
	 * @param annotationClass specified annotation class
	 * @param recursively If true, retrieving fields from all target class hierarchy
	 * @param criteria If null all fields accepted, if match otherwise   
	 * @return list of annotated fields
	 */
	public static Field[] getAnnotatedDeclaredFields(Class<?> targetClass, Class<? extends Annotation> annotationClass, boolean recursively,
			FieldAcceptCriteria criteria) {
		List<Field> annotatedFields = new LinkedList<Field>();
		for (Field field : getDeclaredFields(targetClass, recursively, null)) {
			if (field.isAnnotationPresent(annotationClass)) {
				if (null == criteria || criteria.match(field))
					annotatedFields.add(field);
			}
		}
		return annotatedFields.toArray(new Field[annotatedFields.size()]);
	}

	/**
	 * <p>Retrieving target class list of fields annotated by specified annotation class
	 * If recursively is true, retrieving fields from all class hierarchy</p>
	 *
	 * @param targetClass - where fields are searching
	 * @param annotationClass specified annotation class
	 * @param recursively If true, retrieving fields from all target class hierarchy
	 * @return list of annotated fields
	 */
	public static Field[] getAnnotatedDeclaredFields(Class<?> targetClass, Class<? extends Annotation> annotationClass, boolean recursively) {
		return getAnnotatedDeclaredFields(targetClass, annotationClass, recursively, null);
	}

	/**
	 * <p>Converts the specified wrapper class to its corresponding primitive class.
	 * This method is the counter part of primitiveToWrapper(). 
	 * If the passed in class is a wrapper class for a primitive type, 
	 * this primitive type will be returned (e.g. Integer.TYPE for Integer.class). 
	 * For other classes, or if the parameter is null, the return value is null.</p>
	 * 
	 * @param cls the class to convert, may be null
	 * @return the corresponding primitive type if cls is a wrapper class, null otherwise
	 */
	public static Class<?> wrapperToPrimitive(Class<?> cls) {
		Class<?> result = null;
		if (null != cls) {
			for (Class<?>[] pair : sWrapper2PrimitiveArr) {
				if (pair[0].equals(cls)) {
					result = pair[1];
					break;
				}
			}
		}
		return result;
	}

	/**
	 * <p>Converts the specified primitive Class object to its corresponding wrapper Class object.</p>
	 * 
	 * @param cls the class to convert, may be null
	 * @return the wrapper class for cls or cls if cls is not a primitive. null if null input.
	 */
	public static Class<?> primitiveToWrapper(Class<?> cls) {
		Class<?> result = null;
		if (null != cls) {
			result = cls;
			for (Class<?>[] pair : sWrapper2PrimitiveArr) {
				if (pair[1].equals(cls)) {
					result = pair[0];
					break;
				}
			}
		}
		return result;
	}

	public static void changeObjectValue(Object targetObject, Field field, Object newValue) {
		boolean inAccesible = !field.isAccessible();
		if (inAccesible)
			field.setAccessible(true);
		try {
			field.set(targetObject, newValue);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} finally {
			if (inAccesible)
				field.setAccessible(false);
		}
	}

	public static void changeObjectValue(Object targetObject, String fieldName, Object newValue) {
		try {
			Field targetField = targetObject.getClass().getDeclaredField(fieldName);
			changeObjectValue(targetObject, targetField, newValue);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	private static Class<?>[][] sWrapper2PrimitiveArr = new Class<?>[][] { { Boolean.class, Boolean.TYPE }, { Byte.class, Byte.TYPE },
			{ Character.class, Character.TYPE }, { Short.class, Short.TYPE }, { Integer.class, Integer.TYPE }, { Long.class, Long.TYPE },
			{ Double.class, Double.TYPE }, { Float.class, Float.TYPE }, { Void.class, Void.TYPE } };

}
