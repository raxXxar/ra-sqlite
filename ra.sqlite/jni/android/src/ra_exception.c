#include <jni.h>
#include <android/log.h>
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_exception.h"

void throw_sqlite3_exception(JNIEnv* jniEnv, sqlite3* handle, jint errcode) {
	if (SQLITE_OK == errcode) {
		errcode = sqlite3_errcode(handle);
	}
	const char* errmsg = sqlite3_errmsg(handle);
#ifdef LOG_MSG
	__LE("E_MSG[%d : %s]", errcode, errmsg);
#endif
	throw_sqlite3_exception_msg(jniEnv, errmsg);
}

void throw_sqlite3_exception_msg(JNIEnv* jniEnv, const char* msg) {
#ifdef LOG_MSG
	__LE("E_MSG[%s]", msg);
#endif
	(*jniEnv)->ThrowNew(jniEnv, jClassSQLException, msg);
}

void throw_no_such_cursor_msg(JNIEnv* jniEnv, const char* msg) {
#ifdef LOG_MSG
	__LE("E_MSG[%s]", msg);
#endif
	(*jniEnv)->ThrowNew(jniEnv, jClassNoSuchCursorException, msg);

}

void throw_no_such_db_msg(JNIEnv* jniEnv, const char* msg) {
#ifdef LOG_MSG
	__LE("E_MSG[%s]", msg);
#endif
	(*jniEnv)->ThrowNew(jniEnv, jClassNoSuchDatabaseException, msg);

}
