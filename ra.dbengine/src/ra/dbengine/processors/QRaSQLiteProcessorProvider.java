package ra.dbengine.processors;

import ra.dbengine.QProcessor;
import ra.dbengine.interfaces.QProcessorProvider;

public class QRaSQLiteProcessorProvider implements QProcessorProvider {

	@Override
	public QProcessor get() {
		return new QRaSQLiteProcessorImpl();
	}
}
