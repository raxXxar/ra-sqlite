package ra.dbengine;

import ra.db.DbConfig;
import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import ra.dbengine.interfaces.DbProvider;

public abstract class ISQLDbProvider implements DbProvider<ISQLDatabase> {

	@Override
	public void setConfig(DbConfig config) {
		this.config = config;
	}

	@Override
	public DbConfig getConfig() {
		return config;
	}

	@Override
	public ISQLDatabase get() throws DbIncorrectVersionException, DbOpenErrorException {
		return get(config.getDbName());
	}

	@Override
	public ISQLDatabase get(String databasePath) throws DbIncorrectVersionException, DbOpenErrorException {
		ISQLDatabase db = null;
		if (null != databasePath) {
			db = open(databasePath);
			if (null != db && db.isOpen()) {
				config.prepareDb(db);
				config.setLocale(db);
				// check version
				if (!isDbVersionOk(db, config.getDbVersion())) {
					db.close();
					db = null;
					throw new DbIncorrectVersionException();
				}
			} else {
				throw new DbOpenErrorException();
			}
		}
		return db;
	}

	@Override
	public boolean isDbVersionOk(ISQLDatabase db, int dbExpectedSchemaVersion) {
		boolean result = false;
		int dbSchemaVersion = db.getVersion();
		if (dbExpectedSchemaVersion == dbSchemaVersion) {
			result = true;
		}
		return result;
	}

	protected abstract ISQLDatabase open(String databasePath);

	protected DbConfig config = null;
}
