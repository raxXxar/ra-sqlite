package ra.dbengine;

import ra.dbengine.interfaces.SqlCommandSource;

/**
 * The Class SqlCommand - most common implementation of {@link SqlCommandSource} widely used to access data.
 */
public class SqlCommand implements SqlCommandSource {

	/**
	 * Disable instantiation without arguments.
	 */
	protected SqlCommand() {
		mNotificationTags = new String[] {};
	}

	/**
	 * Instantiates a new SqlCommand with SQL statement (DDL/DML/Query).
	 *
	 * @param sqlCmd the SQL statement
	 */
	public SqlCommand(String sqlCmd) {
		this(sqlCmd, null, null);
	}

	/**
	 * Instantiates a new SqlCommand with DDL/DML statements block.
	 *
	 * @param sqlCommandBlock the DDL/DML statements String array
	 */
	public SqlCommand(String[] sqlCommandBlock) {
		this(null, null, sqlCommandBlock);
	}

	/**
	 * Instantiates a new SqlCommand with SQL statement (DDL+DML/Query) and array of modifying/listened tags.
	 *
	 * @param sqlCmd the SQL statement
	 * @param notificationTags the notification tags
	 */
	public SqlCommand(String sqlCmd, String[] notificationTags) {
		this(sqlCmd, notificationTags, null);
	}

	/**
	 * Instantiates a new SqlCommand with SQL statement (DDL+DML) block and array of modifying tags.
	 *
	 * @param sqlCommandBlock the SQL statement block
	 * @param notificationTags the notification tags
	 */
	public SqlCommand(String[] sqlCommandBlock, String[] notificationTags) {
		this(null, notificationTags, sqlCommandBlock);
	}

	/**
	 * Instantiates a new SqlCommand - <i>strange constructor</i>
	 *
	 * @param sqlCmd the SqlCommand with SQL statement
	 * @param notificationTags the notification tags
	 * @param the SQL statement block
	 */
	protected SqlCommand(String sqlCmd, String[] notificationTags, String[] sqlCommandBlock) {
		mSqlCmd = sqlCmd;
		if (null != notificationTags) {
			mNotificationTags = notificationTags;
		} else {
			mNotificationTags = new String[] {};
		}
		mSqlCommandBlock = sqlCommandBlock;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.SqlCommandSource#getSqlCommand()
	 */
	@Override
	public String getSqlCommand() {
		return mSqlCmd;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.SqlCommandSource#getSqlCommandBlock()
	 */
	@Override
	public String[] getSqlCommandBlock() {
		return mSqlCommandBlock;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.SqlCommandSource#getNotificationTags()
	 */
	@Override
	public String[] getNotificationTags() {
		return mNotificationTags;
	}

	protected String mSqlCmd;

	/** The Notification tags. 
	 * <br><b>shouldn't be null</b> 
	 */
	protected String[] mNotificationTags;

	protected String[] mSqlCommandBlock;
}
