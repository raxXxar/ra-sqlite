/*
 * ra_collator.c
 *
 *      Author: =ra=
 */

#include <jni.h>
#include <malloc.h>
#include <string.h>
#include "unicode/ucol.h"
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_exception.h"
#include "ra_sqlite_SQLiteDatabase_.h"

#define LOCALIZED "LOCALIZED"

typedef int pFCollate(void*, int, const void*, int, const void*);

static int icu_string_from_utf8(UChar **ustr, int32_t *ulen, const char *str);

static int icuCollationColl_U16(void *pCtx,
                                int nLeft,
                                const void* zLeft,
                                int nRight,
                                const void* zRight);
static int icuCollationColl_U8(void *pCtx,
                               int nLeft,
                               const void* zLeft,
                               int nRight,
                               const void* zRight);

void setDefaultCollation(JNIEnv* jniEnv,
                         Dbh* pDbh,
                         const char* zLocale /* Locale identifier - (e.g. "uk_UA") */) {
	if (pDbh && zLocale) {
		// 1. create collator
		UErrorCode status = U_ZERO_ERROR;
		UCollator* pUCollator /* ICU library collation object */=
		    ucol_open(zLocale, &status);
		if (!U_SUCCESS(status)) {
			THROW_SQL3_MSG_FORMAT(jniEnv,
			                      "ICU error: ucol_open(): %s", u_errorName(status));
			return;
		}
		if (SQLITE_OK
		    == sqlite3_create_collation(pDbh->pSqliteDb,
		                                LOCALIZED,
		                                SQLITE_UTF16,
		                                (void*) pUCollator,
		                                icuCollationColl_U16)) {
			if (SQLITE_OK
			    == sqlite3_create_collation(pDbh->pSqliteDb,
			                                LOCALIZED,
			                                SQLITE_UTF8,
			                                (void*) pUCollator,
			                                icuCollationColl_U8)) {
				if (NULL != pDbh->pDefCollator) {
					ucol_close(pDbh->pDefCollator);
				}
				pDbh->pDefCollator = pUCollator;
			} else {
				if (NULL == pDbh->pDefCollator) {
					pDbh->pDefCollator = pUCollator;
				}
				THROW_SQL3_MSG_FORMAT(jniEnv,
				                      "Error registering collation function "
				                      " (SQLITE_UTF8)");
			}
		} else {
			ucol_close(pUCollator);
			THROW_SQL3_MSG_FORMAT(jniEnv,
			                      "Error registering collation function "
			                      "(SQLITE_UTF16)");
		}
	}
}

/*
 ** Collation sequence comparison function. The pCtx argument points to
 ** a UCollator structure previously allocated using ucol_open().
 */
static int icuCollationColl_U16(void* pCtx,
                                int nLeft,
                                const void* zLeft,
                                int nRight,
                                const void* zRight) {
	int result = 0;
	UCollationResult res = ucol_strcoll((UCollator*) pCtx,
	                                    zLeft,
	                                    nLeft / 2,
	                                    zRight,
	                                    nRight / 2);
	switch (res) {
		case UCOL_LESS:
			result = -1;
			break;
		case UCOL_GREATER:
			result = +1;
			break;
		case UCOL_EQUAL:
			break;
		default:
			__LE("icuCollationColl_U16(ucol_strcoll strange result: %d)", res);
			break;
	}
	return result;
}

/*
 ** Collation sequence comparison function. The pCtx argument points to
 ** a UCollator structure previously allocated using ucol_open().
 */
static int icuCollationColl_U8(void* pCtx,
                               int nLeft,
                               const void* zLeft,
                               int nRight,
                               const void* zRight) {
	int result = 0;
	char* csLeft = malloc(sizeof(char) * (nLeft + 1));
	memcpy(csLeft, zLeft, nLeft);
	csLeft[nLeft] = 0;
	char* csRight = malloc(sizeof(char) * (nRight + 1));
	memcpy(csRight, zRight, nRight);
	csRight[nRight] = 0;
	UChar* u16Left = NULL;
	UChar* u16Right = NULL;
	int32_t lenLeft = 0;
	int32_t lenRight = 0;
	if (icu_string_from_utf8(&u16Left, &lenLeft, csLeft)
	    && icu_string_from_utf8(&u16Right, &lenRight, csRight)) {
		UCollationResult res = ucol_strcoll((UCollator*) pCtx,
		                                    u16Left,
		                                    lenLeft,
		                                    u16Right,
		                                    lenRight);
		switch (res) {
			case UCOL_LESS:
				result = -1;
				break;
			case UCOL_GREATER:
				result = +1;
				break;
			case UCOL_EQUAL:
				break;
			default:
				__LE("icuCollationColl_U8(ucol_strcoll "
				"strange result: %d)", res);
				break;
		}
		free(u16Left);
		free(u16Right);
	}
	free(csLeft);
	free(csRight);
	return result;
}

static int icu_string_from_utf8(UChar** ustr, int32_t *ulen, const char *str) {
	UErrorCode err;
	int32_t uconv_len;
	UChar* p = NULL;
	*ustr = NULL;
	err = U_ZERO_ERROR;
	u_strFromUTF8(NULL, 0, &uconv_len, str, -1, &err);
	if (U_FAILURE(err) && (err != U_BUFFER_OVERFLOW_ERROR)) {
		__LE("icu_convert_string_from_utf8(u_strFromUTF8, "
		"%s): %s\n", str, u_errorName(err));
		return 0;
	}
	p = malloc(sizeof(UChar) * (uconv_len + 1));
	err = U_ZERO_ERROR;
	u_strFromUTF8(p, uconv_len, ulen, str, -1, &err);
	if (U_FAILURE(err)) {
		__LE("icu_convert_string_from_utf8(u_strFromUTF8, "
		"%s): %s\n", str, u_errorName(err));
		free(p);
		p = NULL;
		return 0;
	}
	if (p) {
		p[uconv_len] = 0;
		*ustr = p;
		return 1;
	} else
		return 0;
}
