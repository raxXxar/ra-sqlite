package ra.dbengine;

import ra.dbengine.exceptions.DbIncorrectVersionException;
import ra.dbengine.exceptions.DbOpenErrorException;
import ra.dbengine.interfaces.DbProvider;
import android.database.Cursor;
import android.database.sqlite.SQLiteTransactionListener;

/**
 * Abstract class <b>QProcessor</b> designed to separate and encapsulate data modification and requesting logic.
 */
/**
 *  @author =ra=
 */
public abstract class QProcessor {

	/**
	 * Executes DDL/DML statement.
	 *
	 * @param cmd the DDL/DML statement
	 */
	public abstract void exec(String cmd);

	/**
	 * Executes DDL/DML statement.
	 *
	 * @param cmd the DDL/DML statement
	 */
	public abstract void exec(String cmd, Object[] bindArgs);

	/**
	 * Executes DDL/DML statement block.
	 *
	 * @param cmds the DDL/DML statement array
	 */
	public abstract void execBlock(String[] cmds);

	/**
	 * Returns true if the QProcessor has a transaction pending.
	 *
	 * @return True if the QProcessor is in a transaction.
	 */
	public abstract boolean inTransaction();

	/**
	 * Begin transaction. See {@link #endTransaction()}, {@link #setTransactionSuccessful()}, {@link #inTransaction()}
	 */
	public abstract void beginTransaction();

	/**
	 * End transaction. See {@link #beginTransaction()}, {@link #setTransactionSuccessful()}, {@link #inTransaction()}
	 */
	public abstract void endTransaction();

	/**
	 * Marks the current transaction as successful. See {@link #endTransaction()}
	 */
	public abstract void setTransactionSuccessful();

	/**
	 * Begins a transaction in <b>EXCLUSIVE</b> mode.
	 * 
	 * <br>Transactions can be nested. 
	 * <br>When the outer transaction is ended all of the work done in that transaction and all of the nested transactions will be committed or rolled back. 
	 * The changes will be rolled back if any transaction is ended without being marked as clean (by calling setTransactionSuccessful). 
	 * Otherwise they will be committed.
	 *
	 * @param listener the listener
	 */
	public abstract void beginTransactionWithListener(SQLiteTransactionListener listener);

	/**
	 * Fetch data into cursor.
	 *
	 * @param cmd the query statement
	 * @return the cursor
	 */
	protected abstract Cursor fetch(String cmd);

	/**
	 * Stop work with database. Close database. Release used resources and data structures. 
	 */
	protected abstract void stop();

	/**
	 * Start work with database, prepare necessary data structures.
	 */
	protected abstract void start();

	/**
	 * Connect processor with appropriate SQLite database provider.
	 *
	 * @param dbProvider the new database provider
	 * @throws DbIncorrectVersionException the database incorrect version exception
	 * @throws DbOpenErrorException the database open error exception
	 */
	protected abstract void setDbProvider(DbProvider<?> dbProvider) throws DbIncorrectVersionException, DbOpenErrorException;

	/**
	 * @return the SQLiteDatabase objectionable connection
	 */
	public abstract ISQLDatabase getISQLiteConcreteDb();
}
