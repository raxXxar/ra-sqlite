package ra.dbengine.interfaces;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 *  @author =ra=
 */
public interface SQLiteDbProvider extends DbProvider<SQLiteDatabase> {

	public void setContext(Context context);
}
