#include <jni.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_exception.h"
#include "ra_recordset.h"
#include "ra_sqlite_SQLiteCursor.h"
#include "ra_sqlite_SQLiteCursor_.h"
#include "ra_pool.h"
#include "ra_sqlite_SQLiteDatabase_.h"
#include "ra_accessor.h"

static void __loadRow(Cursor* pCursor, jint position);

static jint __loadBlock(Cursor* pCursor, jint start, jint end);

static void __loadColumnValue(Cursor* pCursor, Field* pField, jint columnIdx);

static jint __calcRowCount(Cursor* pCursor);

static jint __moveTo(Cursor* pCursor, jint position);

Cursor* Cursor_Create(Dbh* pDbh, sqlite3_stmt* pPreparedStatement, jint rowsPerExtent, jint blobExtentSize) {
	Cursor* pCursor = (Cursor*) malloc(sizeof(Cursor));
	pCursor->pDb = pDbh;
	pCursor->pStatement = pPreparedStatement;
	pCursor->recordsetPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	pCursor->cursorPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	pCursor->fieldCount = sqlite3_column_count(pCursor->pStatement);
	pCursor->rowsCount = ra_sqlite_SQLiteCursor_UNDEFINED;
	pCursor->rowsPerExtent = rowsPerExtent;
	pCursor->blobExtentSize = blobExtentSize;
	pCursor->pRecordSet = RecordSet_Create(pCursor->fieldCount, pCursor->rowsPerExtent, pCursor->blobExtentSize);
	Array_Insert(pDbh->pCursors, pCursor);
	pCursor->pAccessor = NULL;
	__INIT(pCursor);
	pCursor->g_poolId = registerInPool(pCursor);
	Accessor_ConnectToCursor(pCursor->g_poolId, DEF_FAST_ACCESS_COLUMN_NAME);
	return pCursor;
}

jint Cursor_Reset(Cursor* pCursor) {
	__LOCK(pCursor);
	jint err = cursor_InnerReset(pCursor);
	__UNLOCK(pCursor);
	return err;
}

jint cursor_InnerReset(Cursor* pCursor) {
	pCursor->cursorPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	return sqlite3_reset(pCursor->pStatement);
}

jint Cursor_Deactivate(Cursor* pCursor) {
	__LOCK(pCursor);
	pCursor->recordsetPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	pCursor->cursorPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
	pCursor->rowsCount = ra_sqlite_SQLiteCursor_UNDEFINED;
	if (NULL != pCursor->pRecordSet) {
		RecordSet_Reset(pCursor->pRecordSet);
	}
	jint err = sqlite3_reset(pCursor->pStatement);
	Accessor_Deactivate(pCursor->pAccessor);
	__UNLOCK(pCursor);
	return err;
}

void Cursor_Free(Cursor* pCursor) {
	__LOCK(pCursor);
	if (NULL != pCursor->pRecordSet) {
		RecordSet_Free(pCursor->pRecordSet);
		pCursor->pRecordSet = NULL;
	}
	pCursor->pAccessor = Accessor_Free(pCursor->pAccessor);
	unregisterInPool(pCursor->g_poolId);
	__UNLOCK(pCursor);
	free(pCursor);
}

jint Cursor_GetRowCount(Cursor* pCursor, jint* resultRowCount) {
	__LOCK(pCursor);
	jint err = SQLITE_OK;
	if (ra_sqlite_SQLiteCursor_UNDEFINED == pCursor->rowsCount) {
		err = __calcRowCount(pCursor);
	}
	*resultRowCount = pCursor->rowsCount;
	__UNLOCK(pCursor);
	return err;
}

jint Cursor_GetColumnCount(Cursor* pCursor) {
	return pCursor->fieldCount;
}

jint Cursor_GetColumnType(Cursor* pCursor, jint columnIndex) {
	jint result;
	__LOCK(pCursor);
	result = RecordSet_GetType(RecordSet_GetField(pCursor->pRecordSet, pCursor->recordsetPosition, columnIndex));
	__UNLOCK(pCursor);
	return result;
}

jint Cursor_GetColumnIsNull(Cursor* pCursor, jint columnIndex) {
	return SQLITE_NULL == Cursor_GetColumnType(pCursor, columnIndex);
}

jint Cursor_GetColumnInt64(Cursor* pCursor, jint columnIndex, jlong* resultValue) {
	jint error = CURSOR_ERROR_CODE_OK;
	__LOCK(pCursor);
	Field* pField = RecordSet_GetField(pCursor->pRecordSet, pCursor->recordsetPosition, columnIndex);
	switch (RecordSet_GetType(pField)) {
		case SQLITE_INTEGER:
			*resultValue = RecordSet_GetInt64(pField);
			break;
		case SQLITE_FLOAT:
			*resultValue = (jlong) RecordSet_GetDouble(pField);
			break;
		case SQLITE_TEXT:
			if (pField->value.ptr.size > 1) {
				*resultValue = strtoll((char*) pField->value.ptr.data, NULL, 0);
			}
			break;
		case SQLITE_BLOB:
			error = CURSOR_ERROR_CODE_BLOB_TO_LONG;
			break;
		case SQLITE_NULL:
			break;
		default:
			__LE("Unknown Type[ %d ] Cursor[%d]", RecordSet_GetType(pField), pCursor);
			error = CURSOR_ERROR_CODE_UNKNOWN_TYPE;
			break;
	}__UNLOCK(pCursor);
	return error;
}

jint Cursor_GetColumnDouble(Cursor* pCursor, jint columnIndex, jdouble* resultValue) {
	jint error = CURSOR_ERROR_CODE_OK;
	__LOCK(pCursor);
	Field* pField = RecordSet_GetField(pCursor->pRecordSet, pCursor->recordsetPosition, columnIndex);
	switch (RecordSet_GetType(pField)) {
		case SQLITE_INTEGER:
			*resultValue = (jdouble) RecordSet_GetInt64(pField);
			break;
		case SQLITE_FLOAT:
			*resultValue = RecordSet_GetDouble(pField);
			break;
		case SQLITE_TEXT:
			if (pField->value.ptr.size > 1) {
				*resultValue = strtod((char*) pField->value.ptr.data, NULL );
			}
			break;
		case SQLITE_BLOB:
			error = CURSOR_ERROR_CODE_BLOB_TO_DOUBLE;
			break;
		case SQLITE_NULL:
			break;
		default:
			__LE("Unknown Type[ %d ]", RecordSet_GetType(pField));
			error = CURSOR_ERROR_CODE_UNKNOWN_TYPE;
			break;
	}__UNLOCK(pCursor);
	return error;
}

jint Cursor_GetColumnString(Cursor* pCursor, jint columnIndex, jstring* resultValue, JNIEnv* jEnv) {
	jint error = CURSOR_ERROR_CODE_OK;
	__LOCK(pCursor);
	Field* pField = RecordSet_GetField(pCursor->pRecordSet, pCursor->recordsetPosition, columnIndex);
	switch (RecordSet_GetType(pField)) {
		case SQLITE_INTEGER: {
			char buf[32];
			jlong value = RecordSet_GetInt64(pField);
			snprintf(buf, sizeof(buf), "%lld", value);
			*resultValue = (*jEnv)->NewStringUTF(jEnv, buf);
			break;
		}
		case SQLITE_FLOAT: {
			char buf[32];
			jdouble value = RecordSet_GetDouble(pField);
			snprintf(buf, sizeof(buf), "%g", value);
			*resultValue = (*jEnv)->NewStringUTF(jEnv, buf);
			break;
		}
		case SQLITE_TEXT:
			*resultValue = RecordSet_GetString(pField, jEnv);
			break;
		case SQLITE_BLOB:
			error = CURSOR_ERROR_CODE_BLOB_TO_STRING;
			break;
		case SQLITE_NULL:
			break;
		default:
			__LE("Unknown Type[ %d ]", RecordSet_GetType(pField));
			error = CURSOR_ERROR_CODE_UNKNOWN_TYPE;
			break;
	}__UNLOCK(pCursor);
	return error;
}

jint Cursor_GetColumnBlob(Cursor* pCursor, jint columnIndex, jbyteArray* resultValue, JNIEnv* jEnv) {
	jint error = CURSOR_ERROR_CODE_OK;
	__LOCK(pCursor);
	Field* pField = RecordSet_GetField(pCursor->pRecordSet, pCursor->recordsetPosition, columnIndex);
	switch (RecordSet_GetType(pField)) {
		case SQLITE_INTEGER:
			error = CURSOR_ERROR_CODE_INTEGER_TO_BLOB;
			break;
		case SQLITE_FLOAT:
			error = CURSOR_ERROR_CODE_FLOAT_TO_BLOB;
			break;
		case SQLITE_TEXT:
		case SQLITE_BLOB:
			*resultValue = RecordSet_GetBlob(pField, jEnv);
			break;
		case SQLITE_NULL:
			break;
		default:
			__LE("Unknown Type[ %d ]", RecordSet_GetType(pField));
			error = CURSOR_ERROR_CODE_UNKNOWN_TYPE;
			break;
	}__UNLOCK(pCursor);
	return error;
}

jint Cursor_MoveToPosition(Cursor* pCursor, jint position, jint* resultPosition) {

	jint err = SQLITE_OK;
	__LOCK(pCursor);
	if (!RecordSet_IsRead(pCursor->pRecordSet, position)) {
		// load block
		int start = 0, stop = 0;
		RecordSet_GetReadScope(pCursor->pRecordSet, position, &start, &stop);
		err = __loadBlock(pCursor, start, stop);
	}
	*resultPosition = pCursor->recordsetPosition = (SQLITE_OK == err) ? position : ra_sqlite_SQLiteCursor_UNDEFINED;
	__UNLOCK(pCursor);
	return err;
}

static jint __calcRowCount(Cursor* pCursor) {
	jint err;
	jint rowsCount = 0;

	while (1) {
		if (SQLITE_OK != (err = sqlite3_reset(pCursor->pStatement))) {
			break;
		}
		jlong maxBlockIndexValue;
		jint blockStartPosition = 0;
		jint indexColumnIndex = (NULL == pCursor->pAccessor) ? -1 : Accessor_GeIndexColumnIndex(pCursor->pAccessor);
		while (1) {
			if (SQLITE_ROW == (err = sqlite3_step(pCursor->pStatement))) {
				if (rowsCount < pCursor->rowsPerExtent) {
					__loadRow(pCursor, rowsCount);
				}
				++rowsCount;
				if (indexColumnIndex > 0) {
					maxBlockIndexValue = sqlite3_column_int64(pCursor->pStatement, indexColumnIndex);
					if (0 == rowsCount % pCursor->rowsPerExtent) {
						Accessor_AppendBlockIndex(pCursor->pAccessor,
						                          blockStartPosition,
						                          pCursor->rowsPerExtent,
						                          maxBlockIndexValue);
						blockStartPosition += pCursor->rowsPerExtent;
					}
				}
			} else {
				break;
			}
		}
		if (SQLITE_DONE == err) {
			if (indexColumnIndex > 0) {
				if (0 != rowsCount % pCursor->rowsPerExtent) {
					Accessor_AppendBlockIndex(pCursor->pAccessor,
					                          blockStartPosition,
					                          rowsCount % pCursor->rowsPerExtent,
					                          /* previous successful read */
					                          maxBlockIndexValue);
				}
			}
			sqlite3_reset(pCursor->pStatement);
			pCursor->rowsCount = rowsCount;
			RecordSet_SetRowsCount(pCursor->pRecordSet, pCursor->rowsCount);
			err = SQLITE_OK;
		}
		break;
	}
	return err;
}

static void __loadColumnValue(Cursor* pCursor, Field* pField, jint columnIdx) {
	if (NULL != pField) {
		switch (sqlite3_column_type(pCursor->pStatement, columnIdx)) {
			case SQLITE_INTEGER: {
				RecordSet_PutInt64(pField, sqlite3_column_int64(pCursor->pStatement, columnIdx));
				break;
			}
			case SQLITE_FLOAT: {
				RecordSet_PutDouble(pField, sqlite3_column_double(pCursor->pStatement, columnIdx));
				break;
			}
			case SQLITE_TEXT: {
				jint str_size = sqlite3_column_bytes(pCursor->pStatement, columnIdx);
				if (str_size) {
					str_size = str_size + 1;
				}
				RecordSet_PutString(pCursor->pRecordSet,
				                    pField,
				                    sqlite3_column_blob(pCursor->pStatement, columnIdx),
				                    str_size);
				break;
			}
			case SQLITE_BLOB: {
				RecordSet_PutBlob(pCursor->pRecordSet,
				                  pField,
				                  sqlite3_column_blob(pCursor->pStatement, columnIdx),
				                  sqlite3_column_bytes(pCursor->pStatement, columnIdx));
				break;
			}
			case SQLITE_NULL: {
				RecordSet_PutType(pField, SQLITE_NULL);
				break;
			}
			default:
				break;
		}
	}
}

static void __loadRow(Cursor* pCursor, jint position) {
	jint columnIdx;
	for (columnIdx = 0; columnIdx < pCursor->fieldCount; ++columnIdx) {
		__loadColumnValue(pCursor, RecordSet_GetField(pCursor->pRecordSet, position, columnIdx), columnIdx);
	}
}

static jint __moveTo(Cursor* pCursor, jint position) {
	int err = SQLITE_OK;
	for (;;) {
		if (position == pCursor->cursorPosition) {
			break;
		}
		if (position < pCursor->cursorPosition) {
			err = sqlite3_reset(pCursor->pStatement);
			if (SQLITE_OK != err) {
				break;
			}
			pCursor->cursorPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
		}
		for (; pCursor->cursorPosition < position; ++pCursor->cursorPosition) {
			err = sqlite3_step(pCursor->pStatement);

			if (SQLITE_ROW != err) {
				break;
			}
		}
		if (SQLITE_DONE == err) {
			sqlite3_reset(pCursor->pStatement);
			pCursor->cursorPosition = ra_sqlite_SQLiteCursor_UNDEFINED;
		} else if (SQLITE_ROW == err) {
			err = SQLITE_OK;
		}
		break;
	}
	return err;
}

static jint __loadBlock(Cursor* pCursor, jint start, jint end) {
	static jint err;
	while (start < end) {
		if (SQLITE_OK == (err = __moveTo(pCursor, start))) {
			__loadRow(pCursor, start++);
			continue;
		}
		break;
	}
	return err;
}
