LOCAL_PATH := $(call my-dir)

icu_src_c_files := 			\
	bocsu.c				\
	cmemory.c			\
	cstring.c			\
	cwchar.c			\
	decContext.c		\
	decNumber.c			\
	icuplug.c			\
	locmap.c			\
	propsvec.c			\
	punycode.c			\
	putil.c				\
	uarrsort.c			\
	ubidi.c				\
	ubidi_props.c		\
	ubidiln.c			\
	ubidiwrt.c			\
	ucase.c				\
	ucasemap.c			\
	ucat.c				\
	uchar.c				\
	ucln_cmn.c			\
	ucln_in.c			\
	ucmndata.c			\
	ucnv.c				\
	ucnv_bld.c			\
	ucnv_cb.c			\
	ucnv_cnv.c			\
	ucnv_err.c			\
	ucnv_ext.c			\
	ucnv_io.c			\
	ucnv_lmb.c			\
	ucnv_set.c			\
	ucnv_u16.c			\
	ucnv_u32.c			\
	ucnv_u7.c			\
	ucnv_u8.c			\
	ucnv2022.c			\
	ucnvbocu.c			\
	ucnvdisp.c			\
	ucnvhz.c			\
	ucnvisci.c			\
	ucnvlat1.c			\
	ucnvmbcs.c			\
	ucnvscsu.c			\
	udata.c				\
	udatamem.c			\
	udataswp.c			\
	uenum.c				\
	uhash.c				\
	uinit.c				\
	uinvchar.c			\
	ulist.c				\
	uloc.c				\
	uloc_tag.c			\
	ulocdata.c			\
	umapfile.c			\
	umath.c				\
	umutex.c			\
	unames.c			\
	unorm_it.c			\
	ures_cnv.c			\
	uresbund.c			\
	uresdata.c			\
	usc_impl.c			\
	uscript.c			\
	ushape.c			\
	ustr_cnv.c			\
	ustr_wcs.c			\
	ustrcase.c			\
	ustrfmt.c			\
	ustring.c			\
	ustrtrns.c			\
	utf_impl.c			\
	utmscale.c			\
	utrace.c			\
	utrie.c				\
	utrie2_builder.c	\
	utypes.c			\
	wintz.c				\
	

icu_src_cpp_files := 		\
	anytrans.cpp		\
	astro.cpp			\
	basictz.cpp			\
	bmpset.cpp			\
	bms.cpp				\
	bmsearch.cpp		\
	brkeng.cpp			\
	brkiter.cpp			\
	brktrans.cpp		\
	buddhcal.cpp		\
	bytestream.cpp		\
	calendar.cpp		\
	caniter.cpp			\
	casetrn.cpp			\
	cecal.cpp			\
	chariter.cpp		\
	chnsecal.cpp		\
	choicfmt.cpp		\
	coleitr.cpp			\
	coll.cpp			\
	colldata.cpp		\
	coptccal.cpp		\
	cpdtrans.cpp		\
	csdetect.cpp		\
	csmatch.cpp			\
	csr2022.cpp			\
	csrecog.cpp			\
	csrmbcs.cpp			\
	csrsbcs.cpp			\
	csrucode.cpp		\
	csrutf8.cpp			\
	curramt.cpp			\
	currfmt.cpp			\
	currpinf.cpp		\
	currunit.cpp		\
	datefmt.cpp			\
	dcfmtsym.cpp		\
	decimfmt.cpp		\
	decnumstr.cpp		\
	dictbe.cpp			\
	digitlst.cpp		\
	dtfmtsym.cpp		\
	dtintrv.cpp			\
	dtitvfmt.cpp		\
	dtitvinf.cpp		\
	dtptngen.cpp		\
	dtrule.cpp			\
	errorcode.cpp		\
	esctrn.cpp			\
	ethpccal.cpp		\
	filterednormalizer2.cpp	\
	fmtable.cpp			\
	fmtable_cnv.cpp		\
	format.cpp			\
	fphdlimp.cpp		\
	fpositer.cpp		\
	funcrepl.cpp		\
	gregocal.cpp		\
	gregoimp.cpp		\
	hebrwcal.cpp		\
	indiancal.cpp		\
	inputext.cpp		\
	islamcal.cpp		\
	japancal.cpp		\
	locavailable.cpp	\
	locbased.cpp		\
	locdispnames.cpp	\
	locdspnm.cpp		\
	locid.cpp			\
	loclikely.cpp		\
	locresdata.cpp		\
	locutil.cpp			\
	measfmt.cpp			\
	measure.cpp			\
	msgfmt.cpp			\
	mutex.cpp			\
	name2uni.cpp		\
	nfrs.cpp			\
	nfrule.cpp			\
	nfsubs.cpp			\
	normalizer2.cpp		\
	normalizer2impl.cpp	\
	normlzr.cpp			\
	nortrans.cpp		\
	nultrans.cpp		\
	numfmt.cpp			\
	numsys.cpp			\
	olsontz.cpp			\
	parsepos.cpp		\
	persncal.cpp		\
	plurfmt.cpp			\
	plurrule.cpp		\
	propname.cpp		\
	quant.cpp			\
	rbbi.cpp			\
	rbbidata.cpp		\
	rbbinode.cpp		\
	rbbirb.cpp			\
	rbbiscan.cpp		\
	rbbisetb.cpp		\
	rbbistbl.cpp		\
	rbbitblb.cpp		\
	rbnf.cpp			\
	rbt.cpp				\
	rbt_data.cpp		\
	rbt_pars.cpp		\
	rbt_rule.cpp		\
	rbt_set.cpp			\
	rbtz.cpp			\
	regexcmp.cpp		\
	regexst.cpp			\
	regextxt.cpp		\
	reldtfmt.cpp		\
	rematch.cpp			\
	remtrans.cpp		\
	repattrn.cpp		\
	resbund.cpp			\
	resbund_cnv.cpp		\
	ruleiter.cpp		\
	schriter.cpp		\
	search.cpp			\
	selfmt.cpp			\
	serv.cpp			\
	servlk.cpp			\
	servlkf.cpp			\
	servls.cpp			\
	servnotf.cpp		\
	servrbf.cpp			\
	servslkf.cpp		\
	simpletz.cpp		\
	smpdtfmt.cpp		\
	sortkey.cpp			\
	stringpiece.cpp		\
	strmatch.cpp		\
	strrepl.cpp			\
	stsearch.cpp		\
	taiwncal.cpp		\
	tblcoll.cpp			\
	timezone.cpp		\
	titletrn.cpp		\
	tmunit.cpp			\
	tmutamt.cpp			\
	tmutfmt.cpp			\
	tolowtrn.cpp		\
	toupptrn.cpp		\
	translit.cpp		\
	transreg.cpp		\
	tridpars.cpp		\
	triedict.cpp		\
	tzrule.cpp			\
	tztrans.cpp			\
	ubrk.cpp			\
	ucal.cpp			\
	uchriter.cpp		\
	ucnvsel.cpp			\
	ucol.cpp			\
	ucol_bld.cpp		\
	ucol_cnt.cpp		\
	ucol_elm.cpp		\
	ucol_res.cpp		\
	ucol_sit.cpp		\
	ucol_swp.cpp		\
	ucol_tok.cpp		\
	ucol_wgt.cpp		\
	ucoleitr.cpp		\
	ucsdet.cpp			\
	ucurr.cpp			\
	udat.cpp			\
	udatpg.cpp			\
	uhash_us.cpp		\
	uidna.cpp			\
	uiter.cpp			\
	umsg.cpp			\
	unesctrn.cpp		\
	uni2name.cpp		\
	unifilt.cpp			\
	unifunct.cpp		\
	uniset.cpp			\
	uniset_props.cpp	\
	unisetspan.cpp		\
	unistr.cpp			\
	unistr_case.cpp		\
	unistr_cnv.cpp		\
	unistr_props.cpp	\
	unorm.cpp			\
	unormcmp.cpp		\
	unum.cpp			\
	uobject.cpp			\
	uprops.cpp			\
	uregex.cpp			\
	uregexc.cpp			\
	usearch.cpp			\
	uset.cpp			\
	uset_props.cpp		\
	usetiter.cpp		\
	uspoof.cpp			\
	uspoof_build.cpp	\
	uspoof_conf.cpp		\
	uspoof_impl.cpp		\
	uspoof_wsconf.cpp	\
	usprep.cpp			\
	ustack.cpp			\
	ustrenum.cpp		\
	utext.cpp			\
	util.cpp			\
	util_props.cpp		\
	utrans.cpp			\
	utrie2.cpp			\
	uvector.cpp			\
	uvectr32.cpp		\
	uvectr64.cpp		\
	vtzone.cpp			\
	vzone.cpp			\
	windtfmt.cpp		\
	winnmfmt.cpp		\
	wintzimpl.cpp		\
	zonemeta.cpp		\
	zrule.cpp			\
	zstrfmt.cpp			\
	ztrans.cpp			
				
icu_src_sqlite_files := \
	sqlite3.c	
	
icu_src_files := $(addprefix src/,$(icu_src_c_files)) $(addprefix src/,$(icu_src_cpp_files))  
icu_src_files += $(addprefix src/sqlite/,$(icu_src_sqlite_files))
icu_src_files += icudt44l_dat.s 

local_ldlibs := -lz -lm -llog -lc -L$(call host-path, $(LOCAL_PATH))/$(TARGET_ARCH_ABI) 

c_includes := $(LOCAL_PATH)	

cf_includes_local := inc inc/unicode  
cf_includes := $(addprefix -Ijni/sqlite/,$(cf_includes_local))

export_c_includes := $(c_includes)

# We make the ICU data directory relative to $ANDROID_ROOT on Android, so both
# device and sim builds can use the same codepath, and it's hard to break one
# without noticing because the other still works.
icu_local_cflags := '-DICU_DATA_DIR_PREFIX_ENV_VAR="ANDROID_ROOT"'
icu_local_cflags += '-DICU_DATA_DIR="/usr/icu"'
icu_local_cflags += -D_REENTRANT -DU_COMMON_IMPLEMENTATION -O3 -DU_I18N_IMPLEMENTATION
icu_local_cflags += -DPIC -fPIC -DANDROID_NDK -Dfdatasync=fsync -DSQLITE_ENABLE_FTS3 
icu_local_cflags += -DSQLITE_ENABLE_ICU -DSQLITE_ENABLE_FTS3_PARENTHESIS
icu_local_cflags += -DSQLITE_THREADSAFE 


include $(CLEAR_VARS)

LOCAL_MODULE			:= libicusqlite
LOCAL_MODULE_TAGS		:= optional
LOCAL_CFLAGS 			:= $(icu_local_cflags) 
LOCAL_CFLAGS 			+= $(cf_includes)
LOCAL_LDLIBS			+= $(local_ldlibs)
LOCAL_EXPORT_C_INCLUDES := $(export_c_includes)
LOCAL_C_INCLUDES		:= $(c_includes)
LOCAL_SRC_FILES			:= $(icu_src_files)
LOCAL_LDFLAGS   		:= 

include $(BUILD_STATIC_LIBRARY)
