/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class ra_sqlite_SQLiteCursor */
#ifndef _Included_ra_sqlite_SQLiteCursor
#define _Included_ra_sqlite_SQLiteCursor
#undef ra_sqlite_SQLiteCursor_UNDEFINED
#define ra_sqlite_SQLiteCursor_UNDEFINED -1L
#undef ra_sqlite_SQLiteCursor_CURSOR_HANDLE_UNDEFINED
#define ra_sqlite_SQLiteCursor_CURSOR_HANDLE_UNDEFINED 0L
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    moveTo
 * Signature: (II)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_moveTo(JNIEnv *,
                                                           jobject,
                                                           jint,
                                                           jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnCount
 * Signature: (I)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnCount(JNIEnv *,
                                                                jobject,
                                                                jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnType
 * Signature: (II)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnType(JNIEnv *,
                                                               jobject,
                                                               jint,
                                                               jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnIsNull
 * Signature: (II)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_columnIsNull(JNIEnv *,
                                                                 jobject,
                                                                 jint,
                                                                 jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnInt64Value
 * Signature: (II)J
 */JNIEXPORT jlong JNICALL Java_ra_sqlite_SQLiteCursor_columnInt64Value(JNIEnv *,
                                                                      jobject,
                                                                      jint,
                                                                      jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnDoubleValue
 * Signature: (II)D
 */JNIEXPORT jdouble JNICALL Java_ra_sqlite_SQLiteCursor_columnDoubleValue(JNIEnv *,
                                                                         jobject,
                                                                         jint,
                                                                         jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnStringValue
 * Signature: (II)Ljava/lang/String;
 */JNIEXPORT jstring JNICALL Java_ra_sqlite_SQLiteCursor_columnStringValue(JNIEnv *,
                                                                         jobject,
                                                                         jint,
                                                                         jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    columnByteBufferValue
 * Signature: (II)[B
 */JNIEXPORT jbyteArray JNICALL Java_ra_sqlite_SQLiteCursor_columnByteBufferValue(JNIEnv *,
                                                                                jobject,
                                                                                jint,
                                                                                jint);/*
                                                                                 * Class:     ra_sqlite_SQLiteCursor
                                                                                 * Method:    columnName
                                                                                 * Signature: (II)Ljava/lang/String;
                                                                                 */
JNIEXPORT jstring JNICALL Java_ra_sqlite_SQLiteCursor_columnName(JNIEnv *,
                                                                 jobject,
                                                                 jint,
                                                                 jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    rowsCount
 * Signature: (I)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_rowsCount(JNIEnv *,
                                                              jobject,
                                                              jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    deactivateCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_deactivateCursor(JNIEnv *,
                                                                    jobject,
                                                                    jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    resetCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_resetCursor(JNIEnv *,
                                                               jobject,
                                                               jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    createCursor
 * Signature: (II)I
 */JNIEXPORT jint JNICALL Java_ra_sqlite_SQLiteCursor_createCursor(JNIEnv *,
                                                                 jobject,
                                                                 jint,
                                                                 jint);
/*
 * Class:     ra_sqlite_SQLiteCursor
 * Method:    destroyCursor
 * Signature: (I)V
 */

JNIEXPORT void JNICALL Java_ra_sqlite_SQLiteCursor_destroyCursor(JNIEnv *,
                                                                 jobject,
                                                                 jint);

#endif
