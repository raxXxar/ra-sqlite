package ra.sqlite.test;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import ra.sqlite.SQLiteDatabase;

public class TestHelper {

	public static final double DBL_FRACTION = .23;

	public static ByteBuffer str2buf(String str) {
		try {
			ByteBuffer buf = encoder.encode(CharBuffer.wrap(str));
			ByteBuffer retbuf = ByteBuffer.allocateDirect(buf.capacity());
			retbuf.put(buf);
			return retbuf;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String buf2str(ByteBuffer buf) {
		String str = "";
		try {
			int old_position = buf.position();
			str = decoder.decode(buf).toString();
			// reset buffer's position to its original so it is not altered:
			buf.position(old_position);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return str.trim();
	}

	public static long setupTable(SQLiteDatabase db, int rowsCount) {
		db.execSQL("DROP TABLE IF EXISTS test");
		db.execSQL("CREATE TABLE IF NOT EXISTS test(i INTEGER, f FLOAT, t TEXT, b BLOB)");
		for (int i = 0; i < rowsCount; ++i) {
			Integer vInt = Integer.valueOf(i);
			Double vDbl = Double.valueOf(DBL_FRACTION + vInt);
			String vStr = new String("TEXT " + String.valueOf(vInt));
			ByteBuffer vBuf = TestHelper.str2buf(vStr);
			db.execSQL("INSERT INTO test(i,f,t,b) VALUES (?, ?, ?, ?)", new Object[] { vInt, vDbl, vStr, vBuf });
		}
		return rowsCount;
	}

	public static long setupTable2(SQLiteDatabase db, int rowsCount) {
		db.execSQL("DROP TABLE IF EXISTS testInt");
		db.execSQL("CREATE TABLE IF NOT EXISTS testInt(i INTEGER, f FLOAT)");
		for (int i = 0; i < rowsCount; ++i) {
			Integer vInt = Integer.valueOf(i);
			Double vDbl = Double.valueOf(DBL_FRACTION + vInt);
			db.execSQL("INSERT INTO testInt(i,f) VALUES (?, ?)", new Object[] { vInt, vDbl });
		}
		return rowsCount;
	}

	private static final Charset charset = Charset.forName("UTF-8");
	private static final CharsetEncoder encoder = charset.newEncoder();
	private static final CharsetDecoder decoder = charset.newDecoder();
}
