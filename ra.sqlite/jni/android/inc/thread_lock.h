/*
 * thread_lock.h
 *
 *  Created on: 13 груд. 2012
 *      Author: akulyn
 */

#ifndef THREAD_LOCK_H_
#define THREAD_LOCK_H_

#ifdef THREAD_SAFE
#include <semaphore.h>
#define __INIT(ptr) sem_init(&(ptr)->lock, 0, 1);
#define __LOCK(ptr) {sem_wait(&(ptr)->lock);};
#define __UNLOCK(ptr) {sem_post(&(ptr)->lock);};
#define __LMEMBER sem_t lock
#else
#define __INIT(ptr) ;
#define __LOCK(ptr) ;
#define __UNLOCK(ptr) ;
#define __LMEMBER	;
#endif

#endif /* THREAD_LOCK_H_ */
