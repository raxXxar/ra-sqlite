package ra.sqlite;

import java.io.File;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import ra.sqlite.exceptions.NoSuchDatabaseException;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteTransactionListener;
import android.text.TextUtils;
import android.util.Log;

public class SQLiteDatabase {

	// @formatter:off
	public static final int SQLITE_OPEN_READONLY 			= 0x00000001; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_READWRITE			= 0x00000002; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_CREATE				= 0x00000004; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_DELETEONCLOSE		= 0x00000008; /* VFS only */
	public static final int SQLITE_OPEN_EXCLUSIVE			= 0x00000010; /* VFS only */
	public static final int SQLITE_OPEN_AUTOPROXY			= 0x00000020; /* VFS only */
	public static final int SQLITE_OPEN_URI					= 0x00000040; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_MEMORY				= 0x00000080; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_MAIN_DB				= 0x00000100; /* VFS only */
	public static final int SQLITE_OPEN_TEMP_DB				= 0x00000200; /* VFS only */
	public static final int SQLITE_OPEN_TRANSIENT_DB		= 0x00000400; /* VFS only */
	public static final int SQLITE_OPEN_MAIN_JOURNAL		= 0x00000800; /* VFS only */
	public static final int SQLITE_OPEN_TEMP_JOURNAL		= 0x00001000; /* VFS only */
	public static final int SQLITE_OPEN_SUBJOURNAL			= 0x00002000; /* VFS only */
	public static final int SQLITE_OPEN_MASTER_JOURNAL		= 0x00004000; /* VFS only */
	public static final int SQLITE_OPEN_NOMUTEX				= 0x00008000; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_FULLMUTEX			= 0x00010000; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_SHAREDCACHE			= 0x00020000; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_PRIVATECACHE		= 0x00040000; /* Ok for sqlite3_open_v2() */
	public static final int SQLITE_OPEN_WAL					= 0x00080000; /* VFS only */
	// @formatter:on

	public static SQLiteDatabase openDatabase(String path) {
		return openDatabase(path, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE);
	}

	public static SQLiteDatabase openDatabase(String path, int flags) {
		SQLiteDatabase sqliteDatabase = null;
		try {
			// Open the database
			sqliteDatabase = new SQLiteDatabase(path, flags);
		} catch (RuntimeException e) {
			Log.e(TAG, "database " + path, e);
			throw e;
		}
		return sqliteDatabase;
	}

	/**
	 * Protected constructor. See {@link #openDatabase}.
	 * @param path The full path to the database
	 * @param flags Database open/create flags 
	 */
	protected SQLiteDatabase(String path, int flags) {
		if (path == null) {
			throw new IllegalArgumentException("path should not be null");
		}
		dbPathName = path;
		openFlags = flags;
		dbHandle = dbOpen(path, openFlags);
		try {
			File file = new File(path);
			String absoluteDbPath = file.getAbsolutePath();
			String dbPathPrefix = absoluteDbPath.substring(0, absoluteDbPath.lastIndexOf(File.separator));
			String pragma = "PRAGMA TEMP_STORE_DIRECTORY = '" + dbPathPrefix + "'";
			execSQL(pragma);
			dbVersion = checkDbVersion();
		} catch (RuntimeException e) {
			Log.e(TAG, "Failed to adjust db settings when constructing, closing the database", e);
			close();
			throw e;
		} finally {
			inTransaction = false;
		}
	}

	public void setLocale(String localeStr) {
		if (!isOpen()) {
			throw new IllegalStateException("Database is not open");
		}
		dbSetLocale(dbHandle, localeStr);
	}

	public Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having,
			String orderBy, String limit) {
		String sql = SQLiteQueryBuilder.buildQueryString(distinct, table, columns, selection, groupBy, having, orderBy, limit);
		return rawQuery(sql, selectionArgs);
	}

	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
		return query(false, table, columns, selection, selectionArgs, groupBy, having, orderBy, null /* limit */);
	}

	public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
		return query(false, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}

	public Cursor rawQuery(String sql, String[] selectionArgs) {
		return rawQueryBind(sql, selectionArgs);
	}

	public long insert(String table, String nullColumnHack, ContentValues initialValues) {
		List<Object> bindArgs = new LinkedList<Object>();
		StringBuilder sql = new StringBuilder(152);
		sql.append("INSERT INTO ");
		sql.append(table);
		StringBuilder values = new StringBuilder(40);
		Set<Map.Entry<String, Object>> entrySet = null;
		if (initialValues != null && initialValues.size() > 0) {
			entrySet = initialValues.valueSet();
			Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
			sql.append('(');
			boolean needSeparator = false;
			while (entriesIter.hasNext()) {
				if (needSeparator) {
					sql.append(", ");
					values.append(", ");
				}
				needSeparator = true;
				Map.Entry<String, Object> entry = entriesIter.next();
				sql.append(entry.getKey());
				values.append('?');
				bindArgs.add(entry.getValue());
			}
			sql.append(')');
		} else {
			sql.append("(" + nullColumnHack + ") ");
			values.append("NULL");
		}
		sql.append(" VALUES(");
		sql.append(values);
		sql.append(");");
		return execSQL(sql.toString(), bindArgs.toArray(new Object[0]));
	}

	public long replaceOrThrow(String table, String nullColumnHack, ContentValues initialValues) throws SQLException {
		if (initialValues == null || initialValues.size() == 0) {
			throw new IllegalArgumentException("Empty values");
		}
		List<Object> bindArgs = new LinkedList<Object>();
		StringBuilder sql = new StringBuilder(152);
		sql.append("REPLACE INTO ");
		sql.append(table);
		StringBuilder values = new StringBuilder(40);
		Set<Map.Entry<String, Object>> entrySet = null;
		if (initialValues != null && initialValues.size() > 0) {
			entrySet = initialValues.valueSet();
			Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
			sql.append('(');
			boolean needSeparator = false;
			while (entriesIter.hasNext()) {
				if (needSeparator) {
					sql.append(", ");
					values.append(", ");
				}
				needSeparator = true;
				Map.Entry<String, Object> entry = entriesIter.next();
				sql.append(entry.getKey());
				values.append('?');
				bindArgs.add(entry.getValue());
			}
			sql.append(')');
		} else {
			sql.append("(" + nullColumnHack + ") ");
			values.append("NULL");
		}
		sql.append(" VALUES(");
		sql.append(values);
		sql.append(");");
		return execSQL(sql.toString(), bindArgs.toArray(new Object[0]));
	}

	public long replace(String table, String nullColumnHack, ContentValues initialValues) {
		try {
			return replaceOrThrow(table, nullColumnHack, initialValues);
		} catch (SQLException e) {
			Log.e(TAG, "Error replacing " + initialValues, e);
			return -1;
		}
	}

	public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
		if (values == null || values.size() == 0) {
			throw new IllegalArgumentException("Empty values");
		}
		StringBuilder sql = new StringBuilder(120);
		sql.append("UPDATE ");
		sql.append(table);
		sql.append(" SET ");
		List<Object> bindArgs = new LinkedList<Object>();
		Set<Map.Entry<String, Object>> entrySet = values.valueSet();
		Iterator<Map.Entry<String, Object>> entriesIter = entrySet.iterator();
		while (entriesIter.hasNext()) {
			Map.Entry<String, Object> entry = entriesIter.next();
			sql.append(entry.getKey());
			sql.append("=?");
			if (entriesIter.hasNext()) {
				sql.append(", ");
			}
			bindArgs.add(entry.getValue());
		}
		if (!TextUtils.isEmpty(whereClause)) {
			sql.append(" WHERE ");
			sql.append(whereClause);
			if (whereArgs != null) {
				for (String whereArg : whereArgs) {
					bindArgs.add(whereArg);
				}
			}
		}
		return execSQL(sql.toString(), bindArgs.toArray(new Object[0]));
	}

	public int delete(String table, String whereClause, String[] whereArgs) {
		String sql = "DELETE FROM " + table + (!TextUtils.isEmpty(whereClause) ? " WHERE " + whereClause : "");
		return execSQL(sql, whereArgs);
	}

	public void beginTransaction() {
		beginTransactionWithListener(null /* transactionListener */);
	}

	public void beginTransactionWithListener(SQLiteTransactionListener transactionListener) {
		if (!isOpen()) {
			throw new IllegalStateException("Database is not open");
		}
		if (innerTransactionIsSuccessful) {
			String msg = "Cannot call beginTransaction between " + "calling setTransactionSuccessful and endTransaction";
			IllegalStateException e = new IllegalStateException(msg);
			throw e;
		}
		dbStartTransaction(dbHandle);
		this.transactionListener = transactionListener;
		inTransaction = true;
		transactionIsSuccessful = true;
		innerTransactionIsSuccessful = false;
		if (transactionListener != null) {
			try {
				transactionListener.onBegin();
			} catch (RuntimeException e) {
				lock(dbHandle);
				dbRollbackTransaction(dbHandle);
				unlock(dbHandle);
				throw e;
			}
		}
	}

	public void endTransaction() {
		if (!isOpen()) {
			throw new IllegalStateException("Database is not open");
		}
		if (!inTransaction) {
			throw new IllegalStateException("No transaction pending");
		}
		try {
			if (innerTransactionIsSuccessful) {
				innerTransactionIsSuccessful = false;
			} else {
				transactionIsSuccessful = false;
			}

			RuntimeException savedException = null;
			if (transactionListener != null) {
				try {
					if (transactionIsSuccessful) {
						transactionListener.onCommit();
					} else {
						transactionListener.onRollback();
					}
				} catch (RuntimeException e) {
					savedException = e;
					transactionIsSuccessful = false;
				}
			}
			if (transactionIsSuccessful) {
				dbCommitTransaction(dbHandle);
			} else {
				try {
					lock(dbHandle);
					dbRollbackTransaction(dbHandle);
					unlock(dbHandle);
					if (savedException != null) {
						throw savedException;
					}
				} catch (SQLException e) {
					Log.e(TAG, "Exception during rollback, maybe the DB previously " + "performed an auto-rollback");
				}
			}
		} finally {
			inTransaction = false;
			transactionListener = null;
		}
	}

	public void setTransactionSuccessful() {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		if (!inTransaction) {
			throw new IllegalStateException("No transaction pending");
		}
		if (innerTransactionIsSuccessful) {
			throw new IllegalStateException("setTransactionSuccessful may only be called once per call to beginTransaction");
		}
		innerTransactionIsSuccessful = true;
	}

	public boolean inTransaction() {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		return inTransaction;
	}

	public void close() {
		if (!isOpen()) {
			return; // already closed
		}
		try {
			Cursor[] cursorList = activeCursors.toArray();
			Log.i(TAG, "close: " + cursorList.length + " active cursors left!");
			for (Cursor cursor : cursorList) {
				cursor.close();
			}
			dbClose(dbHandle);
		} finally {
			dbHandle = 0;
		}
	}

	public int getVersion() {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		return dbVersion;
	}

	public void setVersion(int version) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		if (dbVersion != version) {
			execSQL("PRAGMA USER_VERSION=" + String.valueOf(version));
			dbVersion = version;
		}
	}

	public Cursor rawQuery(String sql) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		return createNRegisterCursor(statementHandle);
	}

	public Cursor rawQueryBind(String sql, Object[] args) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		if (null == args) {
			args = new Object[0];
		}
		try {
			bindStatement(statementHandle, args);
		} catch (SQLException e) {
			finalize(dbHandle, statementHandle);
			throw e;
		}
		return createNRegisterCursor(statementHandle);
	}

	public int execSQL(String sql) throws SQLException {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int affectedRows = 0;
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			affectedRows = exec(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
		return affectedRows;
	}

	public int execSQL(String sql, Object[] bindArgs) throws SQLException {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int affectedRows = 0;
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			bindStatement(statementHandle, bindArgs);
			affectedRows = exec(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
		return affectedRows;
	}

	public boolean isOpen() {
		return dbHandle != 0;
	}

	public boolean isReadOnly() {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		return (openFlags & SQLITE_OPEN_READONLY) == SQLITE_OPEN_READONLY;
	}

	public boolean needUpgrade(int newVersion) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		return newVersion > dbVersion;
	}

	protected int checkDbVersion() {
		return (int) simpleQueryForLong("PRAGMA USER_VERSION");
	}

	public long simpleQueryForLong(String sql) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			exec(statementHandle);
			return getInt64Value(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
	}

	public long simpleQueryForLong(String sql, Object[] bindArgs) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			bindStatement(statementHandle, bindArgs);
			exec(statementHandle);
			return getInt64Value(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
	}

	public String simpleQueryForString(String sql) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			exec(statementHandle);
			return getStringValue(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
	}

	public String simpleQueryForString(String sql, Object[] bindArgs) {
		if (!isOpen()) {
			throw new IllegalStateException("Database not open");
		}
		int statementHandle = prepareSql(dbHandle, sql);
		try {
			bindStatement(statementHandle, bindArgs);
			exec(statementHandle);
			return getStringValue(statementHandle);
		} finally {
			finalize(dbHandle, statementHandle);
		}
	}

	Cursor createNRegisterCursor(int statementHandle) {
		Cursor cursor = new SQLiteCursor(dbHandle, statementHandle);
		activeCursors.add(cursor);
		return cursor;
	}

	void bindStatement(int statementHandle, Object[] args) throws SQLException {
		if (null != args) {
			if (args.length != getStatementArgsCount(statementHandle)) {
				throw new IllegalArgumentException();
			} else {
				int i = 1;// SQLite args indexed from 1
				for (Object obj : args) {
					if (null == obj) {
						bindNull(statementHandle, i);
					} else if (obj instanceof Integer) {
						bindLong(statementHandle, i, ((Integer) obj).longValue());
					} else if (obj instanceof Long) {
						bindLong(statementHandle, i, ((Long) obj).longValue());
					} else if (obj instanceof Short) {
						bindLong(statementHandle, i, ((Short) obj).longValue());
					} else if (obj instanceof Double) {
						bindDouble(statementHandle, i, ((Double) obj).doubleValue());
					} else if (obj instanceof Float) {
						bindDouble(statementHandle, i, ((Float) obj).doubleValue());
					} else if (obj instanceof String) {
						bindString(statementHandle, i, (String) obj);
					} else if (obj instanceof Boolean) {
						bindLong(statementHandle, i, Long.valueOf(((Boolean) obj) ? 1L : 0L));
					} else if (obj instanceof byte[]) {
						bindByteArray(statementHandle, i, (byte[]) obj);
					} else if (obj instanceof ByteBuffer) {
						ByteBuffer buf = (ByteBuffer) obj;
						if (!buf.isDirect()) {
							throw new IllegalArgumentException("Only direct ByteBuffers are supported");
						}
						bindByteBuffer(statementHandle, i, (ByteBuffer) obj);
					} else {
						throw new IllegalArgumentException();
					}
					i++;
				}
			}
		}
	}

	private int prepareSql(int dbHandle, String sql) {
		if (null != sql && sql.length() > 0 && 0 != dbHandle) {
			return prepare(dbHandle, sql);
		} else {
			throw new IllegalArgumentException();
		}
	}

	native int dbOpen(String fileName, int dbOpenFlags) throws SQLException;

	native void dbClose(int dbHandle) throws SQLException, NoSuchDatabaseException;

	native void dbStartTransaction(int dbHandle) throws SQLException, NoSuchDatabaseException;

	native void dbCommitTransaction(int dbHandle) throws SQLException, NoSuchDatabaseException;

	native void dbRollbackTransaction(int dbHandle) throws SQLException, NoSuchDatabaseException;

	native void lock(int dbHandle) throws NoSuchDatabaseException;

	native void unlock(int dbHandle) throws NoSuchDatabaseException;

	native void bindByteArray(int statementHandle, int index, byte[] value) throws SQLException;

	native void bindByteBuffer(int statementHandle, int index, ByteBuffer value) throws SQLException;

	native void bindString(int statementHandle, int index, String value) throws SQLException;

	native void bindLong(int statementHandle, int index, long value) throws SQLException;

	native void bindDouble(int statementHandle, int index, double value) throws SQLException;

	native void bindNull(int statementHandle, int index) throws SQLException;

	native String[] getColumnNames(int statementHandle) throws SQLException;

	native void reset(int statementHandle) throws SQLException;

	native int prepare(int dbHandle, String sql) throws SQLException, NoSuchDatabaseException;

	native void finalize(int dbHandle, int statementHandle) throws SQLException, NoSuchDatabaseException;

	native int getStatementArgsCount(int statementHandle);

	native int exec(int statementHandle) throws SQLException;

	native long getInt64Value(int statementHandle);

	native String getStringValue(int statementHandle);

	native void dbSetLocale(int dbHandle, String locale) throws NoSuchDatabaseException;

	private int dbHandle;
	private final int openFlags;
	private final String dbPathName;
	private int dbVersion;

	private boolean inTransaction = false;
	private boolean transactionIsSuccessful = false;
	private boolean innerTransactionIsSuccessful = false;
	private SQLiteTransactionListener transactionListener = null;
	private static final String TAG = SQLiteDatabase.class.getSimpleName();
	static {
		try {
			System.loadLibrary("ra_sqlite");
		} catch (UnsatisfiedLinkError e) {
			Log.e(TAG, e.toString());
			throw e;
		}
	}

	private final CursorWeakRefList activeCursors = new CursorWeakRefList();

	@SuppressWarnings("unused")
	private static class CursorWeakRefList {

		public boolean contains(Cursor cursor) {
			return null != getCorrespondingWeakReference(cursor);
		}

		public void add(int index, Cursor element) {
			addElement(index, element);
		}

		public WeakReference<Cursor> addElement(int index, Cursor cursor) {
			synchronized (this) {
				WeakReference<Cursor> obj = new WeakReference<Cursor>(cursor, mReleasedQueue);
				mReferences.add(index, obj);
				return obj;
			}
		}

		public boolean add(Cursor cursor) {
			synchronized (this) {
				return mReferences.add(new WeakReference<Cursor>(cursor, mReleasedQueue));
			}
		}

		public int size() {
			synchronized (this) {
				processQueue();
				return mReferences.size();
			}
		}

		public Cursor get(int index) {
			synchronized (this) {
				processQueue();
				Cursor result = mReferences.get(index).get();
				return result;
			}
		}

		public WeakReference<Cursor> getCorrespondingWeakReference(Cursor cursor) {
			synchronized (this) {
				ListIterator<WeakReference<Cursor>> referenceListIterator = mReferences.listIterator();
				WeakReference<Cursor> weakReference;
				Cursor element;
				while (referenceListIterator.hasNext()) {
					weakReference = referenceListIterator.next();
					if (null != (element = weakReference.get()) && element.equals(cursor)) {
						return weakReference;
					}
				}
				return null;
			}
		}

		public boolean remove(Cursor cursor) {
			synchronized (this) {
				ListIterator<WeakReference<Cursor>> referenceListIterator = mReferences.listIterator();
				WeakReference<Cursor> weakReference;
				Object element;
				while (referenceListIterator.hasNext()) {
					weakReference = referenceListIterator.next();
					if (null != (element = weakReference.get()) && element.equals(cursor)) {
						return mReferences.remove(weakReference);
					}
				}
				return false;
			}
		}

		public boolean removeWeakReference(WeakReference<Cursor> cursorWeakReference) {
			synchronized (this) {
				return mReferences.remove(cursorWeakReference);
			}
		}

		public Cursor[] toArray() {
			synchronized (this) {
				processQueue();
				int size = mReferences.size();
				Cursor[] copy = new Cursor[size];
				int copyIndex = 0;
				for (WeakReference<Cursor> cursorWR : mReferences) {
					copy[copyIndex++] = cursorWR.get();
				}
				return copy;
			}
		}

		@SuppressWarnings("unchecked")
		private void processQueue() {
			Reference<Cursor> clearedReference;
			while (null != (clearedReference = (Reference<Cursor>) mReleasedQueue.poll())) {
				mReferences.remove(clearedReference);
			}
		}

		private final List<WeakReference<Cursor>> mReferences = new LinkedList<WeakReference<Cursor>>();
		private final ReferenceQueue<Cursor> mReleasedQueue = new ReferenceQueue<Cursor>();
	}
}
