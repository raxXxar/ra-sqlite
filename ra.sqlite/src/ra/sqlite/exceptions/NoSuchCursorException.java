package ra.sqlite.exceptions;

public class NoSuchCursorException extends RuntimeException {

	private static final long serialVersionUID = 6722371448063740354L;

	public NoSuchCursorException() {
	}

	public NoSuchCursorException(String error) {
		super(error);
	}
}
