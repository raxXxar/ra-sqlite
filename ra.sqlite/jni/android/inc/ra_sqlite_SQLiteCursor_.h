/*
 * ra_sqlite_SQLiteCursor_.h
 *
 *       Author: =ra=
 */

#ifndef RA_SQLITE_SQLITECURSOR__H_
#define RA_SQLITE_SQLITECURSOR__H_
#define _Included_ra_sqlite_SQLiteCursor

#include <jni.h>
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_recordset.h"
#include "ra_sqlite_SQLiteDatabase_.h"
#include "ra_hnd_map.h"
#include "ra_accessor.h"

#define CURSOR_ROWS_PER_EXTENT  100
#define CURSOR_BLOB_EXTENT_SIZE (1024 * 64) /* 64K */

#define CURSOR_ERROR_CODE_OK 				0
#define CURSOR_ERROR_CODE_UNKNOWN_TYPE 		1
#define CURSOR_ERROR_CODE_BLOB_TO_LONG 		2
#define CURSOR_ERROR_CODE_BLOB_TO_DOUBLE	3
#define CURSOR_ERROR_CODE_BLOB_TO_STRING	4
#define CURSOR_ERROR_CODE_INTEGER_TO_BLOB	5
#define CURSOR_ERROR_CODE_FLOAT_TO_BLOB		6

typedef struct Cursor {
	Dbh* pDb;
	sqlite3_stmt* pStatement;
	RecordSet* pRecordSet;
	jint fieldCount;
	jint cursorPosition;
	jint recordsetPosition;
	jint rowsPerExtent;
	jint blobExtentSize;
	jint rowsCount;
	Handle g_poolId;
	// thread safety
	__LMEMBER
	;
	Accessor* pAccessor;
} Cursor;

Cursor* Cursor_Create(Dbh* pHelper, sqlite3_stmt* pPreparedStatement, jint rowsPerExtent, jint blobExtentSize);

jint Cursor_Reset(Cursor* pCursor);

jint cursor_InnerReset(Cursor* pCursor);

jint Cursor_Deactivate(Cursor* pCursor);

void Cursor_Free(Cursor* pCursor);

jint Cursor_GetRowCount(Cursor* pCursor, jint* resultRowCount);

jint Cursor_GetColumnCount(Cursor* pCursor);

jint Cursor_GetColumnType(Cursor* pCursor, jint columnIndex);

jint Cursor_GetColumnIsNull(Cursor* pCursor, jint columnIndex);

jint Cursor_GetColumnInt64(Cursor* pCursor, jint columnIndex, jlong* resultValue);

jint Cursor_GetColumnDouble(Cursor* pCursor, jint columnIndex, jdouble* resultValue);

jint Cursor_GetColumnString(Cursor* pCursor, jint columnIndex, jstring* resultValue, JNIEnv* jEnv);

jint Cursor_GetColumnBlob(Cursor* pCursor, jint columnIndex, jbyteArray* resultValue, JNIEnv* jEnv);

/* bounds are checked by caller code */
jint Cursor_MoveToPosition(Cursor* pCursor, jint position, jint* resultPosition);

#endif /* RA_SQLITE_SQLITECURSOR__H_ */

