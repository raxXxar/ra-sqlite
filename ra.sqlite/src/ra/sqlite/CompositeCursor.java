package ra.sqlite;

import java.lang.reflect.Method;
import java.util.HashMap;
import ra.x.IndexAccessor;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteException;
import android.util.SparseArray;

public class CompositeCursor extends AbstractCursor {

	public CompositeCursor(CantleCursorInfo[] compositionInfo) {
		if (null == compositionInfo || null == compositionInfo[0]) {
			throw new IllegalArgumentException("Argument \"compositionInfo\" have to contain at least one item");
		}
		gatherCompositionData(compositionInfo);
	}

	@Override
	protected String internalGetColumnName(int columnIndex) {
		CantleCursorColumn columnInfo = columnsIndex.get(columnIndex);
		return (null == columnInfo) ? null : columnInfo.getNameWithAlias();
	}

	@Override
	protected String[] internalGetColumnNames() {
		String[] columnNames = new String[columnsIndex.size()];
		for (int i = 0; i < columnNames.length; ++i) {
			columnNames[i] = columnsIndex.valueAt(i).getNameWithAlias();
		}
		return columnNames;
	}

	@Override
	protected int internalGetColumnIndex(String columnName) {
		if (null == columnName) {
			throw new IllegalArgumentException("Column name shouldn't be null");
		}
		columnName = columnName.toLowerCase();
		CantleCursorColumn columnInfo = null;
		if (-1 == columnName.indexOf('.')) {
			columnInfo = distinctColumns.get(columnName);
		} else {
			columnInfo = allColumns.get(columnName);
		}
		return (null == columnInfo) ? -1 : columnInfo.getGlobalColumnIndex();
	}

	@Override
	protected int internalMoveTo(int position) {
		if (master.moveToPosition(position)) {
			localPosition = position;
		}
		return localPosition;
	}

	@Override
	protected int internalGetColumnCount() {
		return columnsIndex.size();
	}

	@Override
	protected Number internalGetInt64(int columnIndex) {
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null == cursorInfo) {
			throw new SQLiteException("type mismatch: Column value is null");
		}
		return cursorInfo.getCursor().getLong(columnIndex - cursorInfo.getStartGlobalColumnIndex());
	}

	@Override
	protected Number internalGetDouble(int columnIndex) {
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null == cursorInfo) {
			throw new SQLiteException("type mismatch: Column value is null");
		}
		return cursorInfo.getCursor().getDouble(columnIndex - cursorInfo.getStartGlobalColumnIndex());
	}

	@Override
	protected byte[] internalGetBlob(int columnIndex) {
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null == cursorInfo) {
			throw new SQLiteException("type mismatch: Column value is null");
		}
		return cursorInfo.getCursor().getBlob(columnIndex - cursorInfo.getStartGlobalColumnIndex());
	}

	@Override
	protected String internalGetString(int columnIndex) {
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null == cursorInfo) {
			throw new SQLiteException("type mismatch: Column value is null");
		}
		return cursorInfo.getCursor().getString(columnIndex - cursorInfo.getStartGlobalColumnIndex());
	}

	@Override
	protected boolean internalGetIsNull(int columnIndex) {
		boolean result = true;
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null != cursorInfo) {
			result = cursorInfo.getCursor().isNull(columnIndex - cursorInfo.getStartGlobalColumnIndex());
		}
		return result;
	}

	@Override
	protected int internalGetType(int columnIndex) {
		int result = 0; // type is  null
		CantleCursorInfo cursorInfo = prepare4Reading(columnIndex);
		if (null != cursorInfo) {
			Cursor cursor = cursorInfo.getCursor();
			Method methodGetType = null;
			try {
				// Cursor.getType(int columnIndex) was added in API level 11, so ...
				methodGetType = cursor.getClass().getMethod("getType");
			} catch (Exception e) {
				// doesn't matter
			}
			if (null != methodGetType) {
				try {
					result = ((Integer) methodGetType.invoke(cursor, columnIndex - cursorInfo.getStartGlobalColumnIndex())).intValue();
				} catch (Exception e) {
					// doesn't matter
				}
			}
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void internalDeactivate() {
		try {
			if (!internalIsClosed() && !deactivated) {
				for (CantleCursorInfo cantleCursorInfo : cursorInfo) {
					cantleCursorInfo.getCursor().deactivate();
				}
			}
		} finally {
			deactivated = true;
			resetCursorMembers();
			deactivateInternal();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void internalRequery() {
		if (deactivated) {
			try {
				for (CantleCursorInfo cantleCursorInfo : cursorInfo) {
					cantleCursorInfo.getCursor().requery();
				}
			} finally {
				resetCursorMembers();
				deactivated = false;
			}
			propagateDSNotifications = false;
			requeryInternal();
			propagateDSNotifications = true;
			notifyDataSetChange();
		} else {
			master.requery();
		}
	}

	@Override
	protected boolean internalIsClosed() {
		return master.isClosed();
	}

	@Override
	protected void internalClose() {
		propagateDSNotifications = false;
		for (CantleCursorInfo cantleCursorInfo : cursorInfo) {
			cantleCursorInfo.getCursor().close();
		}
	}

	@Override
	protected int internalRowsCount() {
		return master.getCount();
	}

	/**
	 * no tuples in Java ...
	 * return null if no corresponding data in cursor found
	 * return CantleCursorInfo with cursor positioned to corresponding row
	 */
	protected CantleCursorInfo prepare4Reading(int columnIndex) {
		CantleCursorInfo result = null;
		CantleCursorColumn columnInfo = columnsIndex.get(columnIndex);
		if (null != columnInfo) {
			result = columnInfo.getCantleCursorInfo();
			if (master != result.getCursor()) {
				// master.moveToPosition(localPosition); /* redundant operation */
				long indexValue = master.getLong(result.getMasterColumnIndex());
				if (IndexAccessor.POSITION_UNDEFINED == result.getAccessor().locate(indexValue)) {
					result = null;
				}
			}
		}
		return result;
	}

	protected void gatherCompositionData(CantleCursorInfo[] compositionInfo) {
		master = compositionInfo[0].getCursor();
		cursorInfo = compositionInfo;
		int nextIndex = 0;
		String alias;
		for (int idx = 0; idx < cursorInfo.length; idx++) {
			cursorInfo[idx].getCursor().registerDataSetObserver(cuntleCursorsObserver);
			String numericAlias = String.valueOf(idx);
			alias = cursorInfo[idx].getAlias(numericAlias);
			cursorInfo[idx].setUp(alias, nextIndex, master.getColumnIndex(cursorInfo[idx].getMasterColumn()));
			nextIndex = cursorInfo[idx].getLastGlobalColumnIndex() + 1;
			CantleCursorColumn[] columns = cursorInfo[idx].getColumnsInfo();
			String shortColumnName;
			for (int colIdx = 0; colIdx < columns.length; colIdx++) {
				allColumns.put(columns[colIdx].getNameWithAlias(), columns[colIdx]);
				allColumns.put(columns[colIdx].getNameWithGivenAlias(numericAlias), columns[colIdx]);
				columnsIndex.put(columns[colIdx].getGlobalColumnIndex(), columns[colIdx]);
				shortColumnName = columns[colIdx].getName();
				if (null == distinctColumns.get(shortColumnName)) {
					distinctColumns.put(shortColumnName, columns[colIdx]);
				}
			}
		}
	}

	protected final DataSetObserver cuntleCursorsObserver = new DataSetObserver() {

		public void onChanged() {
			if (propagateDSNotifications) {
				notifyDataSetChange();
			}
		}

		public void onInvalidated() {
			if (propagateDSNotifications) {
				close();
			}
		}
	};

	protected boolean propagateDSNotifications = true;
	protected Cursor master;
	protected final SparseArray<CantleCursorColumn> columnsIndex = new SparseArray<CantleCursorColumn>();
	protected final HashMap<String, CantleCursorColumn> allColumns = new HashMap<String, CantleCursorColumn>();
	protected final HashMap<String, CantleCursorColumn> distinctColumns = new HashMap<String, CantleCursorColumn>();
	protected CantleCursorInfo[] cursorInfo;
	protected final Object selfOperationsLock = new Object();
}
