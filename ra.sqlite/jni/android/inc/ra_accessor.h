/*
 * ra_accessor.h
 *
 *      Author: =ra=
 */

#ifndef RA_ACCESSOR_H_
#define RA_ACCESSOR_H_

#include <jni.h>
#include "ra_hnd_map.h"

#undef DEF_FAST_ACCESS_COLUMN_NAME
#define DEF_FAST_ACCESS_COLUMN_NAME "pk$join$column"

typedef struct Accessor Accessor;

Accessor* Accessor_Create(Handle cursorHandle,
                          const char* columnName,
                          jint columnPosition);

Accessor* Accessor_Free(Accessor* pAccessor);

void Accessor_Deactivate(Accessor* pAccessor);

jint Accessor_GeIndexColumnIndex(Accessor* pAccessor);

/**
 * return Accessor handle on success, ra_x_IndexAccessorRa_ACCESSOR_UNDEFINED otherwise
 */
jint Accessor_ConnectToCursor(Handle cursorHandle, const char* columnName);


/**
 * returns position in cursor for indexValue
 * returns ra_x_IndexAccessorRa_POSITION_UNDEFINED (-1) if no such indexValue in cursor
 */
jint Accessor_Locate(Accessor* pAccessor /* assume !NULL */, jlong indexValue);

#endif /* RA_ACCESSOR_H_ */
