package ra.dbengine.interfaces;

/**
 *  @author =ra=
 */
public interface SqlCommandSource {

	/**
	 * Gets the SQL statement - single statement.
	 *
	 * @return the SQL statement
	 */
	public String getSqlCommand();

	/**
	 * Gets the SQL statement block.
	 * 
	 * <br>Designed to perform more then one continuous DDL/DML or DDL+DML statements.
	 *
	 * @return the SQL statement block as String array
	 */
	public String[] getSqlCommandBlock();

	/**
	 * Gets the notification tags.
	 * <br>Usage of notification tags depends on type of SQL statement:
	 * <br>DDL/DML - tags used to notify interested object (tag listeners), (<b>DDL/DML statement is tag modifier</b>) 
	 * <br>Query - tags used to show that SqlCommandSource owner is interested on tag events (<b>Query statement is tag listener</b>) 
	 *
	 * @return the notification tags
	 */
	public String[] getNotificationTags();
}
