/*
 * ra_pool.h
 *      Author: =ra=
 */

#ifndef RA_POOL_H_
#define RA_POOL_H_

#include <jni.h>
#include "ra_sqlite_SQLiteCursor_.h"
#include "ra_sqlite_SQLiteDatabase_.h"

/**
 * initialize pool, should be used only in JNI_OnLoad() once time
 */

void initPool();

/**
 * free pool resources, redundant, but for a purity ...
 */
void destroyPool();

/**
 * register ptr in a global pool
 * return ptr handle in the pool;
 */
jint registerInPool(void* ptr);

void unregisterInPool(jint handle);

void* getInPool(jint handle);

/**
 * return  pointer associated with requested dbHandle, or throw NoSuchDatabaseException
 */
Dbh* getDbhPtrOrThrow(JNIEnv* jniEnv, jint dbHandle);

/**
 * return pointer associated with requested cursorHandle, or throw NoSuchCursorException
 */
Cursor* getCursorPtrOrThrow(JNIEnv* jniEnv, jint cursorHandle);

#endif /* RA_POOL_H_ */
