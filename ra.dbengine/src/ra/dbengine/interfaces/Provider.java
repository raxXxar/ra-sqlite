package ra.dbengine.interfaces;

/**
 *  @author =ra=
 */
public interface Provider<T> {

	public T get();
}
