package ra.sqlite;

import java.util.concurrent.ConcurrentHashMap;
import ra.sqlite.exceptions.NoSuchCursorException;
import ra.sqlite.exceptions.NoSuchDatabaseException;

public class SQLiteCursor extends AbstractCursor implements NativeCursor {

	SQLiteCursor(int databaseHandle, int statementHandle) {
		cursorHandle = createCursor(databaseHandle, statementHandle);
		resetCursorMembers();
		columnCount = columnCount(cursorHandle);
		for (int i = 0; i < columnCount; i++) {
			String colName = columnName(cursorHandle, i).toUpperCase();
			columnNames.put(colName, Integer.valueOf(i));
		}
	}

	@Override
	public int getHandle() {
		synchronized (selfOperationsLock) {
			return cursorHandle;
		}
	}

	@Override
	protected int internalGetType(int columnIndex) {
		return columnType(cursorHandle, columnIndex);
	}

	@Override
	protected String internalGetColumnName(int columnIndex) {
		if (columnIndex >= 0 && columnIndex < columnCount) {
			return columnName(cursorHandle, columnIndex).toUpperCase();
		} else {
			return null;
		}
	}

	@Override
	protected String[] internalGetColumnNames() {
		return columnNames.keySet().toArray(new String[] {});
	}

	@Override
	protected int internalGetColumnIndex(String columnName) {
		Integer columnIndex = columnNames.get(columnName.toUpperCase());
		if (null == columnIndex) {
			return UNDEFINED;
		}
		return columnIndex.intValue();
	}

	@Override
	protected int internalMoveTo(int position) {
		return moveTo(cursorHandle, position);
	}

	@Override
	protected int internalGetColumnCount() {
		return columnCount;
	}

	@Override
	protected Number internalGetInt64(int columnIndex) {
		return columnInt64Value(cursorHandle, columnIndex);
	}

	@Override
	protected Number internalGetDouble(int columnIndex) {
		return columnDoubleValue(cursorHandle, columnIndex);
	}

	@Override
	protected byte[] internalGetBlob(int columnIndex) {
		return columnByteBufferValue(cursorHandle, columnIndex);
	}

	@Override
	protected String internalGetString(int columnIndex) {
		return columnStringValue(cursorHandle, columnIndex);
	}

	@Override
	protected boolean internalGetIsNull(int columnIndex) {
		return columnIsNull(cursorHandle, columnIndex) == 1;
	}

	@Override
	protected void internalDeactivate() {
		try {
			if (!internalIsClosed() && !deactivated) {
				deactivateCursor(cursorHandle);
			}
		} finally {
			deactivated = true;
			resetCursorMembers();
			deactivateInternal();
		}
	}

	@Override
	protected void internalRequery() {
		try {
			deactivateCursor(cursorHandle);
		} finally {
			resetCursorMembers();
			deactivated = false;
		}
		requeryInternal();
	}

	@Override
	protected boolean internalIsClosed() {
		return cursorHandle == CURSOR_HANDLE_UNDEFINED;
	}

	@Override
	protected void internalClose() {
		try {
			if (cursorHandle != CURSOR_HANDLE_UNDEFINED) {
				destroyCursor(cursorHandle);
			}
		} finally {
			cursorHandle = CURSOR_HANDLE_UNDEFINED;
			columnNames.clear();
			resetCursorMembers();
			contentObservable.unregisterAll();
			deactivateInternal();
		}
	}

	@Override
	protected int internalRowsCount() {
		if (UNDEFINED == rowsCount && cursorHandle != CURSOR_HANDLE_UNDEFINED) {
			rowsCount = rowsCount(cursorHandle);
			inRow = rowsCount > 0;
		}
		return rowsCount;
	}

	@Override
	protected void finalize() {
		if (null != selfObserver && null != contentResolver && true == selfObserverRegistered) {
			contentResolver.unregisterContentObserver(selfObserver);
			selfObserverRegistered = false;
		}
		if (!internalIsClosed()) {
			try {
				destroyCursor(cursorHandle);
			} finally {
				cursorHandle = CURSOR_HANDLE_UNDEFINED;
			}
		}
	}

	native int moveTo(int cursorHandle, int position) throws NoSuchCursorException;

	native int columnCount(int cursorHandle) throws NoSuchCursorException;

	native int columnType(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native int columnIsNull(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native long columnInt64Value(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native double columnDoubleValue(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native String columnStringValue(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native byte[] columnByteBufferValue(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native String columnName(int cursorHandle, int columnIndex) throws NoSuchCursorException;

	native int rowsCount(int cursorHandle) throws NoSuchCursorException;

	native void deactivateCursor(int cursorHandle) throws NoSuchCursorException;

	native void resetCursor(int cursorHandle) throws NoSuchCursorException;

	native int createCursor(int databaseHandle, int statementHandle) throws NoSuchDatabaseException;

	native void destroyCursor(int cursorHandle) throws NoSuchCursorException;

	protected int cursorHandle;
	protected int columnCount = 0;
	protected final ConcurrentHashMap<String, Integer> columnNames = new ConcurrentHashMap<String, Integer>();

	private static final int CURSOR_HANDLE_UNDEFINED = 0;
	private static final String TAG = SQLiteCursor.class.getSimpleName();

}
