package ra.sqlite;

import java.lang.ref.WeakReference;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObservable;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

public abstract class AbstractCursor implements Cursor {

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getCount()
	 */
	@Override
	public int getCount() {
		synchronized (selfOperationsLock) {
			checkState();
			return internalRowsCount();
		}
	}

	/*
	 * (non-Javadoc
	 * @see android.database.Cursor#getPosition()
	 */
	@Override
	public int getPosition() {
		synchronized (selfOperationsLock) {
			checkState();
			return localPosition;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#move(int)
	 */
	@Override
	public boolean move(int offset) {
		return moveToPosition(localPosition + offset);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToPosition(int)
	 */
	@Override
	public boolean moveToPosition(int position) {
		synchronized (selfOperationsLock) {
			checkState();
			if (boundsIsOK(position)) {
				localPosition = internalMoveTo(position);
			}
			if (position < 0) {
				localPosition = UNDEFINED;
			} else {
				int internalRowsCount = internalRowsCount();
				if (position >= internalRowsCount) {
					localPosition = internalRowsCount;
				}
			}
			inRow = boundsIsOK(localPosition);
			return inRow;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToFirst()
	 */
	@Override
	public boolean moveToFirst() {
		return moveToPosition(0);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToLast()
	 */
	@Override
	public boolean moveToLast() {
		return moveToPosition(internalRowsCount() - 1);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToNext()
	 */
	@Override
	public boolean moveToNext() {
		return moveToPosition(localPosition + 1);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToPrevious()
	 */
	@Override
	public boolean moveToPrevious() {
		return moveToPosition(localPosition - 1);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isFirst()
	 */
	@Override
	public boolean isFirst() {
		synchronized (selfOperationsLock) {
			checkState();
			return 0 == localPosition && 0 != internalRowsCount();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isLast()
	 */
	@Override
	public boolean isLast() {
		synchronized (selfOperationsLock) {
			checkState();
			int rowsCount = internalRowsCount();
			return localPosition == (rowsCount - 1) && 0 != rowsCount;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isBeforeFirst()
	 */
	@Override
	public boolean isBeforeFirst() {
		synchronized (selfOperationsLock) {
			checkState();
			int rowsCount = internalRowsCount();
			if (0 == rowsCount) {
				return true;
			}
			return localPosition == UNDEFINED;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isAfterLast()
	 */
	@Override
	public boolean isAfterLast() {
		synchronized (selfOperationsLock) {
			checkState();
			int rowsCount = internalRowsCount();
			if (0 == rowsCount) {
				return true;
			}
			return localPosition >= rowsCount;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnIndex(java.lang.String)
	 */
	@Override
	public int getColumnIndex(String columnName) {
		synchronized (selfOperationsLock) {
			checkState();
			if (null == columnName)
				return UNDEFINED;
			else
				return internalGetColumnIndex(columnName);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnIndexOrThrow(java.lang.String)
	 */
	@SuppressLint("DefaultLocale")
	@Override
	public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
		synchronized (selfOperationsLock) {
			checkState();
			int colIndex = (null == columnName) ? UNDEFINED : internalGetColumnIndex(columnName.toUpperCase());
			if (UNDEFINED == colIndex) {
				throw new IllegalArgumentException();
			}
			return colIndex;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnName(int)
	 */
	@Override
	public String getColumnName(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkState();
			return internalGetColumnName(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnNames()
	 */
	@Override
	public String[] getColumnNames() {
		synchronized (selfOperationsLock) {
			checkState();
			return internalGetColumnNames();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		synchronized (selfOperationsLock) {
			return internalGetColumnCount();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getBlob(int)
	 */
	@Override
	public byte[] getBlob(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetBlob(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getString(int)
	 */
	@Override
	public String getString(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetString(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#copyStringToBuffer(int, android.database.CharArrayBuffer)
	 */
	@Override
	public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
		// Default implementation, uses getString
		String result = getString(columnIndex);
		if (result != null) {
			char[] data = buffer.data;
			if (data == null || data.length < result.length()) {
				buffer.data = result.toCharArray();
			} else {
				result.getChars(0, result.length(), data, 0);
			}
			buffer.sizeCopied = result.length();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getShort(int)
	 */
	@Override
	public short getShort(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetInt64(columnIndex).shortValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getInt(int)
	 */
	@Override
	public int getInt(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetInt64(columnIndex).intValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getLong(int)
	 */
	@Override
	public long getLong(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetInt64(columnIndex).longValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getFloat(int)
	 */
	@Override
	public float getFloat(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetDouble(columnIndex).floatValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getDouble(int)
	 */
	@Override
	public double getDouble(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetDouble(columnIndex).doubleValue();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isNull(int)
	 */
	@Override
	public boolean isNull(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetIsNull(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#deactivate()
	 */
	@Override
	public void deactivate() {
		synchronized (selfOperationsLock) {
			internalDeactivate();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#requery()
	 */
	@Override
	public boolean requery() {
		synchronized (selfOperationsLock) {
			if (internalIsClosed()) {
				return false;
			} else {
				internalRequery();
				return true;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#close()
	 */
	@Override
	public void close() {
		synchronized (selfOperationsLock) {
			internalClose();
			notifyInvalidate();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isClosed()
	 */
	@Override
	public boolean isClosed() {
		synchronized (selfOperationsLock) {
			return internalIsClosed();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#registerContentObserver(android.database.ContentObserver)
	 */
	@Override
	public void registerContentObserver(ContentObserver observer) {
		contentObservable.registerObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#unregisterContentObserver(android.database.ContentObserver)
	 */
	@Override
	public void unregisterContentObserver(ContentObserver observer) {
		if (!internalIsClosed()/* !isClosed() */) {
			contentObservable.unregisterObserver(observer);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#registerDataSetObserver(android.database.DataSetObserver)
	 */
	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		dataSetObservable.registerObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#unregisterDataSetObserver(android.database.DataSetObserver)
	 */
	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		dataSetObservable.unregisterObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#setNotificationUri(android.content.ContentResolver, android.net.Uri)
	 */
	@Override
	public void setNotificationUri(ContentResolver contentResolver, Uri uri) {
		synchronized (selfObserverLock) {
			notifyUri = uri;
			if (selfObserver != null) {
				if (null != this.contentResolver) {
					this.contentResolver.unregisterContentObserver(selfObserver);
					selfObserverRegistered = false;
				}
			} else {
				selfObserver = new SelfContentObserver(this);
			}
			if (null != contentResolver) {
				contentResolver.registerContentObserver(uri, true, selfObserver);
				selfObserverRegistered = true;
			}
			this.contentResolver = contentResolver;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getWantsAllOnMoveCalls()
	 */
	@Override
	public boolean getWantsAllOnMoveCalls() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getExtras()
	 */
	@Override
	public Bundle getExtras() {
		return Bundle.EMPTY;
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#respond(android.os.Bundle)
	 */
	@Override
	public Bundle respond(Bundle extras) {
		return Bundle.EMPTY;
	}

	public int getType(int columnIndex) {
		synchronized (selfOperationsLock) {
			checkPosition();
			return internalGetType(columnIndex);
		}
	}

	protected void checkState() {
		if (internalIsClosed() /* isClosed() */) {
			throw new IllegalStateException("Cursor is Closed");
		} else if (deactivated) {
			throw new IllegalStateException("Cursor is Deactivated");
		}

	}

	protected void resetCursorMembers() {
		rowsCount = UNDEFINED;
		localPosition = UNDEFINED;
		inRow = false;
	}

	protected abstract String internalGetColumnName(int columnIndex);

	protected abstract String[] internalGetColumnNames();

	protected abstract int internalGetColumnIndex(String columnName);

	protected abstract int internalMoveTo(int position);

	protected abstract int internalGetColumnCount();

	protected abstract Number internalGetInt64(int columnIndex);

	protected abstract Number internalGetDouble(int columnIndex);

	protected abstract byte[] internalGetBlob(int columnIndex);

	protected abstract String internalGetString(int columnIndex);

	protected abstract boolean internalGetIsNull(int columnIndex);

	protected abstract int internalGetType(int columnIndex);

	protected abstract void internalDeactivate();

	protected abstract void internalRequery();

	protected abstract boolean internalIsClosed();

	protected abstract void internalClose();

	protected abstract int internalRowsCount();

	protected void notifyDataSetChange() {
		dataSetObservable.notifyChanged();
	}

	protected void notifyInvalidate() {
		dataSetObservable.notifyInvalidated();
	}

	protected void checkPosition() {
		if (!inRow) {
			throw new CursorIndexOutOfBoundsException(localPosition, internalRowsCount());
		}
	}

	protected void deactivateInternal() {
		if (null != selfObserver && null != contentResolver && true == selfObserverRegistered) {
			contentResolver.unregisterContentObserver(selfObserver);
			selfObserverRegistered = false;
		}
	}

	protected void requeryInternal() {
		if (null != selfObserver && null != contentResolver && false == selfObserverRegistered) {
			contentResolver.registerContentObserver(notifyUri, true, selfObserver);
			selfObserverRegistered = true;
		}
		notifyDataSetChange();
	}

	@SuppressWarnings("deprecation")
	protected void onChange(boolean selfChange) {
		synchronized (selfObserverLock) {
			contentObservable.dispatchChange(selfChange);
			if (null != notifyUri && null != contentResolver && selfChange) {
				contentResolver.notifyChange(notifyUri, selfObserver);
			}
		}
	}

	protected boolean boundsIsOK(int position) {
		return ((position >= 0) && (position < internalRowsCount()));
	}

	protected boolean inRow = false;
	protected boolean deactivated = false;
	protected int rowsCount;
	protected int localPosition = UNDEFINED;

	protected ContentObserver selfObserver;
	protected Uri notifyUri;
	protected ContentResolver contentResolver;
	protected boolean selfObserverRegistered;

	protected final DataSetObservable dataSetObservable = new DataSetObservable();
	protected final ContentObservable contentObservable = new ContentObservable();
	protected final Object selfObserverLock = new Object();
	protected final Object selfOperationsLock = new Object();

	/**
	 * Cursors use this class to track changes others make to their URI.
	 */
	protected static class SelfContentObserver extends ContentObserver {

		WeakReference<AbstractCursor> cursorRef;

		public SelfContentObserver(AbstractCursor abstractCursor) {
			super(null);
			cursorRef = new WeakReference<AbstractCursor>(abstractCursor);
		}

		@Override
		public boolean deliverSelfNotifications() {
			return false;
		}

		@Override
		public void onChange(boolean selfChange) {
			AbstractCursor cursor = cursorRef.get();
			if (null != cursor) {
				cursor.onChange(false);
			}
		}
	}

	protected static final int UNDEFINED = -1;
}
