package ra.dbengine.exceptions;

/**
 *  @author =ra=
 */
public class DbOpenErrorException extends DbEngineProviderException {

	private static final long serialVersionUID = -3465540844119362825L;

	public DbOpenErrorException() {
		super("Can not open database");
	}
}