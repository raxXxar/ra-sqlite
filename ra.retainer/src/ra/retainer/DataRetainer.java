package ra.retainer;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Bundle;

public class DataRetainer {

	protected DataRetainer() {
	}

	public static void retain(Object objectToRetain, Bundle toBundle) {
		if (null != toBundle && null != objectToRetain) {
			toBundle.putString(CLASS_NAME_TAG, objectToRetain.getClass().getCanonicalName());
			for (Field field : getRetainFields(objectToRetain))
				BundleUtils.storeIntoBundle(objectToRetain, field, toBundle);
		}
	}

	public static Object retain(Object objectToRetain) {
		DataRetainer retainer = null;
		if (null != objectToRetain) {
			retainer = new DataRetainer();
			retainer.mRetaineeType = objectToRetain.getClass();
			for (Field field : getRetainFields(objectToRetain))
				try {
					boolean inAcessible = !field.isAccessible();
					if (inAcessible)
						field.setAccessible(true);
					retainer.mRetainData.put(field.getName(), field.get(objectToRetain));
					if (inAcessible)
						field.setAccessible(false);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
		}
		return retainer;
	}

	public static boolean restore(Object objectToRestore, Bundle fromBundle, Object fromNonConfigurationInstance) {
		boolean result;
		if (null != fromNonConfigurationInstance)
			result = restoreFromNonConfigurationInstance(objectToRestore, fromNonConfigurationInstance);
		else if (null != fromBundle)
			result = restoreFromBundle(objectToRestore, fromBundle);
		else
			result = false;
		return result;
	}

	protected static boolean restoreFromBundle(Object objectToRestore, Bundle fromBundle) {
		boolean result;
		String canonicalName = fromBundle.getString(CLASS_NAME_TAG);
		if (objectToRestore.getClass().getCanonicalName().equals(canonicalName)) {
			for (Field field : getRetainFields(objectToRestore))
				ClassUtils.changeObjectValue(objectToRestore, field, fromBundle.get(field.getName()));
			result = true;
		} else
			result = false;
		return result;
	}

	protected static boolean restoreFromNonConfigurationInstance(Object objectToRestore, Object fromNonConfigurationInstance) {
		boolean result = false;
		if (fromNonConfigurationInstance instanceof DataRetainer) {
			DataRetainer dataRetainer = (DataRetainer) fromNonConfigurationInstance;
			if (!dataRetainer.mRetaineeType.equals(objectToRestore.getClass()))
				throw new IllegalArgumentException("incompatible type of argument");
			for (Field field : getRetainFields(objectToRestore)) {
				String fieldName = field.getName();
				Object retainedValue = dataRetainer.mRetainData.get(fieldName);
				if (null != retainedValue)
					ClassUtils.changeObjectValue(objectToRestore, fieldName, retainedValue);
			}
			result = true;
		}
		return result;
	}

	protected static Field[] getRetainFields(Object targetObject) {
		Field[] result;
		if (null == targetObject)
			result = new Field[0];
		else
			result = ClassUtils.getAnnotatedDeclaredFields(targetObject.getClass(), Retain.class, true);
		return result;
	}

	protected final ConcurrentHashMap<String, Object> mRetainData = new ConcurrentHashMap<String, Object>(MAP_INITIAL_CAPACITY);
	protected Class<?> mRetaineeType;
	protected static final int MAP_INITIAL_CAPACITY = 2;
	protected static final String CLASS_NAME_TAG = "CanonicalName";
}
