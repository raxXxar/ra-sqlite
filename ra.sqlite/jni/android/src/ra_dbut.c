/*
 * ra_dbut.c
 *
 *      Author: =ra=
 */
#include <jni.h>
#include <stddef.h>
#include <string.h>
#include "sqlite3.h"
#include "ra_sqlite.h"
#include "ra_exception.h"
#include "ra_sqlite_SQLiteDatabase_.h"

jobjectArray getStatementColumnNames(JNIEnv* jniEnv, sqlite3_stmt* handle) {
	jint stmtColumnCount = sqlite3_column_count(handle);
	jobjectArray strArray = (*jniEnv)->NewObjectArray(jniEnv,
	                                                  stmtColumnCount,
	                                                  jClassString,
	                                                  0);
	static jsize i;
	for (i = 0; i < stmtColumnCount; i++) {
		const char* name = sqlite3_column_name(handle, i);
		jstring nameString = (*jniEnv)->NewStringUTF(jniEnv, name);
		(*jniEnv)->SetObjectArrayElement(jniEnv, strArray, i, nameString);
		(*jniEnv)->DeleteLocalRef(jniEnv, nameString);
	}
	return strArray;
}

jstring getStatementColumnName(JNIEnv* jniEnv,
                               sqlite3_stmt* handle,
                               jint columnIndex) {
	const char* name = sqlite3_column_name(handle, columnIndex);
	jstring nameString = (*jniEnv)->NewStringUTF(jniEnv, name);
	return nameString;
}

jint getStatementColumnIndexCS(sqlite3_stmt* handle, const char* columnName) {
	jint result = -1;
	const char* idxColName;
	jint idx = sqlite3_column_count(handle) - 1;
	while (idx >= 0) {
		idxColName = sqlite3_column_name(handle, idx);
		if (NULL != idxColName && 0 == strcasecmp(idxColName, columnName)) {
			result = idx;
			break;
		}
		idx--;
	}
	return result;
}

void resetStatement(JNIEnv* jniEnv, sqlite3_stmt* handle) {
	jint errcode = sqlite3_reset(handle);
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

jint prepareStatement(JNIEnv* jniEnv, Dbh* pDbh, jstring strSql) {
	sqlite3* sqliteDbHandle = pDbh->pSqliteDb;
	char const *sqlStr = (*jniEnv)->GetStringUTFChars(jniEnv, strSql, 0);
	sqlite3_stmt* stmtHandle;
	jint errcode = sqlite3_prepare_v2(sqliteDbHandle,
	                                  sqlStr,
	                                  -1,
	                                  &stmtHandle,
	                                  0);
	THROW_SQL3(jniEnv, errcode, sqliteDbHandle);
	if (sqlStr != 0) {
		(*jniEnv)->ReleaseStringUTFChars(jniEnv, strSql, sqlStr);
	}
	Array_Insert(pDbh->pStatements, stmtHandle);

	return (jint) stmtHandle;
}

void finalizeStatement(Dbh* pDbh, sqlite3_stmt* handle) {
	Array_RemoveEntry(pDbh->pStatements, handle, JNI_TRUE);
}

jint getStatementArgsCount(sqlite3_stmt* handle) {
	return (jint) sqlite3_bind_parameter_count(handle);
}

jint execStatement(JNIEnv* jniEnv, sqlite3_stmt* handle) {
	jint affectedRows = 0;
	jint errcode = SQLITE_OK;
	errcode = sqlite3_step(handle);
	switch (errcode) {
		case SQLITE_OK:
		case SQLITE_ROW:
		case SQLITE_DONE:
			affectedRows = sqlite3_changes(sqlite3_db_handle(handle));
			break;
		default:
			throw_sqlite3_exception(jniEnv, sqlite3_db_handle(handle), errcode);
			break;
	}
	return affectedRows;
}

jlong columnInt64Value(sqlite3_stmt* handle, jint columnIndex) {
	jint valueType = sqlite3_column_type(handle, columnIndex);
	if (SQLITE_NULL == valueType) {
		return 0;
	}
	return (jlong) sqlite3_column_int64(handle, columnIndex);
}

jstring columnStringValue(JNIEnv* jniEnv,
                          sqlite3_stmt* handle,
                          jint columnIndex) {
	const char* str = sqlite3_column_text(handle, columnIndex);
	if (NULL == str) {
		return (*jniEnv)->NewStringUTF(jniEnv, "");
	} else {
		return (*jniEnv)->NewStringUTF(jniEnv, str);
	}
}

void statementBindNull(JNIEnv* jniEnv, sqlite3_stmt* handle, jint index) {
	jint errcode = sqlite3_bind_null(handle, index);
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

void statementBindDouble(JNIEnv* jniEnv,
                         sqlite3_stmt* handle,
                         jint index,
                         jdouble valueDouble) {
	jint errcode = sqlite3_bind_double(handle, index, valueDouble);
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

void statementBindLong(JNIEnv* jniEnv,
                       sqlite3_stmt* handle,
                       jint index,
                       jlong valueLong) {
	jint errcode = sqlite3_bind_int64(handle, index, valueLong);
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

void statementBindString(JNIEnv* jniEnv,
                         sqlite3_stmt* handle,
                         jint index,
                         jstring valueString) {
	char const *valueStr = (*jniEnv)->GetStringUTFChars(jniEnv, valueString, 0);
	jint errcode = sqlite3_bind_text(handle,
	                                 index,
	                                 valueStr,
	                                 -1,
	                                 SQLITE_TRANSIENT );
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
	if (NULL != valueStr) {
		(*jniEnv)->ReleaseStringUTFChars(jniEnv, valueString, valueStr);
	}
}

void statementBindByteArray(JNIEnv* jniEnv,
                            sqlite3_stmt* handle,
                            jint index,
                            jbyteArray valueByteArray) {

	const jbyte* buf = (*jniEnv)->GetByteArrayElements(jniEnv,
	                                                   valueByteArray,
	                                                   0);
	jint length = (*jniEnv)->GetArrayLength(jniEnv, valueByteArray);
	jint errcode = sqlite3_bind_blob(handle,
	                                 index,
	                                 buf,
	                                 length,
	                                 SQLITE_STATIC );
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

void statementBindByteBuffer(JNIEnv* jniEnv,
                             sqlite3_stmt* handle,
                             jint index,
                             jobject valueByteBuffer) {
	const void* buf = (*jniEnv)->GetDirectBufferAddress(jniEnv,
	                                                    valueByteBuffer);
	jint length = (*jniEnv)->GetDirectBufferCapacity(jniEnv, valueByteBuffer);
	jint errcode = sqlite3_bind_blob(handle,
	                                 index,
	                                 buf,
	                                 length,
	                                 SQLITE_STATIC );
	THROW_SQL3(jniEnv, errcode, sqlite3_db_handle(handle));
}

