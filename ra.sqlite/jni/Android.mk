LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

subdirs := $(addprefix $(LOCAL_PATH)/, $(addsuffix /icuSqlite.mk, sqlite)) 
subdirs += $(addprefix $(LOCAL_PATH)/, $(addsuffix /raSqlite.mk, android)) 

include $(subdirs)
