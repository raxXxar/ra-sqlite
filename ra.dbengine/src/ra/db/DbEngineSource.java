package ra.db;

import ra.dbengine.DbEngineImpl;
import ra.dbengine.interfaces.DbEngine;

/**
 * A factory for creating concrete DbEngine.
 */
/** 
 * @author =ra=
 */
public class DbEngineSource {

	/**
	 * Gets the work database.
	 *
	 * @param context the context
	 * @return the database engine
	 */
	public static DbEngine getDbEngine(DbConfig dbCfg) {
		return new DbEngineImpl(dbCfg);
	}
}
