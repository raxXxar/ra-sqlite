package ra.dbengine.processors;

import ra.dbengine.QProcessor;
import ra.dbengine.interfaces.QProcessorProvider;

/**
 *  @author =ra=
 */
public class QAndroidSQLiteProcessorProvider implements QProcessorProvider {

	@Override
	public QProcessor get() {
		return new QAndroidSQLiteProcessorImpl();
	}

}