/*
 * ra_sqlite_SQLiteDatabase_.h
 *
 *       Author: =ra=
 */

#ifndef RA_SQLITE_SQLITEDATABASE__H_
#define RA_SQLITE_SQLITEDATABASE__H_

#include <jni.h>
#include <unicode/ucol.h>
#include "ra_sqlite.h"
#include "ra_dynarr.h"
#include "thread_lock.h"
#include "ra_hnd_map.h"

#define CURSORS_INITIAL_CAPACITY 	(2 << 5)
#define CURSORS_GROW_SIZE 			(2 << 4)

typedef struct Dbh {
	sqlite3* pSqliteDb;
	Array* pCursors;
	Array* pStatements;
	Handle g_poolId;
	UCollator* pDefCollator;
	// thread safety
	__LMEMBER;
} Dbh;

#endif /* RA_SQLITE_SQLITEDATABASE__H_ */
