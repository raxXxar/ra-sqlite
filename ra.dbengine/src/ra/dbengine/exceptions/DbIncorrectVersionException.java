package ra.dbengine.exceptions;

/**
 *  @author =ra=
 */
public class DbIncorrectVersionException extends DbEngineProviderException {

	private static final long serialVersionUID = 199784015142376927L;

	public DbIncorrectVersionException() {
		super("Incorrect database version");
	}
}