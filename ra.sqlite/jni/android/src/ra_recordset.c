#include <malloc.h>
#include <string.h>
#include <jni.h>
#include "sqlite3.h"
#include "ra_sqlite_SQLiteCursor.h"
#include "ra_recordset.h"
#include "ra_extent.h"
#include "ra_list.h"

static void __reallocRowExtents(RecordSet* pRecordSet, jint rowCount);

static void* __getBuffer(RecordSet* pRecordSet, jint size);

static void __Field_PutData(jint type, RecordSet* pRecordSet, Field* pField, const void* data, jint size);

RecordSet* RecordSet_Create(jint fieldCount, jint rowsPerExtent, jint blobExtentSize) {
	RecordSet* pRecordSet = (RecordSet*) malloc(sizeof(RecordSet));
	pRecordSet->rowExtents = NULL;
	pRecordSet->totalRowCount = ra_sqlite_SQLiteCursor_UNDEFINED;
	pRecordSet->rowExtentsCapacity = 0;
	pRecordSet->rowsPerExtent = rowsPerExtent;
	pRecordSet->fieldCount = fieldCount;
	pRecordSet->rowSize = fieldCount * sizeof(Field);
	pRecordSet->blobExtentSize = blobExtentSize;
	pRecordSet->blobData = List_Create((FnListRecordDestructor) &Extent_Free);
	pRecordSet->currentBlob = NULL;
	__reallocRowExtents(pRecordSet, pRecordSet->rowsPerExtent);
	return pRecordSet;
}

void RecordSet_SetRowsCount(RecordSet* pRecordSet, jint rowCount) {
	pRecordSet->totalRowCount = rowCount;
	__reallocRowExtents(pRecordSet, rowCount);
}

void RecordSet_Reset(RecordSet* pRecordSet) {
	if (NULL != pRecordSet->rowExtents) {
		jint idx;
		for (idx = 0; idx < pRecordSet->rowExtentsCapacity; ++idx) {
			if (NULL != pRecordSet->rowExtents[idx]) {
				Extent_Free(pRecordSet->rowExtents[idx]);
				pRecordSet->rowExtents[idx] = NULL;
			}
		}
	}
	Extent* pExtent;
	while (NULL != (pExtent = (Extent*) List_Pop(pRecordSet->blobData))) {
		Extent_Free(pExtent);
	}
	pRecordSet->totalRowCount = ra_sqlite_SQLiteCursor_UNDEFINED;
	pRecordSet->currentBlob = NULL;
}

void RecordSet_Free(RecordSet* pRecordSet) {
	if (NULL != pRecordSet->rowExtents) {
		jint idx;

		for (idx = 0; idx < pRecordSet->rowExtentsCapacity; ++idx) {
			if (NULL != pRecordSet->rowExtents[idx]) {
				Extent_Free(pRecordSet->rowExtents[idx]);
				pRecordSet->rowExtents[idx] = NULL;
			}
		}
	}
	free(pRecordSet->rowExtents);
	free(pRecordSet->blobData);
	free(pRecordSet);
}

Field* RecordSet_GetField(RecordSet* pRecordSet, jint row, jint col) {
	static jint sizeOfField = sizeof(Field);
	jint rowExtentIdx = row / pRecordSet->rowsPerExtent;
	if (NULL == pRecordSet->rowExtents) {
		__reallocRowExtents(pRecordSet, pRecordSet->rowsPerExtent);
	}
	Extent* pExtent = pRecordSet->rowExtents[rowExtentIdx];
	if (NULL == pExtent) {
		pExtent = Extent_Create(sizeOfField * pRecordSet->fieldCount * pRecordSet->rowsPerExtent);
		pRecordSet->rowExtents[rowExtentIdx] = pExtent;
	}
	return Extent_GetMemPtr(pExtent) + sizeOfField * (pRecordSet->fieldCount * (row % pRecordSet->rowsPerExtent) + col);
}

jlong RecordSet_GetInt64(Field* pField) {
	return pField->value.l;
}

jdouble RecordSet_GetDouble(Field* pField) {
	return pField->value.d;
}

jstring RecordSet_GetString(Field* pField, JNIEnv* jniEnv) {
	jstring result =
	    (NULL != pField->value.ptr.data) ? (*jniEnv)->NewStringUTF(jniEnv, (char*) pField->value.ptr.data) :
	                                       (*jniEnv)->NewStringUTF(jniEnv, "");
	return result;
}

jobject RecordSet_GetBlob(Field* pField, JNIEnv* jniEnv) {
	jbyteArray result = NULL;
	if ((NULL != pField->value.ptr.data) && (pField->value.ptr.size > 0)) {
		result = (*jniEnv)->NewByteArray(jniEnv, pField->value.ptr.size);
		(*jniEnv)->SetByteArrayRegion(jniEnv,
		                              result,
		                              0,
		                              pField->value.ptr.size,
		                              (const jbyte*) (pField->value.ptr.data));
	}
	return result;

}

jint RecordSet_GetType(Field* pField) {
	return pField->type;
}

void RecordSet_PutInt64(Field* pField, jlong value) {
	pField->type = SQLITE_INTEGER;
	pField->value.l = value;
}

void RecordSet_PutDouble(Field* pField, jdouble value) {
	pField->type = SQLITE_FLOAT;
	pField->value.d = value;
}

void RecordSet_PutString(RecordSet* pRecordSet, Field* pField, const char* str, jint size) {
	__Field_PutData(SQLITE_TEXT, pRecordSet, pField, str, size);
}

void RecordSet_PutBlob(RecordSet* pRecordSet, Field* pField, const void* data, jint size) {
	__Field_PutData(SQLITE_BLOB, pRecordSet, pField, data, size);
}

void RecordSet_PutType(Field* pField, jint type) {
	pField->type = type;
}

void RecordSet_GetReadScope(RecordSet* pRecordSet, jint position, jint* start, jint* stop) {
	jint rowExtentIdx = position / pRecordSet->rowsPerExtent;
	*start = rowExtentIdx * pRecordSet->rowsPerExtent;
	jint endPos = *start + pRecordSet->rowsPerExtent;
	*stop = (endPos > pRecordSet->totalRowCount) ? pRecordSet->totalRowCount : endPos;
}

jint RecordSet_IsRead(RecordSet* pRecordSet, jint position) {
	return (NULL != pRecordSet->rowExtents[position / pRecordSet->rowsPerExtent]);
}

static void __reallocRowExtents(RecordSet* pRecordSet, jint rowCount) {
	jint newRowExtendsCapacity = rowCount / pRecordSet->rowsPerExtent + !!(rowCount % pRecordSet->rowsPerExtent);
	if (newRowExtendsCapacity < pRecordSet->rowExtentsCapacity) {
		int idx;
		for (idx = newRowExtendsCapacity; idx < pRecordSet->rowExtentsCapacity; ++idx) {
			if (NULL != pRecordSet->rowExtents[idx]) {
				Extent_Free(pRecordSet->rowExtents[idx]);
				pRecordSet->rowExtents[idx] = NULL;
			}
		}
	}
	static jint sizeOfPExtent = sizeof(Extent*);
	if (newRowExtendsCapacity != pRecordSet->rowExtentsCapacity) {
		pRecordSet->rowExtents = realloc(pRecordSet->rowExtents, newRowExtendsCapacity * sizeOfPExtent);
	}
	if (newRowExtendsCapacity > pRecordSet->rowExtentsCapacity) {
		memset(pRecordSet->rowExtents + pRecordSet->rowExtentsCapacity,
		       0,
		       (newRowExtendsCapacity - pRecordSet->rowExtentsCapacity) * sizeOfPExtent);
	}
	pRecordSet->rowExtentsCapacity = newRowExtendsCapacity;
}

static void* __getBuffer(RecordSet* pRecordSet, jint size) {
	if ((NULL == pRecordSet->currentBlob) || (size > Extent_GetFreeSize(pRecordSet->currentBlob))) {
		pRecordSet->currentBlob = Extent_Create(
		    (size > pRecordSet->blobExtentSize) ? size : pRecordSet->blobExtentSize);
		List_Push(pRecordSet->blobData, pRecordSet->currentBlob);
	}
	return Extent_CallocMem(pRecordSet->currentBlob, size);
}

static void __Field_PutData(jint type, RecordSet* pRecordSet, Field* pField, const void* data, jint size) {
	pField->type = type;
	if (size > pField->value.ptr.size) {
		pField->value.ptr.data = __getBuffer(pRecordSet, size);
	}
	pField->value.ptr.size = size;
	memcpy(pField->value.ptr.data, data, size);
}
