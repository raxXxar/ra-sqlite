package ra.sqlite.exceptions;

public class NoSuchDatabaseException extends RuntimeException {

	private static final long serialVersionUID = -5430832038413106188L;

	public NoSuchDatabaseException() {
	}

	public NoSuchDatabaseException(String error) {
		super(error);
	}
}
