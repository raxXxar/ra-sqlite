#ifndef __RA_SQLITE_H__
#define __RA_SQLITE_H__

#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include "sqlite3.h"

#define LOG_TAG "ra_sqlite"

#ifdef ANDROID
#  define LOGI(...) \
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#  define LOGE(...) \
        __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#else
#  define QUOTEME_(x) #x
#  define QUOTEME(x) QUOTEME_(x)
#  define LOGI(...) \
        printf("I/" LOG_TAG \
        " (" __FILE__ ":" QUOTEME(__LINE__) "): " __VA_ARGS__)
#  define LOGE(...) printf("E/" LOG_TAG "(" ")" __VA_ARGS__)
#endif

#define __LI(...) {char buf[1024]; sprintf(buf, __VA_ARGS__); \
	LOGI("%s: %s", __FUNCTION__, buf);}
#define __LE(...) {char buf[1024]; sprintf(buf, __VA_ARGS__); \
	LOGE("%s: %s", __FUNCTION__, buf);}

#define CLASS_PATH_STRING "java/lang/String"
extern jclass jClassString;

#define CLASS_PATH_SQL_EXCEPTION "android/database/SQLException"
extern jclass jClassSQLException;

#define CLASS_PATH_NO_SUCH_CURSOR_SQL_EXCEPTION \
	"ra/sqlite/exceptions/NoSuchCursorException"
extern jclass jClassNoSuchCursorException;

#define CLASS_PATH_NO_SUCH_DATABASE_EXCEPTION \
	"ra/sqlite/exceptions/NoSuchDatabaseException"
extern jclass jClassNoSuchDatabaseException;

#endif /*__RA_SQLITE_H__*/
