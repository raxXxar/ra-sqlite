/*
 * ra_x_IndexAccessorRa.c
 *
 *      Author: =ra=
 */

#include <jni.h>
#include <malloc.h>
#include "ra_x_IndexAccessorRa.h"
#include "ra_hnd_map.h"
#include "ra_accessor.h"
#include "ra_sqlite.h"
/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Deactivate
 * Signature: (I)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Deactivate(JNIEnv* jniEnv, jobject accessor, jint accessorId) {
	Accessor* pAccessor = (Accessor*) getInPool(accessorId);
	if (NULL != pAccessor) {
		Accessor_Deactivate(pAccessor);
	}
	return accessorId;
}

/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Init
 * Signature: (ILjava/lang/String;)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Init(JNIEnv* jniEnv,
                                                      jobject accessor,
                                                      jint cursorHandle,
                                                      jstring fastAccessColumnName) {
	char const *columnName = (*jniEnv)->GetStringUTFChars(jniEnv, fastAccessColumnName, 0);
	jint result = Accessor_ConnectToCursor(cursorHandle, columnName);
	if (NULL != columnName) {
		(*jniEnv)->ReleaseStringUTFChars(jniEnv, fastAccessColumnName, columnName);
	}
	return result;
}

/*
 * Class:     ra_x_IndexAccessorRa
 * Method:    Locate
 * Signature: (IJ)I
 */
//
JNIEXPORT jint JNICALL Java_ra_x_IndexAccessorRa_Locate(JNIEnv* jniEnv,
                                                        jobject accessor,
                                                        jint accessorId,
                                                        jlong indexValue) {
	jint result = ra_x_IndexAccessorRa_POSITION_UNDEFINED;
	Accessor* pAccessor = (Accessor*) getInPool(accessorId);
	if (NULL != pAccessor) {
		result = Accessor_Locate(pAccessor, indexValue);
	}
	return result;
}
