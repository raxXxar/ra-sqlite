package ra.dbengine;

import java.util.UUID;
import ra.dbengine.interfaces.ChangeListener;
import ra.dbengine.interfaces.CursorTechHole;
import ra.dbengine.utils.NullCursor;
import ra.dbengine.utils.WeakRefList;
import ra.sqlite.NativeCursor;
import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

/**
 * The Class CursorProxy is used as a real (<i>SQLiteCursor</i>) cursor wrapper in DbEngine.
 * <br>Class allows transparently change real cursor with another one with different 
 * SQL query (but with the same columns count, order and type affinity),
 * or with real cursor obtained from another Data Source.
 * 
 * <br>CursorProxy doesn't need to be managed explicitly.
 * <br>CursorProxy closes and free used by real cursor resources if it becomes out of scope.
 * 
 * <br><b>Do not create this class explicitly !!!</b> 
 */
/**
 *  @author =ra=
 */
class CursorProxy implements Cursor, NativeCursor, CursorTechHole, ChangeListener {

	/**
	 * Instantiates a new cursor proxy.
	 *
	 * @param realCursor the real cursor
	 * @param uUID the uuid
	 */
	public CursorProxy(Cursor realCursor, UUID uUID) {
		mRealCursor = realCursor;
		mUUID = uUID;
		if (null == Looper.myLooper()) {
			mContentObserver = null;
		} else {
			mContentObserver = new ProxyContentObserver(new Handler());
		}
	}

	/**
	 * Instantiates a new cursor proxy.
	 *
	 * @param uUID the uuid
	 */
	public CursorProxy(UUID uUID) {
		this(new NullCursor(), uUID);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#getCursor()
	 */
	@Override
	public Cursor getCursor() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#getChangeListener()
	 */
	@Override
	public ChangeListener getChangeListener() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.utils.events.ChangeListener#onChanged(java.lang.Object)
	 */
	@Override
	public void onChanged(Object inciter) {
		notifyDataChanged();
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getCount()
	 */
	@Override
	public int getCount() {
		synchronized (this) {
			return mRealCursor.getCount();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getPosition()
	 */
	@Override
	public int getPosition() {
		synchronized (this) {
			return mRealCursor.getPosition();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#move(int)
	 */
	@Override
	public boolean move(int offset) {
		synchronized (this) {
			return mRealCursor.move(offset);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToPosition(int)
	 */
	@Override
	public boolean moveToPosition(int position) {
		synchronized (this) {
			return mRealCursor.moveToPosition(position);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToFirst()
	 */
	@Override
	public boolean moveToFirst() {
		synchronized (this) {
			return mRealCursor.moveToFirst();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToLast()
	 */
	@Override
	public boolean moveToLast() {
		synchronized (this) {
			return mRealCursor.moveToLast();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToNext()
	 */
	@Override
	public boolean moveToNext() {
		synchronized (this) {
			return mRealCursor.moveToNext();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#moveToPrevious()
	 */
	@Override
	public boolean moveToPrevious() {
		synchronized (this) {
			return mRealCursor.moveToPrevious();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isFirst()
	 */
	@Override
	public boolean isFirst() {
		synchronized (this) {
			return mRealCursor.isFirst();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isLast()
	 */
	@Override
	public boolean isLast() {
		synchronized (this) {
			return mRealCursor.isLast();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isBeforeFirst()
	 */
	@Override
	public boolean isBeforeFirst() {
		synchronized (this) {
			return mRealCursor.isBeforeFirst();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isAfterLast()
	 */
	@Override
	public boolean isAfterLast() {
		synchronized (this) {
			return mRealCursor.isAfterLast();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnIndex(java.lang.String)
	 */
	@Override
	public int getColumnIndex(String columnName) {
		synchronized (this) {
			return mRealCursor.getColumnIndex(columnName);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnIndexOrThrow(java.lang.String)
	 */
	@Override
	public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
		synchronized (this) {
			return mRealCursor.getColumnIndexOrThrow(columnName);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnName(int)
	 */
	@Override
	public String getColumnName(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getColumnName(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnNames()
	 */
	@Override
	public String[] getColumnNames() {
		synchronized (this) {
			return mRealCursor.getColumnNames();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		synchronized (this) {
			return mRealCursor.getColumnCount();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getBlob(int)
	 */
	@Override
	public byte[] getBlob(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getBlob(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getString(int)
	 */
	@Override
	public String getString(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getString(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#copyStringToBuffer(int, android.database.CharArrayBuffer)
	 */
	@Override
	public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
		synchronized (this) {
			mRealCursor.copyStringToBuffer(columnIndex, buffer);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getShort(int)
	 */
	@Override
	public short getShort(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getShort(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getInt(int)
	 */
	@Override
	public int getInt(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getInt(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getLong(int)
	 */
	@Override
	public long getLong(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getLong(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getFloat(int)
	 */
	@Override
	public float getFloat(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getFloat(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getDouble(int)
	 */
	@Override
	public double getDouble(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getDouble(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isNull(int)
	 */
	@Override
	public boolean isNull(int columnIndex) {
		synchronized (this) {
			return mRealCursor.isNull(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getType(int)
	 */
	@Override
	public int getType(int columnIndex) {
		synchronized (this) {
			return mRealCursor.getType(columnIndex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#deactivate()
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void deactivate() {
		synchronized (this) {
			mRealCursor.deactivate();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#requery()
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean requery() {
		synchronized (this) {
			return mRealCursor.requery();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#close()
	 */
	@Override
	public void close() {
		synchronized (this) {
			mRealCursor.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#isClosed()
	 */
	@Override
	public boolean isClosed() {
		synchronized (this) {
			return mRealCursor.isClosed();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#registerContentObserver(android.database.ContentObserver)
	 */
	@Override
	public void registerContentObserver(ContentObserver observer) {
		addContentObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#unregisterContentObserver(android.database.ContentObserver)
	 */
	@Override
	public void unregisterContentObserver(ContentObserver observer) {
		removeContentObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#registerDataSetObserver(android.database.DataSetObserver)
	 */
	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		addDataSetObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#unregisterDataSetObserver(android.database.DataSetObserver)
	 */
	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		removeDataSetObserver(observer);
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#setNotificationUri(android.content.ContentResolver, android.net.Uri)
	 */
	@Override
	public void setNotificationUri(ContentResolver cr, Uri uri) {
		synchronized (this) {
			mRealCursor.setNotificationUri(cr, uri);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getWantsAllOnMoveCalls()
	 */
	@Override
	public boolean getWantsAllOnMoveCalls() {
		synchronized (this) {
			return mRealCursor.getWantsAllOnMoveCalls();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#getExtras()
	 */
	@Override
	public Bundle getExtras() {
		synchronized (this) {
			return mRealCursor.getExtras();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.database.Cursor#respond(android.os.Bundle)
	 */
	@Override
	public Bundle respond(Bundle extras) {
		synchronized (this) {
			return mRealCursor.respond(extras);
		}
	}

	@Override
	public int getHandle() {
		synchronized (this) {
			if (mRealCursor instanceof NativeCursor) {
				return ((NativeCursor) mRealCursor).getHandle();
			} else {
				return 0;
			}
		}
	}

	/**
	 * Adds the data set observer.
	 *
	 * @param observer the observer
	 */
	protected final void addDataSetObserver(DataSetObserver observer) {
		synchronized (this) {
			if (null != observer) {
				if (null == mDataSetObservers) {
					mDataSetObservers = new WeakRefList<DataSetObserver>();
					mDataSetObservers.add(observer);
					mRealCursor.registerDataSetObserver(mDataSetObserver);
				} else {
					if (!mDataSetObservers.contains(observer)) {
						mDataSetObservers.add(observer);
					}
				}
			}
		}
	}

	/**
	 * Check and handle if no more data set observers.
	 * Unregister empty listener.
	 */
	protected final void checkNhandleNoMoreDataSetObservers() {
		synchronized (this) {
			if (0 == mDataSetObservers.size()) {
				mDataSetObservers = null;
				mRealCursor.unregisterDataSetObserver(mDataSetObserver);
			}
		}
	}

	/**
	 * Removes the data set observer.
	 *
	 * @param observer the observer
	 */
	protected final void removeDataSetObserver(DataSetObserver observer) {
		if (null != observer && null != mDataSetObservers) {
			mDataSetObservers.remove(observer);
			checkNhandleNoMoreDataSetObservers();
		}
	}

	/** The Data set observers. */
	protected WeakRefList<DataSetObserver> mDataSetObservers = null;

	/** The Data set observer. */
	protected final DataSetObserver mDataSetObserver = new DataSetObserver() {

		@Override
		public void onChanged() {
			DataSetObserver[] array = getArray();
			for (DataSetObserver observer : array) {
				observer.onChanged();
			}
		}

		@Override
		public void onInvalidated() {
			DataSetObserver[] array = getArray();
			for (DataSetObserver observer : array) {
				observer.onInvalidated();
			}
		}

		private final DataSetObserver[] getArray() {
			DataSetObserver[] result;
			if (null != mDataSetObservers) {
				result = mDataSetObservers.asArray(new DataSetObserver[0]);
				checkNhandleNoMoreDataSetObservers();
			} else {
				result = new DataSetObserver[0];
			}
			return result;
		}
	};

	/**
	 * Adds the content observer.
	 *
	 * @param observer the observer
	 */
	protected final void addContentObserver(ContentObserver observer) {
		if (null != observer && null != mContentObserver) {
			if (null == mContentObservers) {
				mContentObservers = new WeakRefList<ContentObserver>();
				mContentObservers.add(observer);
				mRealCursor.registerContentObserver(mContentObserver);
			} else {
				if (!mContentObservers.contains(observer)) {
					mContentObservers.add(observer);
				}
			}
		}
	}

	/**
	 * Check and handle if no more content observers.
	 * Unregister empty listener.
	 * 
	 */
	protected final void checkNhandleNoMoreContentObservers() {
		if (0 == mContentObservers.size()) {
			mContentObservers = null;
			mRealCursor.unregisterContentObserver(mContentObserver);
		}
	}

	/**
	 * Removes the content observer.
	 *
	 * @param observer the observer
	 */
	protected final void removeContentObserver(ContentObserver observer) {
		if (null != observer && null != mContentObservers) {
			mContentObservers.remove(observer);
			checkNhandleNoMoreContentObservers();
		}
	}

	/** Weak references to Content observers. */
	protected WeakRefList<ContentObserver> mContentObservers = null;

	/** The Content observer. */
	protected final ContentObserver mContentObserver;

	protected class ProxyContentObserver extends ContentObserver {

		public ProxyContentObserver(Handler handler) {
			super(handler);
		}

		@Override
		public boolean deliverSelfNotifications() {
			return true;
		}

		@Override
		public void onChange(boolean selfChange) {
			ContentObserver[] array = getArray();
			for (ContentObserver observer : array) {
				if ((selfChange && observer.deliverSelfNotifications()) || !selfChange) {
					observer.onChange(selfChange);
				}
			}
		}

		private final ContentObserver[] getArray() {
			ContentObserver[] result = new ContentObserver[0];
			if (null != mContentObservers) {
				result = mContentObservers.asArray(result);
				checkNhandleNoMoreContentObservers();
			}
			return result;
		}
	};

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return mUUID;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#replaceCursorWith(android.database.Cursor)
	 */
	@Override
	public void replaceCursorWith(Cursor cursor) {
		synchronized (this) {
			if (null != mContentObservers) {
				mRealCursor.unregisterContentObserver(mContentObserver);
				cursor.registerContentObserver(mContentObserver);
			}
			if (null != mDataSetObservers) {
				mRealCursor.unregisterDataSetObserver(mDataSetObserver);
				cursor.registerDataSetObserver(mDataSetObserver);
			}
			mRealCursor.close();
			mRealCursor = cursor;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#notifyDataChanged()
	 */
	@Override
	public void notifyDataChanged() {
		mDataSetObserver.onChanged();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ssbs.sw.experimental.cpcengine.DbEngineImpl.CursorTechHole#notifyContentChanged()
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void notifyContentChanged() {
		mContentObserver.dispatchChange(false);
	}

	/** The Real cursor. */
	protected Cursor mRealCursor;

	/** The UUID. */
	protected final UUID mUUID;

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@SuppressWarnings("deprecation")
	protected void finalize() throws Throwable {
		if (null != mRealCursor) {
			mRealCursor.deactivate();
			mRealCursor = null;
		}
		super.finalize();
	}
}
